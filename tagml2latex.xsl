<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
	method="text"
	indent="no"
	encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="/tag">
	<xsl:text>
\documentclass[dvips]{article}

\usepackage{times}
\usepackage{pslatex}

\usepackage[ps2pdf]{hyperref}

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\usepackage{latexsym}
\usepackage[leqno,centertags]{amsmath}

\usepackage{a4wide}

\usepackage{avm}

\usepackage{pstcol}
\usepackage{pstricks}
\usepackage{pst-node}
\usepackage{pst-tree}

\avmvskip{.2ex}
%%\avmoptions{labeled}

%\psset{arrows=->,unit=.75cm,treesep=1cm,levelsep=1.5cm,nodesep=2pt,treefit=loose}
\psset{arrows=->,unit=.75cm,treesep=.75cm,levelsep=1.2cm,nodesep=2pt,treefit=tight}

\makeatletter
%%\def\lab#1{~[tnpos=r]{#1}}
\def\lab#1{{\footnotesize #1}}

\newif\ifTAG@xname\TAG@xnamefalse
\def\psset@xname#1{\def\psk@xname{#1}\TAG@xnametrue}

\newpsstyle{dom}{linecolor=blue,linestyle=dashed}
\newpsstyle{father}{linecolor=black,linestyle=solid}

\def\stdN{\def\pst@par{}\pst@object{stdN}}
\def\stdN@i{\stdN@ii}
\def\stdN@ii#1#2{
\begin@OpenObj%
\pstree{%
\ifTAG@xname
\TR[name=N\psk@xname]{#1}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}
\else
\TR{#1}
\fi
}{#2}%
\end@OpenObj%
%%\pstree{\TR{#1}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}}{#2}
}

\def\leafN{\def\pst@par{}\pst@object{leafN}}
\def\leafN@i{\leafN@ii}
\def\leafN@ii#1{
\begin@OpenObj%
\ifTAG@xname
\TR[name=N\psk@xname]{#1}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}
\else
\TR{#1}
\fi
\end@OpenObj%
}

\def\anchor#1{\textcolor{cyan}{\textbf{#1}}$\Diamond$}
\def\foot#1{#1$\star$}
\def\subst#1{#1$\downarrow$}
\def\lex#1{\textcolor{magenta}{#1}$\diamond$}

\makeatother

%\def\link#1{\nccurve[angle=180,linestyle=dotted,ncurv=1.3]{N#1}{Info#1}}
%\def\link#1{\nccurve[angleA=10,angleB=10,linestyle=dotted,ncurv=2]{N#1}{Info#1}}
\def\link#1{\ncarc[arcangle=20,linestyle=dotted,ncurv=1,linecolor=blue]{N#1}{Info#1}}

\def\NodeError#1{\psframebox[fillstyle=vlines,linecolor=red,hatchcolor=red]{#1}}

\begin{document}
\tableofcontents
	</xsl:text>
<xsl:apply-templates select="tree"/>
	<xsl:text>
\end{document}</xsl:text>
</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<xsl:template match="tree">
  <xsl:variable name="size"><xsl:value-of select="count(.//node[not(node)])"/></xsl:variable>
  <xsl:text>\newpage

\section{\protect{\{
</xsl:text>
<xsl:value-of select="@name"/>
<xsl:text> 
\}}}
%%\large
\begin{center}
</xsl:text>
<xsl:apply-templates select="node"/>

<xsl:choose>
  <xsl:when test="$size > 9">
    <xsl:text>
      \end{center}

      \begin{center}
    </xsl:text>
  </xsl:when>
  <xsl:otherwise>
    <xsl:text>\hfil</xsl:text>
  </xsl:otherwise>
</xsl:choose>
<xsl:text>
\small
\begin{avm}
\avml
</xsl:text>
<xsl:apply-templates select="." mode="recurseavm"/>
<xsl:text>
\avmr
\end{avm}
\end{center}
{
</xsl:text>
<xsl:if test="$size > 9">
  <xsl:text>
\def\link#1{\nccurve[angle=180,linestyle=dotted,ncurv=1.3,linecolor=blue]{N#1}{Info#1}}
  </xsl:text>   
</xsl:if>
<xsl:apply-templates select=".//node[@id]" mode="link"/>
<xsl:text>
}
</xsl:text>
<xsl:for-each select="description//f[@name='ht' or @name='classe']/fs | description/fs[not(//f[@name='ht' or @name='classe'])]">
<xsl:text>

\paragraph{Hypertag}
\begin{center}
</xsl:text>
<xsl:apply-templates select="." mode="root"/>
<xsl:text>
\end{center}
</xsl:text>  
</xsl:for-each>

<xsl:if test="description//f[@name='lfg']">
<xsl:text>

\paragraph{LFG}
\begin{center}
</xsl:text>
<xsl:apply-templates select="description//f[@name='lfg']/fs/f[@name='lex']" mode="root"/>
<xsl:text>
\end{center}
</xsl:text>
</xsl:if>

</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Main Feature -->
<xsl:template match="fs" mode="root">
  <xsl:text>
\begin{avm}{\[
  </xsl:text>
  <xsl:apply-templates select="f"/>
  <xsl:text>\]}
\end{avm}
   </xsl:text>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs/f">
                  <xsl:text>\verb@</xsl:text>
                  <xsl:value-of select="@name"/>
                  <xsl:text>@ &amp;</xsl:text>
                  <xsl:apply-templates/>
			<xsl:if test="position()!=last()">
				<xsl:text>\\</xsl:text>
			</xsl:if>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs/f" mode="onelevel">
	<xsl:text>\normalsize </xsl:text><xsl:value-of select="@name"/><xsl:text>\\[-2pt]
</xsl:text>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="sym">
	<xsl:value-of select="@value"/>
</xsl:template >
<!-- End Feature -->

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs">
  <xsl:text>{\[</xsl:text>
  <xsl:apply-templates select="f"/>
  <xsl:text>\]}</xsl:text>
</xsl:template >
<!-- End Feature -->

<!-- Variables -->
<xsl:template match="var">
  <xsl:text>\@</xsl:text><xsl:value-of select="@name"/>
  <xsl:text> </xsl:text><xsl:apply-templates/>
</xsl:template>


<!-- Variables -->
<xsl:template match="vAlt">
  <xsl:for-each select="./*">
	<xsl:apply-templates select="."/>
	<xsl:if test="position()!=last()">
		<xsl:text>\|</xsl:text>
	</xsl:if>
   </xsl:for-each>
</xsl:template>

<!-- Nodes -->

<xsl:template match="node[@type and ./node]">
  <xsl:text>\stdN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:text>]{\NodeError{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}{</xsl:text>
  <xsl:apply-templates select="./node"/>
  <xsl:text>}</xsl:text>  
</xsl:template>

<xsl:template match="node[@type='subst' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:text>]{\subst{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='foot' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text>]{\foot{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='lex' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:text>]{\lex{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='anchor' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:text>]{\anchor{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node">
  <xsl:text>\stdN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:text>]{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}{</xsl:text>
  <xsl:apply-templates select="./node"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="node" mode="dom">
  <xsl:choose>
    <xsl:when test="@dom='+'">
      <xsl:text>,style=dom</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>,style=father</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="node[@id]" mode="avm">
    <xsl:variable name='identifiant'><xsl:value-of select="@id"/></xsl:variable>
    <xsl:if test="narg or ancestor::tree/description/fs/f[@name='lfg']/fs/f[@name='eq']/fs/f[@name=$identifiant]">
    <xsl:text>\pnode{Info</xsl:text>
    <xsl:value-of select="$identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:value-of select="$identifiant"/>
    <xsl:text>&amp;\[</xsl:text>
    <xsl:apply-templates select="narg" mode="narg"/>
    <xsl:for-each select="ancestor::tree/description/fs/f[@name='lfg']/fs/f[@name='eq']/fs/f[@name=$identifiant]">
      <xsl:apply-templates select="fs/f"/>
      <xsl:text>\\</xsl:text>
    </xsl:for-each>
    <xsl:text>\]\\</xsl:text>      
    </xsl:if>
</xsl:template>

<xsl:template match="tree|node" mode="recurseavm">
  <xsl:apply-templates select="./node[@id]" mode="avm"/>
  <xsl:apply-templates select="./node" mode="recurseavm"/>
</xsl:template>

<xsl:template match="narg" mode="narg">
  <xsl:text></xsl:text>
  <xsl:value-of select="@type"/>
  <xsl:text>\;</xsl:text>
  <xsl:apply-templates select="fs"/>
  <xsl:text>\\</xsl:text>
</xsl:template>

<xsl:template match="node[@id]" mode="link">
  <xsl:variable name='identifiant'><xsl:value-of select="@id"/></xsl:variable>
   <xsl:text>\link{</xsl:text><xsl:value-of select="$identifiant"/>
   <xsl:text>}</xsl:text>
</xsl:template>


</xsl:stylesheet>

