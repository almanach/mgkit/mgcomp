# Convert lfg notation into LaTeX ones

while(<>){
  if (/\\verb\@.*?\@/) {
    if (/_/) {
      s/__/\$\\uparrow\$/og;
      s/_/\$\\downarrow\$/og;
    }
  }
  s/\\verb\@(.*?)\@/$1/og;
  print $_;
}
