/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  mgconvert.pl -- MG conversion to XML
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-import{ file=>'format.pl',
	  preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-op( 700, fx, [<]).
:-op( 700, fx, [node]).
:-op( 750, fx, [opt]).

:-require
	'tools.pl'
	.

:-std_prolog convert_init/0.

convert_init :-
	op( 700, xfy, [>>,>>+]),
	op( 700, fx, [<]),
	op( 700, fx, [node]),
	op( 750, fx, [opt])
	.

convert :-
	option('-xml'),
	format('<?xml version="1.0" encoding="ISO-8859-1"?>',[]),
	xml!wrapper('metaGrammar',
		    [],
		    every((recorded((Class --> Body)),
			   xml!wrapper(class,
				       [name:Class],
				       xml_convert_body(Body))
			  ))),
	nl
	.

convert :-
	option('-oldpl'),
	every((recorded((Class --> Body)),
	       format('class(~w).\n',[Class]),
	       oldpl!convert_body(Class,Body)
	      ))
	.

?- convert_init,
   ( option('-usage') ->
       usage
   ;   
       convert,
       fail
   ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Options

:-std_prolog usage/0.
:-extensional usage_info/2.

usage :-
        emit_convert_info,
        format('options:\n',[]),
        every((   usage_info(Option,Help),
                  format('\t~w\t~w\n',[Option,Help]) )),
        exit(1)
        .

usage_info('-xml     ','XML TAG1 compliant output').
usage_info('-usage   ','display this usage info').

:-std_prolog emit_convert_info/0.

emit_convert_info :-
        format( '~w convert version ~w\n~w <~w>\n',
		['mgconvert',
		 '0.0.1',
		 'Eric de la Clergerie',
		 'Eric.De_La_Clergerie@inria.fr']
		),
	format('\n\t A DyALog converter for Meta Grammars\n\n',[]),
	format('\tmgconvert <files> <options>\n\n',[])
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% XML Display

:-std_prolog xml_convert_body/1.

xml_convert_body(Body) :-
	(   Body == true -> true
	;   Body = (A,Body2) ->
	    (	xml_convert_constraint(A), fail
	    ;	xml_convert_body(Body2)
	    )
	;   xml_convert_constraint(Body)
	)
	.

:-rec_prolog xml_convert_constraint/1.

xml_convert_constraint('<' Class) :-
	xml!elt(super,[name:Class],[]).

xml_convert_constraint(N1 < N2) :-
	xml!elt(relation,[rel:precedes,arg1:N1,arg2:N2],[]).

xml_convert_constraint(N1 >> N2) :-
	xml!elt(relation,[rel:father,arg1:N1,arg2:N2],[]).

xml_convert_constraint(N1 >>+ N2) :-
	xml!elt(relation,[rel:dominates,arg1:N1,arg2:N2],[]).

xml_convert_constraint(N : FS) :-
	xml!wrapper(node,[name:N],xml!val(FS)).

xml_convert_constraint(node N : FS) :-
	xml!wrapper(node,[name:N],xml!val(FS)).

xml_convert_constraint(opt node N : FS) :-
	xml!wrapper(node,[name:N],xml!val(FS)).

xml_convert_constraint(+ Res) :-
	xml!elt(provides,[name:Res],[]).

xml_convert_constraint(- Res) :-
	xml!elt(needs,[name:Res],[]).

xml_convert_constraint(desc(FS)) :-
	xml!wrapper(description,[],xml!val(FS))
	.

xml_convert_constraint(A = B) :-
	path_analyze(A,RA,PA),
	path_analyze(B,RB,PB),
	( PA=[],PB=[] ->
	    ( RA=node(NA),RB=node(NB) ->
		xml!elt(relation,[rel:equals,arg1:NA,arg2:NB],[])
	    ;	RA=node(NA),RB=value(FSB) ->
		xml!wrapper(node,[name:NA],xml!val(FSB))
	    ;	RN=node(NB),RA=value(FSA) ->
		xml!wrapper(node,[name:NB],xml!val(FSA))
	    ;	xml_convert_equation(RA,PA,RB,PB)
	    )
	;
	    xml_convert_equation(RA,PA,RB,PB)
	)
	.

:-std_prolog xml_convert_equation/4.

xml_convert_equation(RA,PA,RB,PB) :-
	xml!wrapper(equation,[],
		    (	xml_convert_root(RA,PA),
			xml_convert_root(RB,PB)
		    ))
	.

:-rec_prolog xml_convert_root/2.

xml_convert_root(node(N),P) :-
	( P == [] ->
	    xml!elt(node,[name:N],[])
	;
	    xml!wrapper(node,[name:N],xml_convert_path(P))
	)
	.

xml_convert_root(desc,P) :-
	( P == [] ->
	    xml!elt(description,[],[])
	;   
	    xml!wrapper(description,[],xml_convert_path(P))
	)
	.

xml_convert_root(var(V),P) :-
	( P == [] ->
	    xml!elt(var,[name:V],[])
	;   
	    xml!wrapper(var,[name:V],xml_convert_path(P))
	)
	.

xml_convert_root(father(N),P) :-
	( P == [] ->
	    xml!elt(father,[name:N],[])
	;   
	    xml!wrapper(father,[name:N],xml_convert_path(P))
	)
	.

xml_convert_root(value(FS),[]) :-
	xml!wrapper(value,[],xml!val(FS))
	.

:-std_prolog xml_convert_path/1.

xml_convert_path(P) :-
	( P = [] -> true
	;   P = F:P2 ->
	    xml!wrapper(f,[name:F],xml_convert_path(P2))
	;
	    xml!elt(f,[name:P],[])
	)
	.

:-std_prolog path_analyze/3.

path_analyze(X,RX,PX) :-
	(   X = (RX1 : PX) xor X=RX1,PX=[] ),
	( atom(RX1),\+ RX1 == desc -> RX=node(RX1) ; RX1=RX ),
	true
	.

:-std_prolog xml!fs/1.

xml!fs(FS) :-
	( FS=[] -> xml!elt(fs,[],[])
	;
	    xml!wrapper(fs,[],
			every(( domain(Name:V,FS),
				( var(V) ->
				    xml!elt(f,[name:Name],[])
				;   
				    xml!wrapper(f,[name:Name],xml!val(V))
				)
			      ))
		       )
	)
	.

:-std_prolog xml!val/1.

xml!val(V) :-
	( V = var([]) ->
	    xml!elt(any,[],[])
	;   V = var(Var) ->
	    xml!elt(var,[name:Var],[])
	;   V = var(Var)^FS ->
	    xml!wrapper(var,[name:Var],xml!val(FS))
	;   V=(+) ->
	    xml!elt(plus,[],[])
	;   V=(-) ->
	    xml!elt(minus,[],[])
	;   V = [] ->
	    xml!elt(fs,[],[])
	;   atom(V) ->  
%%	    xml!elt(val,[],[V])
	    xml!elt(sym,[value:V],[])
	;   V = disj(L) ->
	    xml!wrapper('vAlt',[],
			every((domain(VV,L),xml!val(VV))))
	;   V = notdisj(L) ->
	    xml!wrapper('not',[],
			xml!wrapper('vAlt',[],
				    every((domain(VV,L),xml!val(VV)))))
	;   
	    xml!fs(V)
	)
	.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% XML Display tools

:-std_prolog indent_update/2.

indent_update(I,J) :- atom_concat(I,'  ',J).

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :-
	mutable(M,0,true),
	every((   domain(A,AA),
		  mutable_inc(M,V),
		  (   V == 0 xor write(Stream,Sep) ),
		  format(Stream,Format,[A]) ))
	.

format_hook(0'A,Stream,[A:V|R],R) :-
	format(Stream,'~w="~B"',[A,V])
	.

format_hook(0'I,Stream,R,R) :-
	newline_start(Start),
	format(Stream,'\n~w',[Start])
	.

format_hook(0'B,Stream,[V|R],R) :-
	( atom(V) -> atom_module(V,M,VV) ; VV=V, M=[]), 
	(   M == [] xor format(Stream,'~B:',[M])),
	format(Stream,'~w',[VV])
	.

:-xcompiler((
	     newline_start(Start) :- '$interface'( 'Newline_Start_1'(Start:term),[])
	    ))
.

:-std_prolog
	indent!inc/1,
	indent!restore/1.

indent!inc(Old) :-
	newline_start(Old),
	indent_update(Old,New),
	newline_start(New)
	.

indent!restore(Old) :-
	newline_start(Old)
	.

:-xcompiler((indent!wrapper(G) :- indent!inc(Old),G,indent!restore(Old))).
	    
:-xcompiler((xml!wrapper(Name,Attrs,G) :-
	    indent!wrapper(( format('~I<~w~L>',[Name,[' ~A',' '],Attrs]),
			     G,
			     format('~I</~w>',[Name])
			   ))
	    ))
.

:-std_prolog xml!elt/3.

xml!elt(Elt,Attrs,Content) :-
	( Content=[] ->
	    indent!wrapper((format('~I<~w~L/>',[Elt,[' ~A',''],Attrs])))
	;   
	    xml!wrapper(Elt,Attrs,format('~L',[['~w',' '],Content]))
	)
	.
