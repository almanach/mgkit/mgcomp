/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos2.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos2).
node(pos2,x).
node(pos2,a).
node(pos2,b).
node(pos2,c).
father(pos2,x,a).
father(pos2,x,b).
father(pos2,x,c).

precedes(pos2,a,b).

nodefeature(pos2,a,[rank: last]).

%% No answers: a<b violate a last


