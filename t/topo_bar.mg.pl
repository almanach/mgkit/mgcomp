/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_bar.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(bar).
node(bar,s).
node(bar,a).
node(bar,b).
node(bar,c).
dominates(bar,s,a).
dominates(bar,s,b).
dominates(bar,s,c).
precedes(bar,b,c).
%%precedes(bar,a,c).

answer([bar],[s,##([[b],[c]], [[a]])]).
answer([bar],[s,[a,[b]],[c]]).
answer([bar],[s,[a,[b],[c]]]).
answer([bar],[s,[b,[a]],[c]]).
answer([bar],[s,[b],[a,[c]]]).
answer([bar],[s,[b],[c,[a]]]).
