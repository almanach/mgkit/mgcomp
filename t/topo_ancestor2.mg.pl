/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_ancestor2.mg.pl -- Test MGCOMP
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(ancestors2).
node(ancestors2,a1).
node(ancestors2,a2).
node(ancestors2,a3).
node(ancestors2,b).
dominates(ancestors2,a1,b).
dominates(ancestors2,a2,b).
dominates(ancestors2,a3,b).

answer([ancestors2],[a2,[a3,[a1,[b]]]]).
answer([ancestors2],[a2,[a1,[a3,[b]]]]).
answer([ancestors2],[a1,[a3,[a2,[b]]]]).
answer([ancestors2],[a1,[a2,[a3,[b]]]]) .
answer([ancestors2],[a3,[a2,[a1,[b]]]]).
answer([ancestors2],[a3,[a1,[a2,[b]]]]).

