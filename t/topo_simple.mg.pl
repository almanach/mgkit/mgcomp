/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_simple.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(simple).
node(simple,a).
node(simple,b).

answer([simple],[a,[b]]).
answer([simple],[b,[a]]).
