/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos8.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos8).
node(pos8,x).
node(pos8,a).
node(pos8,b).
node(pos8,c).
dominates(pos8,x,a).
dominates(pos8,x,b).
dominates(pos8,x,c).

nodefeature(pos8,a,[rank:first]).
%%nodefeature(pos8,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos8,a,[rank:single]). %% a is both first or last

nodefeature(pos8,c,[rank: first]).
nodefeature(pos8,b,[rank: first]).

answer([pos8],[x,[a,[c,[b]]]]).
answer([pos8],[x,[a,[b,[c]]]]).
answer([pos8],[x,[c,[a,[b]]]]).
answer([pos8],[x,[c,[b,[a]]]]).
answer([pos8],[x,[b,[c,[a]]]]).
answer([pos8],[x,[b,[a,[c]]]]).
