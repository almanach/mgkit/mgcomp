/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos9.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos9).
node(pos9,x).
node(pos9,a).
node(pos9,b).
node(pos9,c).
dominates(pos9,x,a).
dominates(pos9,x,b).
dominates(pos9,x,c).

nodefeature(pos9,a,[rank:first]).
%%nodefeature(pos9,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos9,a,[rank:single]). %% a is both first or last

nodefeature(pos9,c,[rank: last]).
nodefeature(pos9,b,[rank: first]).

answer([pos9],[x,[b],[c,[a]]]).
answer([pos9],[x,[b,[a]],[c]]).
answer([pos9],[x,[b,[c,[a]]]]).
answer([pos9],[x,[b,[a,[c]]]]).
answer([pos9],[x,[b,[a],[c]]]).
answer([pos9],[x,[a],[c,[b]]]).
answer([pos9],[x,[a,[b]],[c]]).
answer([pos9],[x,[a,[c,[b]]]]).
answer([pos9],[x,[a,[b,[c]]]]).
answer([pos9],[x,[a,[b],[c]]]).
answer([pos9],[x,[c,[a,[b]]]]).
answer([pos9],[x,[c,[b,[a]]]]).

