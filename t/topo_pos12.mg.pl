/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos12.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos12).
node(pos12,x).
node(pos12,a).
node(pos12,b).
node(pos12,c).
dominates(pos12,x,a).
dominates(pos12,x,b).
dominates(pos12,x,c).

nodefeature(pos12,a,[rank:single]).
%%nodefeature(pos12,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos12,a,[rank:single]). %% a is both first or last

nodefeature(pos12,c,[rank: last]).
nodefeature(pos12,b,[rank: last]).

answer([pos12],[x,[a,[c,[b]]]]).
answer([pos12],[x,[a,[b,[c]]]]).
answer([pos12],[x,[c,[a,[b]]]]).
answer([pos12],[x,[c,[b,[a]]]]).
answer([pos12],[x,[b,[c,[a]]]]).
answer([pos12],[x,[b,[a,[c]]]]).
