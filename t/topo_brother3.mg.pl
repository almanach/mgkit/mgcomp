/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_brother3.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of brother
 * ----------------------------------------------------------------
 */

class(brother3).
node(brother3,x).
node(brother3,a).
node(brother3,b).
node(brother3,c).
dominates(brother3,x,a).
dominates(brother3,x,b).
dominates(brother3,x,c).

%%father(brother3,x,a).
brother(brother3,a,b).
brother(brother3,a,c).

nodefeature(brother3,a,[rank: last]).

%%father(brother3,y,a).
%%father(brother3,y,b).

answer([brother3],[x,##([[b]], [[c]]),[a]]).

