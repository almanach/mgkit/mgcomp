class(test).

node(test,root).

nodefeature(test,
    'root',
    [cat: 'root']
).


node(test,alpha).

nodefeature(test,
    alpha,
    [cat: a]
).

node(test,beta).

nodefeature(test,
    beta,
    [cat: b]
).


father(test,root,alpha).
father(test,root,beta).
precedes(test,alpha,beta).

guard(test,alpha,+,
      [ node(alpha) : foo = value(1),
	and( [ node(alpha) : bar = value(2),
	       [  node(alpha) : toto = value(3),
		  node(alpha) : toto = value(4)
	       ],
	       node(alpha) : titi = value(5)
	     ]
	   )
      ]
     ).
      
