#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => 1;

my $dir = dirname($0);
my @output = split /\n/, `./mgcomp $dir/sample.mg.pl -simple`;

my $fail = 0;
foreach my $expected_output (@data) {
    my $output = shift @output;
    chomp $expected_output;
    $fail++ unless ($output eq $expected_output);
}
is($fail,0,"test simple display");

__DATA__
det&
PP(prep&, N2)
PP(prep&, S)
WS(csu&, S!)
adv&
S(adv&)
adj(adv&)
adv(adv&)
V(adv&)
N2(np&)
N(adj&)
N2(det!, N(np&))
N2(det!, N(nc&))
