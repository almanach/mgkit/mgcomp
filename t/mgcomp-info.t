#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => 1;

my $dir = dirname($0);
my @output = split /\n/, `./mgcomp $dir/sample.mg.pl -info`;

my $fail = 0;
foreach my $expected_output (@data) {
    my $output = shift @output;
    chomp $expected_output;
#     unless ($output eq $expected_output) {
#       $fail++;
#       print <<EOF;
# expected: $expected_output
# got: $output
# EOF
#    }
    $fail++ unless ($output eq $expected_output);
}
is($fail,0,"test info display");

__DATA__
Terminal adj
Terminal det
Terminal pnoun
Terminal cnoun
Terminal pnoun_as_cnoun
Terminal verbs
Terminal prep_N2
Terminal prep_S
Terminal csu
Terminal adv_leaf
Terminal adv_v
Terminal adv_adv
Terminal adv_adj
Terminal adv_s
Terminal agreement
Terminal auxiliary
Terminal deep_auxiliary
Terminal shallow_auxiliary
Class [det]
	Binding: var(cat@0) : det
	Description: [ht : [cat : var(cat@0)]]
	Node det [Root] { det }
	FS: [cat : det,type : anchor]
Class [prep_N2]
	Binding: var(cat@2) : N2
	Binding: var(pcas@3) : var(pcas@4)
	Binding: var(cat@5) : prep
	Binding: var(pp@1) : N2
	Description: [ht : [cat : var(cat@5),pcas : var(pcas@3),pp : var(pp@1)]]
	Node prep { prep }
	FS: [bot : [pcas : var(pcas@4)],cat : prep,type : anchor]
	Node PP [Root] { PP }
		PP strong father prep
		PP strong father N2
		prep < N2 [strict]
	FS: [cat : PP]
	Node N2 { N2 }
	FS: [cat : N2]
Class [prep_S]
	Binding: var(cat@2) : S
	Binding: var(pcas@3) : var(pcas@4)
	Binding: var(cat@5) : prep
	Binding: var(pp@1) : S
	Description: [ht : [cat : var(cat@5),pcas : var(pcas@3),pp : var(pp@1)]]
	Node prep { prep }
	FS: [bot : [pcas : var(pcas@4)],cat : prep,type : anchor]
	Node PP [Root] { PP }
		PP strong father prep
		PP strong father S
		prep < S [strict]
	FS: [cat : PP]
	Node S { S }
	FS: [cat : S,top : [mode : infinitive]]
Class [csu]
	Binding: var(cat@8) : csu
	Binding: var(lfg@6) : var(lfg@7)
	Description: [ht : [cat : var(cat@8)]]
	Node S { S }
	FS: [cat : S,top : [lfg : var(lfg@7)],type : subst]
	Node WS [Root] { WS }
		WS strong father C
		WS strong father S
		C < S [strict]
	FS: [bot : [lfg : var(lfg@6)],cat : WS]
	Node C { C }
	FS: [cat : csu,type : anchor]
Class [adv_leaf]
	Binding: var(cat@9) : adv
	Description: [ht : [cat : var(cat@9)]]
	Node Adv [Root] { Adv }
	FS: [cat : adv,type : anchor]
Class [auxiliary]
	Binding: var(cat@11) : var(cat@12)
	Binding: var(type@10) : foot
	Description: var([])
Class [adv_s,H!shallow_auxiliary]
	Binding: var(H!bot@13) : var(H!top@14)
	Binding: var(cat@9) : adv
	Description: [ht : [cat : var(cat@9)]]
	Node Sr [Root] { Sr }
		Sr strong father Adv
	FS: [bot : var(H!bot@13),cat : S]
	Node Adv { Adv }
	FS: [cat : adv,type : anchor]
Class [adv_adj,H!shallow_auxiliary]
	Binding: var(H!bot@13) : var(H!top@14)
	Binding: var(cat@9) : adv
	Description: [ht : [cat : var(cat@9)]]
	Node Adjr [Root] { Adjr }
		Adjr strong father Adv
	FS: [bot : var(H!bot@13),cat : adj]
	Node Adv { Adv }
	FS: [cat : adv,type : anchor]
Class [adv_adv,H!shallow_auxiliary]
	Binding: var(H!bot@13) : var(H!top@14)
	Binding: var(cat@9) : adv
	Description: [ht : [cat : var(cat@9)]]
	Node Advr [Root] { Advr }
		Advr strong father Adv
	FS: [bot : var(H!bot@13),cat : adv]
	Node Adv { Adv }
	FS: [cat : adv,type : anchor]
Class [adv_v,H!shallow_auxiliary]
	Binding: var(H!bot@13) : var(H!top@14)
	Binding: var(cat@9) : adv
	Description: [ht : [cat : var(cat@9)]]
	Node Vr [Root] { Vr }
		Vr strong father Adv
	FS: [bot : var(H!bot@13),cat : V]
	Node Adv { Adv }
	FS: [cat : adv,type : anchor]
Class [noun!agreement,pnoun]
	Binding: var(noun!gender@15) : var(noun!gender@16)
	Binding: var(noun!number@17) : var(noun!number@18)
	Description: [ht : [cat : noun]]
	Node Np { Np }
	FS: [cat : np,top : [gender : var(noun!gender@16),number : var(noun!number@18)],type : anchor]
	Node N2 [Root] { N2 }
		N2 strong father Np
	FS: [bot : [gender : var(noun!gender@15),number : var(noun!number@17)],cat : N2]
Class [adj,adj!agreement,noun!shallow_auxiliary]
	Binding: var(noun!top@14) : [gender : var(adj!gender@15),number : var(adj!number@17)]
	Binding: var(adj!gender@15) : var(adj!gender@16)
	Binding: var(adj!number@17) : var(adj!number@18)
	Binding: var(cat@19) : adj
	Binding: var(noun!bot@13) : var(noun!top@14)
	Description: [ht : [cat : var(cat@19)]]
	Node Nr [Root] { Nr }
		Nr strong father adj
	FS: [bot : var(noun!bot@13),cat : N,toto : ,...]
	Node adj { adj }
	FS: [cat : adj,top : [gender : var(adj!gender@16),number : var(adj!number@18)],type : anchor]
Class [det!agreement,n!agreement,nc!agreement,pnoun_as_cnoun]
	Binding: var(nc!gender@15) : var(nc!gender@16)
	Binding: var(nc!number@17) : var(nc!number@18)
	Binding: var(det!gender@15) : var(n!gender@16)
	Binding: var(det!number@17) : var(n!number@18)
	Binding: var(n!number@17) : var(n!number@18)
	Binding: var(n!gender@15) : var(n!gender@16)
	Binding: var(det!gender@16) : var(n!gender@16)
	Binding: var(det!number@18) : var(n!number@18)
	Description: [ht : [cat : noun]]
	Node Nc { Nc }
	FS: [cat : np,top : [gender : var(nc!gender@16),number : var(nc!number@18)],type : anchor]
	Node N2 [Root] { N2 }
		N2 strong father N
		N2 strong father det
		det < N [strict]
	FS: [bot : [gender : var(det!gender@15),number : var(det!number@17)],cat : N2,type : std]
	Node N { N }
		N strong father Nc
	FS: [bot : [gender : var(nc!gender@15),number : var(nc!number@17)],cat : N,top : [gender : var(n!gender@16),number : var(n!number@18)]]
	Node det { det }
	FS: [cat : det,top : [gender : var(det!gender@16),number : var(det!number@18)],type : subst]
Class [det!agreement,n!agreement,nc!agreement,cnoun]
	Binding: var(nc!gender@15) : var(nc!gender@16)
	Binding: var(nc!number@17) : var(nc!number@18)
	Binding: var(det!gender@15) : var(n!gender@16)
	Binding: var(det!number@17) : var(n!number@18)
	Binding: var(n!number@17) : var(n!number@18)
	Binding: var(n!gender@15) : var(n!gender@16)
	Binding: var(det!gender@16) : var(n!gender@16)
	Binding: var(det!number@18) : var(n!number@18)
	Description: [ht : [cat : noun]]
	Node Nc { Nc }
	FS: [cat : nc,top : [gender : var(nc!gender@16),number : var(nc!number@18)],type : anchor]
	Node N2 [Root] { N2 }
		N2 strong father N
		N2 strong father det
		det < N [strict]
	FS: [bot : [gender : var(det!gender@15),number : var(det!number@17)],cat : N2,type : std]
	Node N { N }
		N strong father Nc
	FS: [bot : [gender : var(nc!gender@15),number : var(nc!number@17)],cat : N,top : [gender : var(n!gender@16),number : var(n!number@18)]]
	Node det { det }
	FS: [cat : det,top : [gender : var(det!gender@16),number : var(det!number@18)],type : subst]
