/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_free.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(free).
node(free,a).
node(free,b1).
node(free,b2).
node(free,b3).
father(free,a,b1).
father(free,a,b2).
father(free,a,b3).
node(free,d1).
node(free,e1).
father(free,b1,d1).
father(free,b1,e1).
precedes(free,d1,e1).

node(free,d2).
node(free,e2).
father(free,b2,d2).
father(free,b2,e2).
precedes(free,d2,e2).

node(free,d3).
node(free,e3).
father(free,b3,d3).
father(free,b3,e3).
precedes(free,d3,e3).

answer([free], [a,##([[b3,[d3],[e3]]], ##([[b2,[d2],[e2]]], [[b1,[d1],[e1]]]))]).
