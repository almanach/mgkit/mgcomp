/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_bar2.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(test).
node(test,det).
node(test,det1).
node(test,number).
node(test,range).
node(test,prep).
node(test,number2).
father(test,det,det1).
father(test,det,number).
father(test,det,range).
father(test,number,prep).
father(test,number,number2).
precedes(test,number,range).
precedes(test,prep,number2).
precedes(test,det1,number).

answer([test],[det,[det1],[number,[prep],[number2]],[range]]).
