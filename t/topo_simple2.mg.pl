/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_simple2.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(simple2).
node(simple2,a).
node(simple2,b).
node(simple2,c).

answer([simple2],[c,##([[a]], [[b]])]).
answer([simple2],[c,[b,[a]]]).
answer([simple2],[c,[a,[b]]]).
answer([simple2],[b,##([[a]], [[c]])]).
answer([simple2],[b,[c,[a]]]).
answer([simple2],[b,[a,[c]]]).
answer([simple2],[a,##([[b]], [[c]])]).
answer([simple2],[a,[c,[b]]]).
answer([simple2],[a,[b,[c]]]).

