/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos10.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos10).
node(pos10,x).
node(pos10,a).
node(pos10,b).
node(pos10,c).
dominates(pos10,x,a).
dominates(pos10,x,b).
dominates(pos10,x,c).

nodefeature(pos10,a,[rank:single]).
%%nodefeature(pos10,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos10,a,[rank:single]). %% a is both first or last

%%nodefeature(pos10,c,[rank: last]).
%%nodefeature(pos10,b,[rank: first]).

answer([pos10],[x,##([[b]], [[c,[a]]])]).
answer([pos10],[x,##([[c]], [[b,[a]]])]).
answer([pos10],[x,[a,[c,[b]]]]).
answer([pos10],[x,[a,[b,[c]]]]).
answer([pos10],[x,[a,##([[c]], [[b]])]]).
answer([pos10],[x,[c,[a,[b]]]]).
answer([pos10],[x,[c,[b,[a]]]]).
answer([pos10],[x,[b,[c,[a]]]]).
answer([pos10],[x,[b,[a,[c]]]]).


