/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos7.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos7).
node(pos7,x).
node(pos7,a).
node(pos7,b).
node(pos7,c).
dominates(pos7,x,a).
dominates(pos7,x,b).
dominates(pos7,x,c).

nodefeature(pos7,a,[rank:first]).
%%nodefeature(pos7,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos7,a,[rank:single]). %% a is both first or last

nodefeature(pos7,c,[rank: first]).
%%nodefeature(pos7,b,[rank: first]).

answer([pos7],[x,[a],[b,[c]]]).
answer([pos7],[x,[a,[c]],[b]]).
answer([pos7],[x,[a,[b,[c]]]]).
answer([pos7],[x,[a,[c,[b]]]]).
answer([pos7],[x,[a,[c],[b]]]).
answer([pos7],[x,[c,[a]],[b]]).
answer([pos7],[x,[c,[b,[a]]]]).
answer([pos7],[x,[c,[a,[b]]]]).
answer([pos7],[x,[c,[a],[b]]]).
answer([pos7],[x,[c],[b,[a]]]).
answer([pos7],[x,[b,[c,[a]]]]).
answer([pos7],[x,[b,[a,[c]]]]).
