/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos1).
node(pos1,x).
node(pos1,a).
node(pos1,b).
node(pos1,c).
father(pos1,x,a).
father(pos1,x,b).
father(pos1,x,c).

nodefeature(pos1,a,[rank: last]).

answer([pos1],[x,##([[c]], [[b]]),[a]]).
