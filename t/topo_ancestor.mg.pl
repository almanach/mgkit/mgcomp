/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_ancestor.mg.pl -- Test MGCOMP
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(ancestors).
node(ancestors,a1).
node(ancestors,a2).
node(ancestors,a3).
node(ancestors,b).
dominates(ancestors,a1,b).
dominates(ancestors,a2,b).
dominates(ancestors,a3,b).
%%dominates(ancestors,a,b).

node(ancestors,s).
dominates(ancestors,s,a1).
dominates(ancestors,s,a2).
dominates(ancestors,s,a3).

answer([ancestors],[s,[a2,[a3,[a1,[b]]]]]).
answer([ancestors],[s,[a2,[a1,[a3,[b]]]]]).
answer([ancestors],[s,[a1,[a3,[a2,[b]]]]]).
answer([ancestors],[s,[a1,[a2,[a3,[b]]]]]) .
answer([ancestors],[s,[a3,[a2,[a1,[b]]]]]).
answer([ancestors],[s,[a3,[a1,[a2,[b]]]]]).

