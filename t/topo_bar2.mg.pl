/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_bar2.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(bar2).
node(bar2,s).
node(bar2,a).
node(bar2,b).
node(bar2,c).
node(bar2,d).
dominates(bar2,s,a).
dominates(bar2,s,b).
dominates(bar2,s,c).
dominates(bar2,s,d).
precedes(bar2,b,c).
%%precedes(bar2,a,c).
precedes(bar2,a,d).

answer([bar2],[s,##([[b],[c]], [[a],[d]])]).
answer([bar2],[s,[a,[b]],##([[d]], [[c]])]).
answer([bar2],[s,[a,[b]],[c,[d]]]).
answer([bar2],[s,[a,[b]],[d,[c]]]).
answer([bar2],[s,[a,[b],[c]],[d]]).
answer([bar2],[s,[a],[d,[b]],[c]]).
answer([bar2],[s,[a],[b,[d]],[c]]).
answer([bar2],[s,[a],[b],[d,[c]]]).
answer([bar2],[s,[a],[b],[c,[d]]]).
answer([bar2],[s,[b,[a]],##([[d]], [[c]])]).
answer([bar2],[s,[b,[a],[d]],[c]]).
answer([bar2],[s,[b,[a]],[c,[d]]]).
answer([bar2],[s,[b,[a]],[d,[c]]]).
answer([bar2],[s,[b,[a]],[c],[d]]).
answer([bar2],[s,[b],[c,[a],[d]]]).
answer([bar2],[s,[d],[b],[c,[a]]]).
