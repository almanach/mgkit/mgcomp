/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_brother2.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of brother
 * ----------------------------------------------------------------
 */

class(brother2).
node(brother2,x).
node(brother2,a).
node(brother2,b).
node(brother2,c).
dominates(brother2,x,a).
dominates(brother2,x,b).
dominates(brother2,x,c).

%%father(brother2,x,a).
brother(brother2,a,b).
brother(brother2,a,c).

%%father(brother2,y,a).
%%father(brother2,y,b).

answer([brother2],[x,##([[a]], ##([[b]], [[c]]))]).

