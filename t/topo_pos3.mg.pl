/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos3.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos3).
node(pos3,x).
node(pos3,a).
node(pos3,b).
node(pos3,c).
dominates(pos3,x,a).
dominates(pos3,x,b).
dominates(pos3,x,c).

nodefeature(pos3,a,[rank: last]).
%%nodefeature(pos3,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos3,a,[rank:single]). %% a is both first or last

%%nodefeature(pos3,c,[rank: last]).
%%nodefeature(pos3,b,[rank: last]).

answer([pos3],[x,[c,[b]],[a]]).
answer([pos3],[x,[b,[c]],[a]]).
answer([pos3],[x,##([[c]], [[b]]),[a]]).
answer([pos3],[x,[b,[a,[c]]]]).
answer([pos3],[x,[b,[c,[a]]]]).
answer([pos3],[x,[b],[a,[c]]]).
answer([pos3],[x,[c,[a,[b]]]]).
answer([pos3],[x,[c,[b,[a]]]]).
answer([pos3],[x,[c,[b],[a]]]).
answer([pos3],[x,[c],[a,[b]]]).
answer([pos3],[x,[a,[c,[b]]]]).
answer([pos3],[x,[a,[b,[c]]]]).
answer([pos3],[x,[a,##([[c]], [[b]])]]).
answer([pos3],[x,##([[b]], [[c,[a]]])]).
answer([pos3],[x,##([[c]], [[b,[a]]])]).
answer([pos3],[x,[b,[c],[a]]]).



