/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos4.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos4).
node(pos4,x).
node(pos4,a).
node(pos4,b).
node(pos4,c).
dominates(pos4,x,a).
dominates(pos4,x,b).
dominates(pos4,x,c).

nodefeature(pos4,a,[rank: last]).
%%nodefeature(pos4,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos4,a,[rank:single]). %% a is both first or last

nodefeature(pos4,c,[rank: last]).
%%nodefeature(pos4,b,[rank: last]).

answer([pos4],[x,[b,[c]],[a]]).
answer([pos4],[x,[b,[a]],[c]]).
answer([pos4],[x,[b,[c,[a]]]]).
answer([pos4],[x,[b,[a,[c]]]]).
answer([pos4],[x,[b],[c,[a]]]).
answer([pos4],[x,[b],[a,[c]]]).
answer([pos4],[x,[a,[c,[b]]]]).
answer([pos4],[x,[a,[b,[c]]]]).
answer([pos4],[x,[a,[b],[c]]]).
answer([pos4],[x,[c,[a,[b]]]]).
answer([pos4],[x,[c,[b,[a]]]]).
answer([pos4],[x,[c,[b],[a]]]).
