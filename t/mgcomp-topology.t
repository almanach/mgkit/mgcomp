#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

my $dir = dirname($0);
my @files = map("topo_".$_,
		qw{test
		   free
		   foo
		   deep
		   partial
		   bar
		   bar2
		   ancestor
		   ancestor2
		   simple
		   simple2
		   pos1
		   pos2
		   pos3
		   pos4
		   pos5
		   pos6
		   pos7
		   pos8
		   pos9
		   pos10
		   pos11
		   pos12
		   brother1
		   brother2
		   brother3
		});

plan tests => scalar @files;

foreach my $file (@files) {
  mytest($file);
}

sub mytest {
  my $file = shift;
  my $res = `./mgcomp $dir/$file.mg.pl -test | grep '^fail' | wc -l`;
  $res =~ /(\d+)/;
  is($1,0, "test topology $file");
}
