/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos11.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos11).
node(pos11,x).
node(pos11,a).
node(pos11,b).
node(pos11,c).
dominates(pos11,x,a).
dominates(pos11,x,b).
dominates(pos11,x,c).

nodefeature(pos11,a,[rank:single]).
%%nodefeature(pos11,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos11,a,[rank:single]). %% a is both first or last

nodefeature(pos11,c,[rank: last]).
%%nodefeature(pos11,b,[rank: first]).

answer([pos11],[x,[b],[c,[a]]]).
answer([pos11],[x,[b,[a]],[c]]).
answer([pos11],[x,[b,[c,[a]]]]).
answer([pos11],[x,[b,[a,[c]]]]).
answer([pos11],[x,[a,[c,[b]]]]).
answer([pos11],[x,[a,[b,[c]]]]).
answer([pos11],[x,[a,[b],[c]]]).
answer([pos11],[x,[c,[a,[b]]]]).
answer([pos11],[x,[c,[b,[a]]]]).
