#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => 1;

my $dir = dirname($0);
my @output = split /\n/, `./mgcomp $dir/sample.mg.pl -tree`;

my $fail = 0;
foreach my $expected_output (@data) {
    my $output = shift @output;
    chomp $expected_output;
    $fail++ unless ($output eq $expected_output);
}
is($fail,0,"test tree display");

__DATA__
	Tree [det]
	Tree [PP,[prep],[N2]]
	Tree [PP,[prep],[S]]
	Tree [WS,[C],[S]]
	Tree [Adv]
	Tree [Sr,[Adv]]
	Tree [Adjr,[Adv]]
	Tree [Advr,[Adv]]
	Tree [Vr,[Adv]]
	Tree [N2,[Np]]
	Tree [Nr,[adj]]
	Tree [N2,[det],[N,[Nc]]]
	Tree [N2,[det],[N,[Nc]]]
