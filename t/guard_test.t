#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => 1;

my $dir = dirname($0);
my @output = split /\n/, `./mgcomp $dir/guard_test.mg.pl -xml | xmllint - -format`;

my $fail = 0;
foreach my $expected_output (@data) {
    my $output = shift @output;
    chomp $expected_output;
    $fail++ unless ($output eq $expected_output);
}
is($fail,0,"test complex guard");

__DATA__
<?xml version="1.0" encoding="ISO-8859-1"?>
<tag>
  <tree name="0 test">
    <description>
      <any/>
    </description>
    <node adj="yes" id="root" cat="root" type="std">
      <node adj="yes" id="alpha" cat="a" type="std">
        <guards rel="+">
          <or>
            <guard type="foo">
              <var name="foo@0"/>
              <symbol value="1"/>
            </guard>
            <and>
              <guard type="bar">
                <var name="bar@1"/>
                <symbol value="2"/>
              </guard>
              <or>
                <guard type="toto">
                  <var name="toto@2"/>
                  <symbol value="3"/>
                </guard>
                <guard type="toto">
                  <var name="toto@2"/>
                  <symbol value="4"/>
                </guard>
              </or>
              <guard type="titi">
                <var name="titi@3"/>
                <symbol value="5"/>
              </guard>
            </and>
          </or>
        </guards>
      </node>
      <node adj="yes" id="beta" cat="b" type="std"/>
    </node>
  </tree>
</tag>
