#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => 1;

my $dir = dirname($0);
my @output = split /\n/, `./mgcomp $dir/sample.mg.pl -xml | xmllint - -format`;

my $fail = 0;
foreach my $expected_output (@data) {
    my $output = shift @output;
    chomp $expected_output;
    $fail++ unless ($output eq $expected_output);
}
is($fail,0,"test XML display");

__DATA__
<?xml version="1.0" encoding="ISO-8859-1"?>
<tag>
  <tree name="0 det">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="det"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="det" cat="det" type="anchor"/>
  </tree>
  <tree name="1 prep_N2">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="N2"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="PP" cat="PP" type="std">
      <node adj="yes" id="prep" cat="prep" type="anchor">
        <narg type="bot">
          <fs>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="N2" cat="N2" type="std"/>
    </node>
  </tree>
  <tree name="2 prep_S">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="S"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="PP" cat="PP" type="std">
      <node adj="yes" id="prep" cat="prep" type="anchor">
        <narg type="bot">
          <fs>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="S" cat="S" type="std">
        <narg type="top">
          <fs>
            <f name="mode">
              <symbol value="infinitive"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree>
  <tree name="3 csu">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="csu"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="WS" cat="WS" type="std">
      <narg type="bot">
        <fs>
          <f name="lfg">
            <var name="lfg@7"/>
          </f>
        </fs>
      </narg>
      <node adj="yes" id="C" cat="csu" type="anchor"/>
      <node adj="no" id="S" cat="S" type="subst">
        <narg type="top">
          <fs>
            <f name="lfg">
              <var name="lfg@7"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree>
  <tree name="4 adv_leaf">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Adv" cat="adv" type="anchor"/>
  </tree>
  <tree name="5 adv_s H:shallow_auxiliary">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Sr" cat="S" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree>
  <tree name="6 adv_adj H:shallow_auxiliary">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Adjr" cat="adj" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree>
  <tree name="7 adv_adv H:shallow_auxiliary">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Advr" cat="adv" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree>
  <tree name="8 adv_v H:shallow_auxiliary">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Vr" cat="V" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree>
  <tree name="9 noun:agreement pnoun">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="noun:gender@16"/>
          </f>
          <f name="number">
            <var name="noun:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="yes" id="Np" cat="np" type="anchor">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="noun:gender@16"/>
            </f>
            <f name="number">
              <var name="noun:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree>
  <tree name="10 adj adj:agreement noun:shallow_auxiliary">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adj"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Nr" cat="N" type="std">
      <narg type="bot">
        <var name="noun:top@14">
          <fs>
            <f name="gender">
              <var name="adj:gender@16"/>
            </f>
            <f name="number">
              <var name="adj:number@18"/>
            </f>
          </fs>
        </var>
      </narg>
      <node adj="yes" id="adj" cat="adj" type="anchor">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="adj:gender@16"/>
            </f>
            <f name="number">
              <var name="adj:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree>
  <tree name="11 det:agreement n:agreement nc:agreement pnoun_as_cnoun">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="n:gender@16"/>
          </f>
          <f name="number">
            <var name="n:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="no" id="det" cat="det" type="subst">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="N" cat="N" type="std">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
        <narg type="bot">
          <fs>
            <f name="gender">
              <var name="nc:gender@16"/>
            </f>
            <f name="number">
              <var name="nc:number@18"/>
            </f>
          </fs>
        </narg>
        <node adj="yes" id="Nc" cat="np" type="anchor">
          <narg type="top">
            <fs>
              <f name="gender">
                <var name="nc:gender@16"/>
              </f>
              <f name="number">
                <var name="nc:number@18"/>
              </f>
            </fs>
          </narg>
        </node>
      </node>
    </node>
  </tree>
  <tree name="12 det:agreement n:agreement nc:agreement cnoun">
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="n:gender@16"/>
          </f>
          <f name="number">
            <var name="n:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="no" id="det" cat="det" type="subst">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="N" cat="N" type="std">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
        <narg type="bot">
          <fs>
            <f name="gender">
              <var name="nc:gender@16"/>
            </f>
            <f name="number">
              <var name="nc:number@18"/>
            </f>
          </fs>
        </narg>
        <node adj="yes" id="Nc" cat="nc" type="anchor">
          <narg type="top">
            <fs>
              <f name="gender">
                <var name="nc:gender@16"/>
              </f>
              <f name="number">
                <var name="nc:number@18"/>
              </f>
            </fs>
          </narg>
        </node>
      </node>
    </node>
  </tree>
</tag>
