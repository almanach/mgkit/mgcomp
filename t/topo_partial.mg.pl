/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_partial.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(partial).
node(partial,s).
node(partial,a).
node(partial,b).
node(partial,c).
node(partial,d).
father(partial,s,a).
father(partial,s,b).
father(partial,s,c).
father(partial,s,d).
precedes(partial,a,b).
precedes(partial,c,b).
precedes(partial,c,d).

answer([partial],[s,##([[c],[d]], [[a]]),[b]]).
answer([partial],[s,##([[c]], [[a]]),[b],[d]]).
