/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_foo.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(foo).
node(foo,a).
node(foo,b).
node(foo,c).
dominates(foo,a,b).
dominates(foo,a,c).
precedes(foo,b,c).

node(foo,d).
dominates(foo,c,d).
node(foo,e).
dominates(foo,c,e).
precedes(foo,d,e).

answer([foo],[a,[b],[c,[d],[e]]]).
