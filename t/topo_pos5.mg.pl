/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos5.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos5).
node(pos5,x).
node(pos5,a).
node(pos5,b).
node(pos5,c).
dominates(pos5,x,a).
dominates(pos5,x,b).
dominates(pos5,x,c).

nodefeature(pos5,a,[rank: last]).
%%nodefeature(pos5,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos5,a,[rank:single]). %% a is both first or last

nodefeature(pos5,c,[rank: last]).
nodefeature(pos5,b,[rank: last]).


answer([pos5],[x,[a,[c,[b]]]]).
answer([pos5],[x,[a,[b,[c]]]]).
answer([pos5],[x,[c,[a,[b]]]]).
answer([pos5],[x,[c,[b,[a]]]]).
answer([pos5],[x,[b,[c,[a]]]]).
answer([pos5],[x,[b,[a,[c]]]]). 
