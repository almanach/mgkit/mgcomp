/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_pos6.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of last and first
 * ----------------------------------------------------------------
 */

class(pos6).
node(pos6,x).
node(pos6,a).
node(pos6,b).
node(pos6,c).
dominates(pos6,x,a).
dominates(pos6,x,b).
dominates(pos6,x,c).

nodefeature(pos6,a,[rank:first]).
%%nodefeature(pos6,a,[rank:disj([first,last])]). %% a is first or last
%%nodefeature(pos6,a,[rank:single]). %% a is both first or last

%%nodefeature(pos6,c,[rank: first]).
%%nodefeature(pos6,b,[rank: first]).

answer([pos6],[x,[a],[b,[c]]]).
answer([pos6],[x,[a],[c,[b]]]).
answer([pos6],[x,[a],##([[b]], [[c]])]).
answer([pos6],[x,[a,[c]],[b]]).
answer([pos6],[x,[a,[b]],[c]]).
answer([pos6],[x,[a,[c,[b]]]]).
answer([pos6],[x,[a,[b,[c]]]]).
answer([pos6],[x,[a,##([[c]], [[b]])]]).
answer([pos6],[x,[c,[b,[a]]]]).
answer([pos6],[x,[c,[a,[b]]]]).
answer([pos6],[x,[c,[a],[b]]]).
answer([pos6],[x,[b,[c,[a]]]]).
answer([pos6],[x,[b,[a,[c]]]]).
answer([pos6],[x,[b,[a],[c]]]).
answer([pos6],[x,##([[b]], [[c,[a]]])]).
answer([pos6],[x,##([[c]], [[b,[a]]])]).

