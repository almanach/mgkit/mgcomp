/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007, 2008 by INRIA
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_brother1.mg.pl -- MGCOMP Test
 *
 * ----------------------------------------------------------------
 * Description
 *     Test usage of brother
 * ----------------------------------------------------------------
 */

class(brother1).
node(brother1,x).
node(brother1,a).
node(brother1,b).
node(brother1,c).
dominates(brother1,x,a).
dominates(brother1,x,b).
dominates(brother1,x,c).

%%father(brother1,x,a).
brother(brother1,a,b).
%brother(brother1,a,c).

%%father(brother1,y,a).
%%father(brother1,y,b).

answer([brother1],[x,##([[a,[c]]], [[b]])]).
answer([brother1],[x,##([[a]], [[b,[c]]])]).
answer([brother1],[x,##([[a]], ##([[b]], [[c]]))]).
answer([brother1],[x,[c,##([[a]], [[b]])]]). 
