/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  topo_deep.mg.pl -- A test for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

class(deep).
node(deep,a).
node(deep,b).
node(deep,c).
node(deep,d).
node(deep,e).
dominates(deep,a,b).
dominates(deep,b,d).
dominates(deep,a,c).
dominates(deep,c,e).
%%father(deep,c,e).
%%precedes(deep,a,d).
%%precedes(deep,b,c).
precedes(deep,d,e).
%%precedes(deep,c,b). 
%%dominates(deep,c,b).
%%precedes(deep,c,a).
%%precedes(deep,b,d).
%%needs(deep,foo).

answer([deep], [a,[c,[b,[d],[e]]]]).
answer([deep],[a,[c,[b,[d]],[e]]]).
answer([deep],[a,[b,[c,[d],[e]]]]).
answer([deep],[a,[b,[d],[c,[e]]]]).
answer([deep],[a,[b,[d]],[c,[e]]]).
