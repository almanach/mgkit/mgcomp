/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  toplevel.pl -- Small toplevel for Meta Grammar constraints
 *
 * ----------------------------------------------------------------
 * Description
 *
 * Add a small toplevel for MG constraints, in order to handle
 * integrity rules, filtering or conditionals
 *
 * ----------------------------------------------------------------
 */


 :-import{ file=>'format.pl',
	   preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-require
	'tools.pl',
	'fs.pl',
	'closure.pl'
	.

%% Trigger the insertion of a top level in mgcomp
'$dyalog_toplevel'.

:-rec_prolog
	mgcomp!toplevel/2,
	toplevel/1
	.

:-extensional registered_toplevel/1.
:-extensional usage_info/2.
:-extensional toplevel_loop/0.

registered_toplevel(mgcomp!toplevel/2).

mgcomp!toplevel(Class,(A,B)) :-
	mgcomp!toplevel(Class,A),
	mgcomp!toplevel(Class,B)
	.


mgcomp!toplevel(Class,(A;B)) :-
	(   mgcomp!toplevel(Class,A)
	;   
	    mgcomp!toplevel(Class,B)
	)
	.

mgcomp!toplevel(Class,(A->B;C)) :-
        ( mgcomp!toplevel(Class,A) ->
	    mgcomp!toplevel(Class,B)
	;   
	    mgcomp!toplevel(Class,C)
	)
	.

mgcomp!toplevel(Class,\+ A) :-
	\+ mgcomp!toplevel(Class,A)
	.

mgcomp!toplevel(_,true).

mgcomp!toplevel(Class,class(Class)).

mgcomp!toplevel(Class,super(G)) :-
	xsuper(Class,Super,NS),
	mgcomp!toplevel(Super,G)
	.

%% equivalent to super(class(Super))
mgcomp!toplevel(Class,terminal(Super)) :-
	xsuper(Class,Super,NS)
	.

mgcomp!toplevel(Class,base_ancestor(Base)) :-
	base_ancestor(Class,Base)
	.

mgcomp!toplevel(Class,node(N)) :-
	'$answers'( node_closure(Class,N) )
	.

mgcomp!toplevel(Class,= Res) :-
	recorded(derived_res(Class,= Res))
	.

mgcomp!toplevel(Class,+ Res) :-
	recorded(derived_res(Class,+ Res))
	.

mgcomp!toplevel(Class,- Res) :-
	recorded(derived_res(Class,- Res))
	.

mgcomp!toplevel(Class,N1=N2) :-
	repnode(Class,N1,RN),
	repnode(Class,N2,RN)
	.

mgcomp!toplevel(Class,N1 >> N2) :-
	repnode(Class,N1,RN1),
	repnode(Class,N2,RN2),
	'$answers'( dominates_closure(Class,RN1,RN2,1) )
	.

mgcomp!toplevel(Class,N1 >>+ N2) :-
	repnode(Class,N1,RN1),
	repnode(Class,N2,RN2),
	'$answers'( dominates_closure(Class,RN1,RN2,_) )
	.

mgcomp!toplevel(Class,N1 < N2) :-
	repnode(Class,N1,RN1),
	repnode(Class,N2,RN2),
	'$answers'( precedes_closure(Class,RN1,RN2,strict) )
	.

mgcomp!toplevel(Class,root(N)) :-
	repnode(Class,N,RN),
	'$answers'( root(Class,RN) )
	.

mgcomp!toplevel(Class,N:FS) :-
	repnode(Class,N,RN),
	'$answers'( nodefeature_closure(Class,RN,FS1) ),
	class_bindings(Class,Bindings),
	( var(FS) ->
	    FS1=FS
	;
	    fs!normalize(FS,XFS,Bindings),
	    fs!subsume(XFS,FS1,Bindings)
	)
	.

mgcomp!toplevel(Class,desc(FS)) :-
	'$answers'( description_closure(Class,FS1) ),
	class_bindings(Class,Bindings),
	( var(FS) ->
	    FS1=FS
	;   
	    fs!normalize(FS,XFS,Bindings),
	    fs!subsume(XFS,FS1,Bindings)
	)
	.

mgcomp!toplevel(Class,unify(FS1,FS2,FS3)) :-
	class_bindings(Class,Bindings),
	fs!safe_normalize(FS1,XFS1,Bindings),
	fs!safe_normalize(FS2,XFS2,Bindings),
	fs!unify(XFS1,XFS2,FS3,Bindings)
	.

mgcomp!toplevel(Class,subsume(FS1,FS2)) :-
	class_bindings(Class,Bindings),
	fs!safe_normalize(FS1,XFS1,Bindings),
	fs!safe_normalize(FS2,XFS2,Bindings),
	fs!subsume(XFS1,XFS2,Bindings)
	.

mgcomp!toplevel(Class,{G}) :-
	toplevel(G)
	.

:-extensional mgcomp!toplevel_clause/1.

mgcomp!toplevel(Class,A) :-
        (   mgcomp!toplevel_clause((A)) ;
            mgcomp!toplevel_clause((A :- Body)),
	    mgcomp!toplevel(Class,Body)
        )
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

toplevel((A,B)) :-
        toplevel(A),
        toplevel(B).

toplevel((A;B)) :-
        toplevel(A) ; toplevel(B).

toplevel((A->B;C)) :-
        toplevel(A) -> toplevel(B) ; toplevel(C).

toplevel(X=X).

toplevel(true).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-std_prolog mgcomp!toplevel_read_eval/2.

:-std_prolog mgcomp!toplevel_loop.

mgcomp!toplevel_read_eval(T,V) :-
        ( T==quit ->
            format('Leaving toplevel. Good bye!\n',[])
        ;   T = (?- G) ->
	    mgcomp!goal_analyze(G,XG),
%%	    format('%%Transformed ~w\n\t~w\n',[G,XG]),
	    neutral_class(Class),
            mgcomp!toplevel( Class, XG ),
            ( V = [] ->
                format('\ttrue\n',[])
            ;	domain(X,V),
                format('\t~w\n',[X])
            ),
            fail
        ;
	    mgcomp!goal_analyze(T,XT),
	    record_without_doublon( mgcomp!toplevel_clause(XT) ),
	    fail
        ).

mgcomp!toplevel_loop :-
        format('Entering mgcomp toplevel\n',[]),
        repeat( ( read_term([],T,V),
                    mgcomp!toplevel_read_eval(T,V),
                    T == quit
                ))
        .


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Goal Transformations

%% Experiemental !

%% A more user-friendly and compact notation is allowed in toplevel goals and clauses
%% which is then transformed in elementary goals.

%% For instance, one may write
%%    ?-display(root([id:'Nr'] >>+ [cat:'S']) >>+ [cat:vaux]),simple).
%% instead of
%%    ?-root(N),N:[id:'Nr'],N >>+ M, M:[cat:'S'], M >>+ P,P:[cat:vaux],
%%      tree(N,Tree),display(Tree,simple).

%% Basically, this is achieved by considering Node Expression
%% After transformation, each NExp return some code and a master node (marked with *)
%% NExp ::= VAR
%%        | node(*NExp)
%%        | root(*NExp)
%%        | *FS              {FS not a var,implicit master}
%%        | *NExp : FS
%%        | *NExp >> NExp
%%        | *NExp >>+ NExp
%%        | *NExp < NExp
%%        | *NExp >> NExp
%%        | *NExp = NExp

%% Node Expressions may appear as argument of some toplevel goals
%%          tree(<NExp>,Tree)    = tree(master_of(NExp))
%%          display(<NExp>,Mode) = tree(<NExp>,Tree), display(Tree,Mode)

:-std_prolog mgcomp!goal_analyze/2.
:-rec_prolog mgcomp!transform/2.

mgcomp!goal_analyze(A,B) :-
	( var(A) -> B=A
	;   mgcomp!transform(A,B) -> true
	;   A =.. [P|L],
	    mgcomp!goal_analyze_aux(L,LL),
	    B =.. [P|LL]
	)
	.

:-rec_prolog mgcomp!goal_analyze_aux/2.

mgcomp!goal_analyze_aux([],[]).
mgcomp!goal_analyze_aux([A|L],[XA|XL]) :-
	mgcomp!goal_analyze(A,XA),
	mgcomp!goal_analyze_aux(L,XL)
	.

mgcomp!transform({A},{A}).

mgcomp!transform(tree(N,Tree),XG) :-
	mgcomp!safe_node_transform(N,XN,RN),
	mgcomp!normalize((XN,tree(RN,Tree)),XG)
	.

mgcomp!transform(display(Node,Mode),XG) :-
	\+ var(Node),
	mgcomp!node_transform(Node,XNode,RNode),
	mgcomp!normalize((XNode,tree(RNode,Tree),display(Tree,Mode)),XG)
	.

mgcomp!transform(G,XG) :-
	\+ atom(G),
	mgcomp!node_transform(G,XG,_).

:-std_prolog mgcomp!safe_node_transform/3.

mgcomp!safe_node_transform(N,T,NR) :-
	( var(N) ->
	    T=node(N),
	    NR=N
	;   
	    mgcomp!node_transform(N,T,NR)
	)
	.

:-rec_prolog mgcomp!node_transform/3.

mgcomp!node_transform( root(N), T, NR ) :-
	mgcomp!safe_node_transform(N,XN,NR),
	mgcomp!normalize((XN,root(RN)),T)
	.

mgcomp!node_transform(N1 < N2, T,N1R) :-
	mgcomp!safe_node_transform(N1,XN1,N1R),
	mgcomp!safe_node_transform(N2,XN2,N2R),
	mgcomp!normalize((XN1,XN2,N1R < N2R),T)
	.

mgcomp!node_transform(N1 >> N2, T,N1R) :-
	mgcomp!safe_node_transform(N1,XN1,N1R),
	mgcomp!safe_node_transform(N2,XN2,N2R),
	mgcomp!normalize((XN1,XN2,N1R >> N2R),T)
	.

mgcomp!node_transform(N1 >>+ N2, T,N1R) :-
	mgcomp!safe_node_transform(N1,XN1,N1R),
	mgcomp!safe_node_transform(N2,XN2,N2R),
	mgcomp!normalize((XN1,XN2,N1R >>+ N2R),T)
	.

mgcomp!node_transform(N1 = N2, T,N1R) :-
	mgcomp!safe_node_transform(N1,XN1,N1R),
	mgcomp!safe_node_transform(N2,XN2,N2R),
	mgcomp!normalize((XN1,XN2,N1R=N2R),T)
	.

mgcomp!node_transform(N:F, T,NR) :-
	mgcomp!safe_node_transform(N,XN,NR),
	mgcomp!normalize((XN,N:F),T)
	.

mgcomp!node_transform(F::[_|_], T,N) :-
	mgcomp!normalize((node(N),N:F),T)
	.

mgcomp!node_transform(node(N), T,NR) :-
	( var(N) ->
	    T=node(N),NR=N
	;   
	    mgcomp!node_transform(N,XN,NR),
	    mgcomp!normalize((XN,node(N)),T)
	)
	.

mgcomp!node_transform(N,true,N) :- atom(N).

:-std_prolog mgcomp!normalize/2.

mgcomp!normalize(Code,XCode) :-
	( Code= (A,B) ->
	    mgcomp!normalize(B,XB),
	    mgcomp!normalize(A,XA),
	    ( XB=true -> XCode=XA
	    ;	XA = true -> XCode=XB
	    ;	XCode=(XA,XB)
	    )
	;   
	    XCode=Code
	)
	.
