/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2006, 2007, 2008, 2009, 2011, 2012, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  crossing.pl -- Class crossing algorithm
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-import{ file=>'format.pl',
	  preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-require
	'tools.pl',
	'fs.pl',
	'closure.pl',
	'toplevel.pl',
	'display.pl',
	'tree.pl',
	'check.pl'
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute inherited ressources for terminal classes

closure1 :-
	wait(( terminal(Class),
	       (   needs_closure(Class,_,_)
	       ;   provides_closure(Class,_,_)
	       )))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute neutral classes by crossing

:-light_tabular neutral_class/1.

%% A neutral class has only neutralized ressources
neutral_class(Class) :-
	derived_class(Class),
	\+ derived_res(Class,+ _, _),
	\+ derived_res(Class,- _, _),
	%% uncomment next line to avoid ressource-less terminal classes
	%%	(   derived_res(Class,= _,_) -> true ; fail),
	true
	.

crossing :-
	ressource_list(L),
	every(( terminal(C),
		register_derived_class(C))),
	every((domain(Res,L),
	       verbose('Round ~w\n',[Res]),
	       every(crossing(Res)),
%%	       clean_used_positive_classes,
	       true
	      ))
	.

:-std_prolog display_res/1.

display_res(C) :-
	format('Class ~w Resources: ',[C]),
	every(( derived_res(C,Res,NS),
		format(' ~w',[NS:Res]))),
	nl
	.

:-std_prolog crossing/1.

crossing(Res) :-
%%	format(2,'processing res=~w\n',[Res]),
	every((

	       rev_derived_res(-Res,NS,C2),
	       \+ recorded( newly_created(C2) ),
	       %% select first possible Res for C2
	       %% other ones will be tried on next round
	       \+ ( rev_derived_res(-Res,_NS,C2),
		    _NS @< NS ),

	       every((
		      rev_derived_res(+Res,NS1,C1),
		      \+ recorded( newly_created(C1) ),
		      %%	       \+ recorded( removed(C1) ),
		      %%	       \+ recorded( removed(C2) ),
		      %% display_res(C1),
		      %% display_res(C2),
		      verbose('Combining C1=~w C2=~w\n',[C1,C2]),
		      (   NS1==[]
		      xor error('No allowed namespace for positive resources (res +~w!~w in ~w)\n',
				[NS1,Res,C1])),
		      \+ (derived_res(C1,_Res,_NS),derived_res(C2,_Res,_NS)),
		      \+ shared_neutral_res(C1,C2),
		      \+ shared_neutral_res(C2,C1),
		      register_derived_class(C1,C2,NS),
		      record_without_doublon(again)
		     )),

	       %% at this point, we can get rid of C2
	       record( fully_used(C2) ),
	       try_clean_class(C2),
	       true
	      
	      )),
	( %% recorded(again)
	  rev_derived_res(-Res,NS,_C),
	  recorded( newly_created(_C) )
	->
	  safe_erase(again),
	  abolish( newly_created/1 ),
%	  verbose('TRY AGAIN FOR RES ~w\n',[Res]),
	  crossing(Res)
	;
	  safe_erase(again),
	  abolish( newly_created/1 )
	),
	every((
	       rev_derived_res(+Res,NS1,C1),
	       ((\+ recorded( descendant(C1,_) )) xor \+ super(_,_ : C1)),
	      free_class(C1)
	      )),
	clean_index,
	true
	.

:-std_prolog abolish/1.

abolish(F/N) :- '$interface'( 'Abolish'(F:term,N:int), [return(none)]).

:-std_prolog shared_neutral_res/2.

shared_neutral_res(C1,C2) :-
	derived_res(C1,=Res,NS),
	(derived_res(C2,+Res,NS) xor derived_res(C2,-Res,NS))
	.
	     
:-rec_prolog sorted_append/4.

sorted_append([],L,L,_).
sorted_append([A|X],Y,Z,NS) :-
	sorted_append(X,Y,Z1,NS),
	deep_module_shift(A,NS,XA),
	sorted_list(XA,Z1,Z)
	.

:-rec_prolog sorted_list/3.

sorted_list(A,[],[A]).
sorted_list(A,[B|L],[A,B|L]) :- A @< B.
sorted_list(A,[B|L],[B|LL]) :- A @> B, sorted_list(A,L,LL).

:-std_prolog try_clean_class/1.

try_clean_class(Class) :-
	safe_erase( derived_class(Class) ),
	derived_res(Class,Res,NS),
	safe_erase( derived_res(Class,Res,NS) ),
	safe_erase( rev_derived_res(Res,NS,Class) ),
	( \+ super(_,_:Class) ->
	  free_class(Class),
	  true
	; %% super(_,_:Class),
	  recorded( tried_closure(Class) ),
	  \+ ( recorded(descendant(Class,_Class)),
	       \+ recorded( deleted(_Class) ),
	       \+ recorded(tried_closure(_Class))
	       ),
	  free_class(Class),
	  true
	)
	.

:-std_prolog clean_used_positive_classes/0.

clean_used_positive_classes :-
	every(( 
		recorded(F::used(+Class)),
		safe_erase(F),
		record( fully_used(Class) ),
		\+ recorded( deleted(Class) ),
		safe_erase( derived_class(Class) ),
		derived_res(Class,Res,NS),
		safe_erase( derived_res(Class,Res,NS) ),
		safe_erase( rev_derived_res(Res,NS,Class) ),
		( \+ super(_,_:Class) ->
		  free_class(Class),
		  true
		; % super(_,_:Class),
		  recorded( tried_closure(Class) ) ->
		  \+ ( recorded(descendant(Class,_Class)),
		       \+ recorded( deleted(_Class) ),
		       \+ recorded(tried_closure(_Class))
		     ),
		  format('deleting ~w\n',[Class]),
		  free_class(Class),
		  true
		;
		  true
		),
		verbose('pos erasing class=~w\n',[Class])
	      ))
	.

:-extensional rev_derived_res/3.
:-extensional derived_res/3.
:-std_prolog register_derived_res/3.

register_derived_res(C,Res,NS) :-
%%	verbose('Register res ~w!~w for ~w\n',[NS,Res,C]),
	record_without_doublon( derived_res(C,Res,NS)),
	record_without_doublon( rev_derived_res(Res,NS,C))
	.

:-light_tabular ressource_list/1.

ressource_list(L) :-
	%% Compute the ressource list
	mutable(M,[]),
	every(( ressource(Res),
		mutable_read(M,_L),
		mutable(M,[Res|_L]) )),
	%% Compute the number of occurrences of each ressource
	every(( terminal(C),
		needs_closure(C,Res,_),
		update_counter( -Res, _))),
	every(( terminal(C),
		provides_closure(C,Res,_),
		update_counter( +Res, _))),
	mutable_read(M,LL),
	%% Sort the ressource list by depth and increasing number of occurrences
	sort_list(LL,L),
	length(L,L1),
	verbose('** Ressources ~w: ~w\n',[L1,L])
	.

:-light_tabular res_depth/2.

res_depth(Res,Depth) :-
	mutable(M,0),
	every(( terminal(C),
		provides_closure(C,Res,_),
		needs_closure(C,Res2,_),
		res_depth(Res2,Depth2),
		mutable_read(M,V),
		VV is max(V,1+Depth2),
		mutable(M,VV))),
	mutable_read(M,Depth),
	%% When basic res. (Depth=0), check that we do not have some cycle
	(   Depth == 0,
	    terminal(C),
	    provides_closure(C,Res,_),
	    needs_closure(C,Res2,_) ->
	    error('Cyclic ressource ~w found in ~w\n',[Res,C])
	;   
	    true
	),
	verbose('Res depth ~w=~w\n',[Res,Depth])
	.

:-rec_prolog sort_list/2.

sort_list([],[]).
sort_list([A|L],LL) :-
	sort_list(L,L1),
	sort_list_aux(A,L1,LL).

:-rec_prolog sort_list_aux/3.

sort_list_aux(A,[],[A]).
sort_list_aux(A,[B|L],LL) :-
	value_counter(+A,PA),
	value_counter(+B,PB),
	value_counter(-A,MA),
	value_counter(-B,MB),
	res_depth(A,DA),
	res_depth(B,DB),
	( DA < DB ->
	    LL=[A,B|L]
	;   DA==DB,min(PA,MA) =< min(PB,MB) ->
	    LL=[A,B|L]
	;
	    LL=[B|L1],
	    sort_list_aux(A,L,L1)
	)
	.

:-std_prolog gen_class_name/2.

gen_class_name(C,Name) :-
	update_counter(class,N),
%%	name_builder('class@~w',[N],Name),
	Name = N,
	record_without_doublon(class2name(C,Name)),
	record_without_doublon(name2class(Name,C))
	.
	
:-std_prolog register_derived_class/1.
:-std_prolog register_derived_class/3.
:-extensional derived_class/1.

%% Register a derived class from a terminal one
register_derived_class(C) :-
	gen_class_name([C],Class),
	every(( needs_closure(C,Res,NS),
		( provides_closure(C,Res,NS) ->
		    %% Beware of terminal classes with neutralizing ressources
		    register_derived_res(Class,=Res,NS)
		;   
		    register_derived_res(Class,-Res,NS)))),
	every(( provides_closure(C,Res,NS),
		\+ needs_closure(C,Res,NS),
		register_derived_res(Class,+Res,NS) )),
	record_without_doublon( derived_class(Class) ),
	( handle_class(Class) xor true)
	.

%% Register a derived class from the crossing of C1(+) and C2(-)
%% shifting C1 into namespace NS
register_derived_class(C1,C2,NS) :-
	verbose('Trying Registering class c1=~w c2=~w ns=~w\n',[C1,C2,NS]),
	record_without_doublon( used(+C1) ),
	record_without_doublon( used(-C2) ),
	%% Build a name for the new class
	name2class(C1,XC1),
	name2class(C2,XC2),
	sorted_append(XC1,XC2,XC,NS),
%	verbose('\t->1 class=~w\n',[C]),
	\+ class2name(XC,_),
	gen_class_name(XC,C),
%%	format('=> assign tmp class id ~w\n',[C]),
	record_without_doublon( derived_class(C) ),
	record( super(C,NS:C1) ),
	record( super(C,[]:C2) ),
	verbose('\t->1x class=~w=>~w\n\tC1=~w=>~w\n\tC2=~w=>~w\n',[C,XC,C1,XC1,C2,XC2]),
%	record( derived_class(C,C1,C2) ),
	%% Neutralize pairs of ressources between C1 and C2
	consume_res(NS:C1,[]:C2,C),
	consume_res([]:C2,NS:C1,C),
	verbose('\t->2 class=~w=>~w\n\tC1=~w=>~w\n\tC2=~w=>~w\n',[C,XC,C1,XC1,C2,XC2]),
	%% Inherit other ressources
	every((domain(Nx:Cx,[NS:C1,[]:C2]),
	       derived_res(Cx,+Res,_NS),
	       deep_module_shift(_NS,Nx,_XNS),
	       \+ derived_res(C,=Res,_XNS),
	       register_derived_res(C,+Res,_XNS))),
	every((domain(Nx:Cx,[NS:C1,[]:C2]),
	       derived_res(Cx,=Res,_NS),
	       deep_module_shift(_NS,Nx,_XNS),
	       register_derived_res(C,=Res,_XNS))),
	record( newly_created(C) ),
	record( descendant(C1,C) ),
	record( descendant(C2,C) ),
	verbose('=> done class registering ~w\n',[C]),
	(handle_class(C) xor true),
	true
	.

:-std_prolog consume_res/3.

consume_res(N1:C1,N2:C2,C) :-
	every(( derived_res(C1,-Res,NS1),
		deep_module_shift(NS1,N1,XNS1),
		( NS1=N2,derived_res(C2,+Res,NS2) ->
		    register_derived_res(C,=Res,XNS1)
		;   
		    register_derived_res(C,-Res,XNS1)
		)))
	.


:-std_prolog mutable_free/1.

mutable_free(Mutable) :-
	'$interface'( 'DyALog_Mutable_Free'( Mutable: ptr ), [] )
	.

:-std_prolog safe_erase/1.

safe_erase(G) :-
	every(( recorded(G,Addr),
		delete_address(Addr)
	      ))
	.


:-std_prolog clean_index/0.

%% this function is unsafe
%% it should not be called when there is some indexing traversal underway
clean_index :-
	'$interface'( 'DyALog_Clean_Indexing',[return(none)]).

:-std_prolog try_free_class/1.

try_free_class(Class) :-
	recorded( tried_closure(Class) ),
	every(( super(Class,_:Super),
		try_free_class(Super)
	      )),
	recorded( fully_used(Class ) ),
	\+ recorded( deleted(Class) ),
	\+ ( recorded( descendant(Class,_Class) ),
	     \+ recorded( deleted(_Class) ),
	     \+ recorded( tried_closure(_Class) )
	   ),
	free_class(Class)
	.

:-std_prolog free_class/1.

%% /*
free_class(Class) :-
	verbose('Free class ~w\n',[Class]),
	\+ recorded( deleted(Class) ),
	record( deleted(Class) ),
	every((	super(Class,_:Super),
		try_free_class(Super)
	      )),
	every(( '$answers'(class_bindings(Class,Bindings)),
		%%		mutable(Bindings,[]),
		mutable_free(Bindings),
		true
	      %%		    mutable(Bindings,[])
	      )),
	every(
	      (	 
	      %% ;	  safe_erase(derived_class(Class))
	      name2class(Class,XClass),safe_erase(class2name(XClass,_))
	      ;	  safe_erase(name2class(Class,_))
	      ;   safe_erase(rev_derived_res(_,_,Class))
	      ; every(( recorded( super(Class, _ : Super )),
			safe_erase( descendant(Super,Class) )
		      ))
	      ;	  safe_erase( super(Class,_ ) )
	      ;	  safe_erase( derived_res(Class,_,_))
	      ;	  safe_erase( repnode(Class,_) )
	      ; safe_erase( empty_nodeguard(Class,_,_))
	      ; safe_erase( normalized_guard(Class,_,_,_) )
	      ; safe_erase( normalized_reducer(Class,_,_,_) )
	      ;	  safe_erase( normalized_reducer(Class,_,_) )
	      ;	  safe_erase( normalized_reducer(Class,_) )
	      ;	  safe_erase( binding(Class,_,_) )
	      ;	  safe_erase( node_feature(Class,_,_) )
	      ;	  safe_erase( nodefeature(Class,_,_) )
	      ;	  safe_erase( description(Class,_) )
	      ;	  safe_erase( anonymous(Class,_,_) )
	      ; safe_erase( dump!map(Class,_) ) %to be checked
	      ; safe_erase( tmp_nodeguard(Class,_,_,_) )
	      ; safe_erase( nodeguard_fixpoint(Class,_) )
	      ; safe_erase( derived_class(Class) )
	      ; safe_erase( failed(Class) )
	      ;	  erase_relation( needs_closure(Class,_,_) )
	      ;	  erase_relation( provides_closure(Class,_,_) )
	      ;	  erase_relation( node_closure(Class,_) )
	      ;	  erase_relation( vnode_closure(Class,_) )
	      ;	  erase_relation( equals_closure(Class,_,_) )
	      ;	  erase_relation( equals_closure_aux(Class,_,_,_) )
	      ;	  erase_relation( repnode(Class,_,_) )
	      ;	  erase_relation( dominates_closure(Class,_,_,_) )
	      ;	  erase_relation( ancestor(Class,_,_) )
	      ;	  erase_relation( pfather(Class,_,_,_) )
	      ;	  erase_relation( root(Class,_) )
	      ;	  erase_relation( precedes_closure(Class,_,_,_) )
	      ;	  erase_relation( brother_closure(Class,_,_) )
	      ;	  erase_relation( nodefeature_closure(Class,_,_) )
	      ;	  erase_relation( description_closure(Class,_) )
	      ;	  erase_relation( class_bindings(Class,_) )
	      ;	  erase_relation( nodeguard_closure(Class,_,_,_) )
	      ;	  erase_relation( nodeguard_closure(Class,_,_,_,_) )
	      ;	  erase_relation( node_optional(Class,_,_,_) )
	      ;	  erase_relation( reducer_closure(Class,_) )
	      ;	  erase_relation( analyze_equation(Class,_,_,_) )
	      ;	  erase_relation( anonymous_closure(Class,_,_) )
	      ;	  erase_relation( name_anonymous(Class,_,_) )
	      ;	  erase_relation( node_rel(Class,_,_,_) )
	      ;	  erase_relation( tree(Class,_,_) )
	      ;	  erase_relation( tree(Class,_,_,_,_) )
	      ;	  erase_relation( node_insert(Class,_,_,_,_) )
	      ;	  erase_relation( is_last(Class,_) )
	      ;	  erase_relation( is_first(Class,_) )
	      ;   erase_relation( neutral_class(Class) )
	      ; erase_relation( base_class_dump(_,Class) )
	      ; erase_relation( closure_class(Class) )
	      ; recorded( ('*CONT*' :> closure(_, '{}'('call_equals_closure/3'(Class,_N),Class,_N,_,_,_,_))), Addr ),
		 delete_address(Addr)
	      )
	     ),
%%	format('End cleaning ~w\n',[Class]),
%%	table_analysis(Class),
	true
	.
%%	*/

:-std_prolog table_analysis/1.

table_analysis(Class) :-
	every(( recorded( tablecount(Pred,M) ),
		mutable(M,0)
	      )),
	every((
	       recorded( '*RITEM*'(Call,Ret) ),
	       Call =.. [Pred|_],
	       (recorded(tablecount(Pred,M))
	       xor mutable(M,0),
		record(tablecount(Pred,M))
	       ),
	       mutable_read(M,V),
	       W is V + 1,
	       mutable(M,W)
	      )),
	format('table analysis ~w\n',[Class]),
	every(( recorded( tablecount(Pred,M) ),
		mutable_read(M,V),
		format('\t~w: ~w\n',[Pred,V])
	      )),
	true
	.

%% free_class(_).
	
:-std_prolog check_not_failed/2.

check_not_failed(Class,Name) :-
	( recorded( failed(Class) ) ->
	    verbose('*** Failed class ~w cause=~w\n',[Class,Name]),
%%	    safe_erase( failed(Class) ),
	    free_class(Class),
	    fail
	;   
	    true
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute all closure relations for crossed classes

:-std_prolog find_dumped_class/2.

find_dumped_class(XClass,DClass) :-
	Flat =.. [flat|XClass],
	recorded(dump!alive(Flat,DClass))
	.

closure_class(C) :-
	%% reusing dumped neutral class
	neutral_class(C),
	name2class(C,XClass),
	find_dumped_class(XClass,DClass),
	( recorded(dump2class(DClass,_C)) ->
	  error('Something strange: ~w and ~w both map to dump class ~w\n',[C,_C,DClass])
	;
	  record(dump2class(DClass,C)),
	  record(class2dump(C,DClass)),
	  verbose('Mapping class ~w to dumped class ~w\n',[C,DClass]),
	  verbose_dump('\n%% Mapping class ~w to dumped class ~w\n',[C,DClass]),
	  true
	),
	record_without_doublon( tried_closure(C) ),
	every((dump!fact(DClass,dump!repnode(DClass,Node)),
	       record_without_doublon(repnode(C,Node)))),
	class_bindings(C,_),
	description_closure(C,_),
	every(reducer_closure(C,_)),
	every((anonymous_closure(C,_,_))),
	every(( repnode(C,N), brother_closure(C,N,M))),
	every(( repnode(C,N), dominates_closure(C,N,M,_))),
	every((repnode(C,N), repnode(C,M1), dominates_closure(C,M1,N,1),
	       repnode(C,M2), dominates_closure(C,M2,N,1))),
	every((repnode(C,N), precedes_closure(C,N,M,K))),
	every((repnode(C,N), pfather(C,N,_N,_K))),
	every((repnode(C,N), nodefeature_closure(C,N,_))),
	every((repnode(C,N), root(C,N))),
	every((repnode(C,N), domain(Rel,guardrel[]),
	       nodeguard_closure(C,N,Rel,_))),
	verbose('Restored dumped class ~w completed\n',[DClass]),
	verbose_dump('%% Restored dumped class ~w completed as ~w\n',[DClass,C]),
%%	format(2,'Restored dumped class ~w completed\n',[DClass]),
	true
	.

/*
closure_class(C) :-
	neutral_class(C),
	name2class(C,XC::[_|_]),
	\+ ( domain(XBase,XC),
	     atom_module(XBase,_,Base),
	     modified_base_class(Base)
	   ),
	format(2,'failed class ~w\n',[C]),
	fail
	.
*/

:-light_tabular closure_class/1.

closure_class(C) :-
	%% format('Start closure class ~w\n',[C]),
	%% We recompute only if some base class has been modified (from last restore)
	%% or if it is not a neutral class
	(  \+ neutral_class(C)
	xor name2class(C,XC::[_|_]),
	   domain(XBase,XC),
	   atom_module(XBase,_,Base),
	   modified_base_class(Base)
%%	xor modified_class(C)
	xor fail
	),
	record_without_doublon( tried_closure(C) ),
%%	format(2,'Closure class ~w\n',[C]),
	every((xsuper(C,Super,_),
	       check_failed(C,\+ recorded( failed(Super) )),
	       check_failed(C,\+ recorded( deleted(Super) ))
	      )),
	check_not_failed(C,super_failed),
	every((xsuper(C,Super,_),
	       check_failed(C,closure_class(Super)))),
	check_not_failed(C,super_failed2),
%%	format('Done closure supers ~w\n',[C]),
%%	format('Read equations ~w\n',[C]),
	class_equations(C),
%%	format('Read guards ~w\n',[C]),
	class_nodeguards(C),
%%	format('Read reducers ~w\n',[C]),
	class_reducers(C),
%%      format('Set bindings ~w\n',[C]),
	class_bindings(C,_),
%%	format('Description closure ~w\n',[C]),
	description_closure(C,_),
%%	format('Reducer closure ~w\n',[C]),
	every(reducer_closure(C,_)),
%%	format('Repnode closure ~w\n',[C]),
	every(safe_repnode(C)),	
%%	format('Anonymous closure ~w\n',[C]),
	every((anonymous_closure(C,_,_))),
%%	format( 'Brother closure ~w\n',[C]),

	
	every(( repnode(C,N),
		\+ recorded( failed(C) ),
		brother_closure(C,N,M),
%%		format('Brother ~w ~w < ~w\n',[C,N,M]),
		check_failed(C,N \== M)
	      )),
%%	format('Dominate closure ~w\n',[C]),
	every((repnode(C,N),
	       dominates_closure(C,N,M,_),
	       %% format('Dominate ~w ~w >> ~w\n',[C,N,M]),
	       %% Check that N is not its own ancestor
	       check_failed(C, N \== M )
	      )),
	( neutral_class(C) ->
	  every((repnode(C,N),
		 repnode(C,M1),
		 \+ recorded( failed(C) ),
		 dominates_closure(C,M1,N,1),
		 repnode(C,M2),
		 \+ recorded( failed(C) ),
		 dominates_closure(C,M2,N,1),
		 %% Check that N doesn't have two fathers
		 %% only need to be tested for neutral classes
		 %% for other classes, futur aliasing may occur
		 check_failed(C, M1 == M2 )
		))
	  ;
	  true
	),
	check_not_failed(C,own_ancestor),
%	format('Precede closure ~w\n',[C]),
	every((repnode(C,N),
	       \+ recorded( failed(C) ),
	       precedes_closure(C,N,M,K),
%	       format('Class ~w PRECEDES ~w < ~w [~w]\n',[C,N,M,K]),
	       %% Check that N does not precede itself
	       check_failed(C, (N \== M ; K = dom))
	       )),
	check_not_failed(C,precedes_itself),
	%% cleaning
	% every(('$answers'(precedes_closure(C,N,M,strict)),
	%        '$answers'(precedes_closure(C,N,M,dom)),
	%        tab_item_term(O,precedes_closure(C,N,M,dom)),
	%        recorded(O,Addr),
	%        delete_address(Addr))),
% 	every(( '$answers'( precedes_closure(C,N,M,K) ),
% 		format('PRECEDES ~w < ~w [~w]\n',[N,M,K]))),
%	format('PFather closure ~w\n',[C]),
	( neutral_class(C) ->
	  every((repnode(C,N),
		 pfather(C,N,_N,_K),
%		 format('pfather ~w ~w ~w ~w\n',[C,N,_N,_K]),
		 true
		))
	;
	  true
	),
%	check_not_failed(C,pfather)
% 	every(( '$answers'( pfather(C,N,M,K) ),
% 		format('PFATHER ~w >> ~w [~w]\n',[N,M,K]))),

%	format('Nodefeature closure ~w\n',[C]),
	every((repnode(C,N),
	       nodefeature_closure(C,N,_))),
	check_not_failed(C,node_features),
%	format('Root ~w\n',[C]),
	every((repnode(C,N),
	       root(C,N),
%%	       format('root node ~w ~w\n',[C,N]),
	       true
	      )),
	check_not_failed(C,root),
	( neutral_class(C) ->
	    check_filtering(C)
	;   
	    true
	),
	%% to need to reduce guard unless for kept successful classes
	%% guards do not add bindings
%	format('Nodeguard closure ~w\n',[C]),
	nodeguard_closure_fixpoint(C,0),
	check_not_failed(C,guards),
	every((repnode(C,N),
	       domain(Rel,guardrel[]),
	       \+ recorded( failed(C) ),
	       nodeguard_closure(C,N,Rel,_))),
	check_not_failed(C,guards),
%	format('Done closure ~w\n',[C]),
	true
	.

:-std_prolog class_bindings_reduce/1.

class_bindings_reduce(C) :-
	mutable(Closure,[]),
	class_bindings(C,Bindings),
	every( ( ( '$answers'(description_closure(C,FS))
		 ;   '$answers'(nodefeature_closure(C,_,FS))
		 ;   binding(C,FS1,FS2),
		     domain(FS,[FS1,FS2])
		 ;   '$answers'(nodeguard_closure(C,_,_,Guard)),
		     Guard \== true,
		     guard_follow(Guard,FS)
		 ),
		 fs!closure(FS,Bindings,Closure))),
	mutable_read(Closure,Vars),
	mutable_read(Bindings,L),
%%	format('Reduce bindings class=~w\n\tvars=~w\n\tbindings=~w\n',[C,Vars,L]),
	fs!reduce_bindings(Vars,Bindings),
%%	mutable_read(Bindings,LL),
%%	format('-->\n\tbindings=~w\n\n',[LL]),
	true
	.

:-rec_prolog guard_follow/2.

guard_follow(L::[_|_],FS) :- domain(G,L),guard_follow(G,FS).
guard_follow(guard(_,FS1,FS2),FS) :-
	domain(FS,[FS1,FS2]).
guard_follow(and(L),FS) :- domain(U,L),guard_follow(U,FS).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Filtering

:-std_prolog check_filtering/1.

check_filtering(Class) :-
	(   \+ recorded( filtering(_) )
	xor recorded( filtering(Cond) ),
	    mgcomp!toplevel(Class,Cond))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check restored classes from dump

%:-light_tabular modified_class/1.

:-std_prolog modified_class/1.

modified_class(Class) :-
	\+ class(Class),
	name2class(Class,XClass),
	( domain(XC,XClass),
	  atom_module(XC,_,C),
	  modified_base_class(C) ->
	  verbose_dump('%% Modified class ~w\n',[Class]),
	  true
	;
	  fail
	)
	.

:-light_tabular modified_base_class/1.

modified_base_class(Class) :-
	class(Class),
	%% A class has been added
	\+ dump!fact(Class,class(Class)),
	verbose_dump('%% Modified class ~w\n',[Class]),
	true
	.

modified_base_class(Class) :-
	class(Class),
	dump!fact(Class,class(Class)),
	( %% some ancestor class for both Class and dumped Class is modified
	  super(Class,Super),
	  dump!fact(Class,super(Class,Super)),
	  modified_base_class(Super)
	xor
	  dump!fact(Class,dump!dependency(Class,DepClass,_)),
	  modified_base_class(DepClass)
	xor %% some fact is modified
	  domain(F/N,
		 [ class/1,
		   super/2,
		   needs/2,
		   provides/2,
		   description/2,
		   node/2,
		   nodefeature/3,
		   guard/4,
		   father/3,
		   dominates/3,
		   precedes/3,
		   equals/3,
		   equation/3,
		   anonymous/3,
		   reducer/2
		 ]),
	  functor(Fact,F,N),
	  Fact =.. [_,Class|_],
	  ( %% A fact has been added since dump
	    recorded(Fact),
	    \+ dump!fact(Class,Fact)
	  xor
	    %% A fact has been removed since dump
            dump!fact(Class,Fact),
	    \+ recorded(Fact)
	  xor
	    fail )
	xor
	  fail
	),
	verbose('Modified class ~w\n',[Class]),
	verbose_dump('%% Modified class ~w\n',[Class]),
	true
	.
