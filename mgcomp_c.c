/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2003, 2008, 2010, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  mgcomp_c.c 
 *  \brief C support for mgcomp
 *
 * ----------------------------------------------------------------
 * Description
 *  Fast implementation of some predicates used in mgcomp,
 *  such as fs!unify
 * ----------------------------------------------------------------
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "libdyalog.h"
#include "builtins.h"

static
void XML_atom_print_aux( StmInf *stream, fol_t A)
{
    fol_t module = FOLSMB_MODULE(A);
    if (!FOLNILP(module)) {
        XML_atom_print_aux(stream,module);
        Stream_Putc(':',stream);
    }
    Stream_Puts(FOLSMB_NAME(A),stream);    
}

extern Bool XML_atom_print( int stm, sfol_t SA );

extern char *newline_start;

static void
XML_Indent( StmInf *stream )
{
    Stream_Putc('\n',stream);
    Stream_Puts(newline_start,stream);
}

static void
XML_Attr_Print(StmInf *stream, fol_t Attr, fkey_t Sk(Attr), int in_list) 
{
    Deref(Attr);
    if (FOLNILP(Attr)) {
        return;
    } else if (FOLPAIRP(Attr)) {
        XML_Attr_Print(stream,FOLPAIR_CAR(Attr),Sk(Attr),1);
        Attr=FOLPAIR_CDR(Attr);
        Deref(Attr);
        for (; FOLPAIRP(Attr); ) {
            Stream_Putc(' ',stream);
            XML_Attr_Print(stream,FOLPAIR_CAR(Attr),Sk(Attr),1);
            Attr=FOLPAIR_CDR(Attr);
            Deref(Attr);
        }
    } else if (FOLCMPP(Attr)
               && FOLCMP_ARITY(Attr) == 1
               && !strcmp(FOLCMP_NAME(Attr),"disj") ) {
        Attr=FOLCMP_REF(Attr,1);
        XML_Attr_Print(stream,Attr,Sk(Attr),in_list);
    } else if (FOLSMBP(Attr)) {
        if (strcmp(FOLSMB_NAME(Attr),"\"") == 0) {
            dyalog_fprintf(stream,"&quot;");
        } else if (in_list && strchr(FOLSMB_NAME(Attr),' ')) {
            Stream_Puts("'",stream);
            XML_atom_print_aux(stream,Attr);
            Stream_Puts("'",stream);
        } else
            XML_atom_print_aux(stream,Attr);
    } else {
        dyalog_fprintf(stream,"%&s",Attr,Sk(Attr));  
    }
}

Bool
XML_my_start_element_print( int stm, sfol_t SElt, sfol_t SAttr, Bool empty)
{
    StmInf *stream = (StmInf *)INT_TO_STM(stm);
    fol_t Elt = SElt->t;
    fkey_t Sk(Elt) = SElt->k;
    fol_t Attr = SAttr->t;
    fkey_t Sk(Attr) = SAttr->k;
    Deref_And_Fail_Unless(Elt,FOLSMBP(Elt));
    XML_Indent(stream);
    Stream_Putc('<',stream);
    XML_atom_print_aux(stream,Elt);
    Deref(Attr);
    for(;FOLPAIRP(Attr);) {
        fol_t A=FOLPAIR_CAR(Attr);
        fkey_t Sk(A) = Sk(Attr);
        Deref(A);
        if (!FOLCMPP(A) && FOLCMP_ARITY(A) != 2)
            Fail;
        else {
            fol_t X=FOLCMP_REF(A,1);
            fkey_t Sk(X) = Sk(A);
            fol_t V=FOLCMP_REF(A,2);
            fkey_t Sk(V) = Sk(A);
            Deref(X);
            if (!FOLSMBP(X))
                Fail;
            Stream_Putc(' ',stream);
            XML_atom_print_aux(stream,X);
            Deref(V);
            Stream_Putc('=',stream);
            if (FOLSMBP(V)  && strchr(FOLSMB_NAME(V),'"')) {
                Stream_Putc('\'',stream);
                XML_atom_print_aux(stream,V);
                Stream_Putc('\'',stream);
            } else {
                Stream_Putc('\"',stream);
                XML_Attr_Print(stream,V,Sk(V),0);
                Stream_Putc('\"',stream);
            }
        }
        Attr = FOLPAIR_CDR(Attr);
        Deref(Attr);
    }
    if (empty)
        dyalog_fprintf(stream,"/");
    dyalog_fprintf(stream,">");
    Succeed;
}

Bool
XML_my_close_element_print( int stm, sfol_t SElt)
{
    StmInf *stream = (StmInf *)INT_TO_STM(stm);
    fol_t Elt = SElt->t;
    fkey_t Sk(Elt) = SElt->k;
    Deref_And_Fail_Unless(Elt,FOLSMBP(Elt));
    XML_Indent(stream);
    Stream_Puts("</",stream);
    XML_atom_print_aux(stream,Elt);
    Stream_Putc('>',stream);
    Succeed;
}
