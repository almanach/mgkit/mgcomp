/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  closure.pl -- Compute all needed closure relations
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-import{ file=>'format.pl',
	  preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-require
	'tools.pl',
	'fs.pl'
	.

%% From crossing.pl (to avoid recursive module dependencies)
:-light_tabular neutral_class/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ISA relation
 
:-std_prolog xsuper/3.

xsuper(Class,Super,NS) :-
	( name2class(Class,XClass) ->
	  (	XClass = [Super], NS = []
	  xor super(Class,NS:Super)
	  )
	%%	    domain(XSuper,XClass),
	%%	    atom_module(XSuper,NS,Super)
	;   super(Class,Super),
	    NS=[]
	)
	.

:-std_prolog xsuper/4.

xsuper(Class,Super,NS,From) :-
	( name2class(Class,XClass) ->
	  (	XClass = [Super], NS = []
	  xor super(Class,NS:Super),
		( recorded(deleted(Super)) ->
		  format('using deleted class ~w coming from ~w ~w\n',[Super,From,Class])
		;
		  true
		)
	  )
	%%	    domain(XSuper,XClass),
	%%	    atom_module(XSuper,NS,Super)
	;   super(Class,Super),
	    NS=[]
	)
	.


%% Checking if a base_class is an ancestor of a class

:-std_prolog base_ancestor/2.

base_ancestor(Class,Base_Ancestor) :-
	( name2class(Class,XClass) ->
	    domain(Terminal,XClass)
	;   
	    Terminal = Class
	),
	base_ancestor_aux(Terminal,Base_Ancestor)
	.

:-light_tabular base_ancestor_aux/2.

base_ancestor_aux(Base_Class,Base_Class).

base_ancestor_aux(Base_Class,Base_Ancestor) :-
	super(Base_Class, Super),
	(   Super = Base_Ancestor
	xor base_ancestor_aux(Super,Base_Ancestor)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Terminal classes

:-light_tabular terminal/1.

terminal(Class) :-
	class(Class),
	\+ super(_,Class),
	\+ disabled_base_class(Class)
	.

:-extensional disabled_class/1.

:-light_tabular disabled_base_class/1.

disabled_base_class(Class) :-
	disabled_class(Class)
	.

disabled_base_class(Class) :-
	super(Class,Super),
	disabled_base_class(Super)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Find all ressource

:-light_tabular ressource/1.

ressource(Res) :-
	needs(_,XRes),
	atom_module(XRes,_,Res)
	.

ressource(Res) :-
	provides(_,XRes),
	atom_module(XRes,_,Res)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure on Needed ressources

:-light_tabular needs_closure/3.
:-mode(needs_closure/3,+(+,-,-)).

needs_closure(Class,Need,NS) :-
	needs(Class,XNeed),
	atom_module(XNeed,NS,Need)
	.

needs_closure(Class,Need,NS) :-
	super(Class,Super),
	needs_closure(Super,Need,NS)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure on Provided ressources

:-light_tabular provides_closure/3.
:-mode(provides_closure/3,+(+,-,-)).

provides_closure(Class,Provide,NS) :-
	provides(Class,XProvide),
	atom_module(XProvide,NS,Provide)
	.

provides_closure(Class,Provide,NS) :-
	super(Class,Super),
	provides_closure(Super,Provide,NS)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure on Defined Nodes

%% We assume computed relations for super classes

:-light_tabular node_closure/2.
:-mode(node_closure/2,+(+,-)).

node_closure(Class,Node) :-
	\+ class2dump(Class,DClass),
	node(Class,Node)
	.

node_closure(Class,Node) :-
	\+ class2dump(Class,DClass),
	xsuper(Class,Super,NS,node_closure),
	'$answers'(node_closure(Super,_Node)),
	deep_module_shift(_Node,NS,Node)
	.

node_closure(Class,Node) :-
	\+ class2dump(Class,DClass),
	option('-virtual'),
	vnode_closure(Class,Node)
	.

node_closure(Class,Node) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!repnode(DClass,Node))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure on all nodes (including virtual ones)

:-light_tabular vnode_closure/2.
:-mode(vnode_closure/2,+(+,-)).

vnode_closure(Class,Node) :-
	(  node(Class,N1),N1=N2 ;
	   father(Class,N1,N2) ;
	   dominates(Class,N1,N2) ;
	   precedes(Class,N1,N2) ;
	   equals(Class,N1,N2) ;
	   brother(Class,N1,N2) ;
	   nodefeature(Class,N1,_),N2=N1;
	   recorded(normalized_guard(Class,N1,_,_)), N2=N1
	),
	(   Node=N1;
	    Node=N2
	)
	.

vnode_closure(Class,Node) :-
	xsuper(Class,Super,NS,vnode_closure),
	'$answers'(vnode_closure(Super,_Node)),
	deep_module_shift(_Node,NS,Node)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure on node equality relation

%% We assume computed relations for super classes

:-light_tabular equals_closure/3.
:-mode(equals_closure/3,+(+,+,-)).

equals_closure(Class,N1,N2) :-
	\+ class2dump(Class,_),
	@*{
	   collect_first => [N1],
	   collect_loop => [LN1],
	   collect_next =>  [LN2],
	   collect_last => [N2],
	   vars => [Class], %% explicit propagation of Class because of Bug somewhere in DyALog
	   goal => (   equals(Class,LN1,LN2)
		   ;   equals(Class,LN2,LN1)
		   ;   xsuper(Class,Super,NS,equals_closure),
		       deep_module_unshift(LN1,NS,_N1),
		       ( '$answers'(equals_closure(Super,_N1,_N2))
		       ; '$answers'(equals_closure(Super,_N2,_N1))
		       ),
		       deep_module_shift(_N2,NS,LN2)
		   )
	  },
	( node_closure(Class,N1),
	  node_closure(Class,N2) ->
	  N1 @< N2
	; node_closure(Class,N1) ->
	  true
	; node_closure(Class,N2) ->
	  fail
	;
	  N1 @< N2
	),
%%	format('class ~w: ~w == ~w\n',[Class,N1,N2]),
	true
	.

equals_closure(Class,N1,N2) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!equals(DClass,N1,N2))
	.

:-xcompiler((xequals(Class,N1R,N1) :-
	      (N1R=N1 ; '$answers'(equals_closure(Class,N1R,N1))))).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Computation of representative nodes for the equality relation

:-extensional repnode/2.

:-std_prolog safe_repnode/1.

safe_repnode(C) :-
	( class2dump(C,DClass) ->
	  %% nothing to do. Should have been restored
	  true
	;
	  every(( vnode_closure(C,N1),
		  equals_closure(C,N1,_))),
	  %% Try to use defined node before virtual node
	  %% as representative node
	  (   node_closure(C,N1) ; 
	      '$answers'(vnode_closure(C,N1))
	  ),
	  %%	format('OK1 ~w ~w\n',[C,N1]),
	  (   '$answers'(equals_closure(C,_,N1)) ->
	      %% the node equals (in a oriented way) some other node
	      %% => it is not used as a representative
	      %% format('not rep ~w ~w\n',[C,N1]),
	      fail
	  ; \+ repnode(C,N1),
	      %% no (directed) equal relation starting from this node
	      %% it may be used as a representative
	      record( repnode(C,N1) ),
	      %% format('Register repnode ~w ~w\n',[C,N1]),
	      true
	  )
	)
	.

:-light_tabular repnode/3.
:-mode(repnode/3,+(+,+,-)).

repnode(Class,N1,N2) :-
	repnode(Class,N2),
	xequals(Class,N2,N1)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of brotherhood

:-light_tabular brother_closure/3.
:-mode(brother_closure/3,+(+,+,-)).

brother_closure(Class,N1R,N3R) :-
	\+ class2dump(Class,_),
	(   brother(Class,N1,N2)
	;   brother(Class,N2,N1)
	),
	repnode(Class,N1,N1R),
	repnode(Class,N2,N2R),
	(   N3R = N2R
	;   brother_closure(Class,N2R,N3R)
	),
	N3R \== N1R,
%%	format('Brother closure ~w: ~w ~w\n',[Class,N1R,N3R]),
	true
	.


brother_closure(Class,N1R,N3R) :-
	\+ class2dump(Class,_),
	xsuper(Class,Super,NS,brother_closure),
	xequals(Class,N1R,N1),
	deep_module_unshift(N1,NS,_N1),
	'$answers'(brother_closure(Super,_N1,_N2)),
	deep_module_shift(_N2,NS,N2),
	repnode(Class,N2,N2R),
	(   N3R = N2R
	;   brother_closure(Class,N2R,N3R)
	),
	N3R \== N1R
	.

brother_closure(Class,N1,N2) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!precedes(DClass,N1,N2))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of domination relation (including father)

%% We assume computed relations for super classes

:-light_tabular dominates_closure/4.
:-mode(dominates_closure/4,+(+,+,-,-)).

dominates_closure(Class,N1R,N4R,K) :-
	\+ class2dump(Class,_),
	xequals(Class,N1R,N1),
	(   father(Class,N1,N2), K1=1
	;   dominates(Class,N1,N2), K1= (+)
	;   xsuper(Class,Super,NS,dominates_closure),
	    deep_module_unshift(N1,NS,_N1),
	    '$answers'(dominates_closure(Super,_N1,_N2,K1)),
	    % ( '$answers'(dominates_closure(Super,_N1,_N2,K1)),
	    %   (K1 = 1
	    %   ;
	    %    K1 = (+),
	    %    \+ '$answers'(dominates_closure(Super,_N1,_N2,1))
	    %   )
	    % ),
	    deep_module_shift(_N2,NS,N2)
	),
	repnode(Class,N2,N2R),
	(   N3R=N2R, K=K1,
	    (	K = 1 ->
		every((
		       xequals(Class,N2R,N3),
%%		       format('%% Anonymous father(~w) ~w\n',[N3,N1]),
		       name_anonymous(Class,father(N3),N1)))
	    ;	
		true
	    )
	;   dominates_closure(Class,N2R,N3R,_),
	    K= (+)
	),
	(   N3R=N4R
	;   '$answers'( brother_closure(Class,N3R,N4R) )
	;   '$answers'( brother_closure(Class,N4R,N3R) )
	)
	.

dominates_closure(Class,N1R,N2R,+) :-
	\+ class2dump(Class,_),
	neutral_class(Class),
	dominates_closure(Class,N1R,N3R,+),
	'$answers'(dominates_closure(Class,N2R,N3R,1)),
	N1R \== N2R
	.

dominates_closure(Class,N1,N2,K) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!dominates(DClass,N1,N2,K))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of ancestor relation

%% Only called for neutral classes

%% We assume computed dominates_closure/4

:-light_tabular ancestor/3.
:-mode(ancestor/3,+,(+,+,-)).

ancestor(Class,N1,N2) :-
	'$answers'(dominates_closure(Class,N3,N1,_)),
	(   N2=N3 ; ancestor(Class,N3,N2))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Potential father
%%
%% N1 is a potential father of N2, if either
%%        - N1 is a father of N2
%%        - N1 dominates N2 and all nodes between N1 and N2 are virtual
%%        - N1 dominated by N3 pfather of N2

%% Only called for neutral classes

%% We assume computed dominates_closure/4 node_closure/2 precedes_closure/4

:-light_tabular pfather/4.
:-mode(pfather/4,+,(+,+,-,-)).

pfather(Class,N1,N2,strong:N1) :-
	\+ class2dump(Class,_),
	'$answers'(dominates_closure(Class,N1,N2,1))
	.

pfather(Class,N1,N2,weak:N1) :-
	\+ class2dump(Class,_),
	'$answers'(dominates_closure(Class,N1,N2,+)),
	\+ '$answers'(dominates_closure(Class,N1,N2,1)),
	\+ (   '$answers'(dominates_closure(Class,N1,N,_)),
	       '$answers'(node_closure(Class,N)),
	       '$answers'(dominates_closure(Class,N,N2,_))
	       ),
%%	\+ ( '$answers'(dominates_closure(Class,_N1,N2,+)),
%%	     _N1 \== N1 ),
	true
	.

pfather(Class,N1,N2,weak:[]) :-
	\+ class2dump(Class,_),
%%	option('-forceroot'),
	neutral_class(Class),
	repnode(Class,N1),
	repnode(Class,N2),
	root(Class,N1),
	root(Class,N2),
	N1 \== N2,
%	'$answers'(dominates_closure(Class,N1,N3,+)),
%	'$answers'(dominates_closure(Class,N2,N3,_)),
%%	format('Root pfather ~w ~w\n',[N1,N2]),
	true
	.


pfather(Class,N1,N2,weak:N3) :-
	\+ class2dump(Class,_),
	\+ option('-lightdom'),
	'$answers'(dominates_closure(Class,N3,N1,_)),
	pfather(Class,N3,N2,weak:N3),
	N2 \== N1,
	\+ '$answers'(dominates_closure(Class,N2,N1,_)),
	\+ ((N=N2 ; '$answers'(dominates_closure(Class,N2,N,_))),
	    ('$answers'(precedes_closure(Class,N1,N,strict)) ;
	     '$answers'(precedes_closure(Class,N,N1,strict))))
	.

pfather(Class,N1,N2,weak:N3) :-
	\+ class2dump(Class,_),
	\+ option('-lightdom'),
	'$answers'(dominates_closure(Class,N3,N1,+)),
	pfather(Class,N3,N4,weak:N3),
	N4 \== N1,
        pfather(Class,N4,N2,weak:N4),		   
	\+ '$answers'(dominates_closure(Class,N4,N1,_)),
	\+ ((N=N2 ; '$answers'(dominates_closure(Class,N2,N,_))),
	    ('$answers'(precedes_closure(Class,N1,N,strict)) ;
	     '$answers'(precedes_closure(Class,N,N1,strict)))),
	\+ ((N=N1 ; '$answers'(dominates_closure(Class,N1,N,_))),
	    ('$answers'(precedes_closure(Class,N4,N,strict)) ;
	     '$answers'(precedes_closure(Class,N,N4,strict)))),
	true
	.

pfather(Class,N1,N2,K) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!pfather(DClass,N1,N2,K))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Computation of root nodes

%% Only called for neutral classes

%% We assume computed node_closure/2

:-light_tabular root/2.
:-mode(root/2,+(+,+)).

%% A root node is a defined representative node with no defined ancestor
%% but itself
root(Class,N) :-
	\+ class2dump(Class,_),
	'$answers'(node_closure(Class,N)),
	repnode(Class,N),
	\+ (   ancestor(Class,N,P),
	       P \== N,
	       '$answers'(node_closure(Class,P))
	   )
	.

root(Class,N) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!root(DClass,N))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of preceding relation

%% We assume computed dominates_closure

:-light_tabular precedes_closure/4.
:-mode(precedes_closure/4,+(+,+,-,-)).
	
precedes_closure(Class,N1R,N3R,K) :-
	\+ class2dump(Class,_),
	(
%%	    format('try precedes1 with ~w\n',[N1R]),
	    precedes(Class,N1,N2),
	    repnode(Class,N1,N1R),
	    repnode(Class,N2,N2R),
	    (	N3R=N2R, K=strict,
%%		format('1.1 => ~w < ~w [~w]\n',[N1R,N3R,K]),
		true
	    ;	'$answers'(dominates_closure(Class,N2R,N3R,_)), K= strict,
		%%	    format('1.3 => ~w < ~w N2=~w [~w]\n',[N1R,N3R,N2R,K]),
		true
	    ;	'$answers'(dominates_closure(Class,N3R,N2R,_)), K = dom,
		%%	    format('1.4 => ~w < ~w N2=~w [~w]\n',[N1R,N3R,N2R,K]),
		true
	    ;	%% format('try 1.2 => N1R=~w N2R=~w\n',[N1R,N2R]),
		precedes_closure(Class,N2R,N3R,K),
%%		format('1.2 => ~w < ~w < ~w [~w]\n',[N1R,N2R,N3R,K]),
		true
	    ),
%%	    format('1 => ~w < ~w [~w]\n',[N1R,N3R,K]),
	    true
	;
	    K=strict,
%%	    format('try precedes2 with ~w\n',[N1R]),
	    '$answers'(dominates_closure(Class,N2R,N1R,_)),
	    precedes_closure(Class,N2R,N4R,strict),
	    (	N3R = N4R
	    ;	'$answers'(dominates_closure(Class,N4R,N3R,_))
	    ),
	    %%	format('2 => ~w < ~w [~w]\n',[N1R,N3R,K]),
	    true
	;
	    K=dom,
%%	    format('try precedes3 with ~w\n',[N1R]),
	    '$answers'(dominates_closure(Class,N1R,N2R,_)),
	    precedes_closure(Class,N2R,N3R,_),
	    %%	format('3 => ~w >> ~w and ~w < ~w => ~w < ~w [~w]\n',[N1R,N2R,N2R,N3R,N1R,N3R,dom]),
	    true
	;
%%	    format('try precedes4 with ~w\n',[N1R]),
	    xsuper(Class,Super,NS,precedes_closure),
	    xequals(Class,N1R,N1),
	    deep_module_unshift(N1,NS,_N1),
	 '$answers'(precedes_closure(Super,_N1,_N2,_K)),
	    % ( '$answers'(precedes_closure(Super,_N1,_N2,_K)),
	    %   (
	    %    _K = strict
	    %   ;
	    %    _K = dom,
	    %    \+ '$answers'(precedes_closure(Super,_N1,_N2,dom))
	    %   )
	    % ),
	    deep_module_shift(_N2,NS,N2),
	    repnode(Class,N2,N2R),
	    (	N3R=N2R, K=_K,
		%%	    format('4.1 => ~w < ~w [~w]\n',[N1R,N3R,K]),
		true
	    ;	precedes_closure(Class,N2R,N3R,K2),
		( (_K = strict, K2=strict) -> K = strict
		;   (_K = strict xor K2=strict) -> K = dom
		;   fail
		),
		%%	    format('4.2 => ~w < ~w [~w]\n',[N1R,N3R,K]),
		true
	    ;	'$answers'(dominates_closure(Class,N2R,N3R,_)),
		K=_K, _K=strict,
		%%	    format('4.3 => ~w < ~w [~w]\n',[N1R,N3R,K]),
		true
	    ;	'$answers'(dominates_closure(Class,N3R,N2R,_)), K = dom,
		%%	    format('4.4 => ~w < ~w [~w]\n',[N1R,N3R,K]),
		true
	    ),
	    %%	format('4 => ~w < ~w [~w]\n',[N1R,N3R,K]),
	    true
	)
	.

precedes_closure(Class,N1,N2,K) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!precedes(DClass,N1,N2,K))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of FS node information

:-light_tabular nodefeature_closure/3.
:-mode(nodefeature_closure/3,+(+,+,-)).

%% Assume that N is a representative node
nodefeature_closure(Class,N,FS) :-
	\+ class2dump(Class,_),
	repnode(Class,N),
	mutable(M,var([]),true),
	class_bindings(Class,Bindings),
%%	format('TRY NODEFEATURE ~w ~w ~w\n',[Class,N,Bindings]),
	every(( xequals(Class,N,N1),
		(   
		    nodefeature(Class,N1,FS1),
		    (fs!normalize(FS1,XFS1,Bindings) xor XFS1 = fail)
		;   
		    xsuper(Class,Super,NS,nodefeature_closure),
		    deep_module_unshift(N1,NS,_N1),
		    '$answers'(nodefeature_closure(Super,_N1,FS1)),
%%		    format('TRY FS SHIFT ~w ~w => ~w\n',[_XFS1,NS,XFS1]),
		    fs!shift(FS1,NS,XFS1),
%%		    format('FS SHIFT ~w ~w => ~w\n',[_XFS1,NS,XFS1]),
		    true
		),
		mutable_read(M,FS2),
%%		format('Try Unif ~w ~w ~w\n',[FS1,XFS1,FS2]),
		(   XFS1 \== fail,
%%		    format('\nCheck Node Unify ~w:\n\t~w &\n\t~w\n~Z\n',[Class,XFS1,FS2,Bindings]),
		    fs!unify(XFS1,FS2,FS3,Bindings),
%%		    format('Unif ~w ~w -> ~w ~w:~w\n',[XFS1,FS2,FS3,Bindings,_XB]),
		    true
		xor FS3=fail,
%%		    format('** Failed unif\n\t~w\n\t~w\n\n',[XFS1,FS2]),
		    true
		),
		mutable(M,FS3)
	      )),
	every((
%	       fail,
		mutable_read(M,_FS1),
		_FS1 \== fail,
		fs!value(_FS1,type,type:Type,Bindings),
		domain(Type,[subst,foot]),
		\+ fs!value(_FS1,adj,adj:no,Bindings),
%%		warning('*** force adj=no class=~w node=~w type=~w',[Class,N,Type]),
		fs!unify(_FS1,[adj:no],_FS2,Bindings),
		mutable(M,_FS2)
	      )),
	every((
	       mutable_read(M,FS4),
	       FS4 \== fail,
%	       format(2,'test1 class=~w node=~w\n',[Class,N]),
	       fs!value(FS4,adj,adj:no,Bindings),
	       mutable_read(Bindings,_L),
	       (
		(
		 fs!value(FS4,top,top:Top,Bindings),
		 fs!value(FS4,bot,bot:Bot,Bindings) ->
		 fs!unify(Top,Bot,_,Bindings)
		; fs!value(FS4,top,top:Top,Bindings) ->
		 fs!unify(FS4,[bot:Top],FS5,Bindings),
		 mutable(M,FS5)
		; fs!value(FS4,bot,bot:Bot,Bindings) ->
		 fs!unify(FS4,[top:Bot],FS5,Bindings),
		 mutable(M,FS5)
		;
		 new_var('',Var),
		 fs!unify(FS4,[top:Var,bot:Var],FS5,Bindings),
%		 format(2,'test2 class=~w node=~w\n',[Class,N]),
		 mutable(M,FS5)
		)
	       xor
	       warning('*** mismatch top=bot class=~w node=~w',[Class,N]),
		mutable(Bindings,_L)
	       )
	      )),
	mutable_read(M,FS),	
	check_failed(Class,FS\==fail),
%%	format('** Nodefeature ~w ~w: ~w ~w\n',[Class,N,FS,Bindings]),
	true
	.

nodefeature_closure(Class,N,FS) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!nodefeature(DClass,N,FS))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closure of class description FS

:-light_tabular description_closure/2.
:-mode(description_closure/2,+(+,-)).

description_closure(Class,Desc) :-
	\+ class2dump(Class,_),
	mutable(M,var([]),true),
	class_bindings(Class,Bindings),
	every(( ( description(Class,D1),
		%%    format('\nDesc Normalize ~w: ~w\n',[Class,D1]),
		    (	fs!normalize(D1,XD1,Bindings) xor XD1 = fail )
		;   
		    xsuper(Class,Super,NS,description_closure),
		    '$answers'(description_closure(Super,_XD1)),
		    fs!shift(_XD1,NS,XD1)
		),
		mutable_read(M,D2),
		(   XD1 \== fail,
		    %%  format('\nTry unify ~w ~w\n',[XD1,D2]),
		    fs!unify(XD1,D2,D3,Bindings),
		    %% format('=> ~w\n',[D3]),
		    true
		xor D3=fail
		),
		mutable(M,D3)
	      )),
	mutable_read(M,Desc),
	check_failed(Class,Desc \== fail),
%%	format('** Description ~w: ~w\n',[Class,Desc]),
	true
	.


description_closure(Class,Desc) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!description(DClass,Desc))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Class bindings
%%   setup a mutable to store bindings of the form (X:FS)
%%   inherits bindings from super classes
%%   and class-defined bindings using binding/3

class_bindings(Class,Bindings) :-
	\+ class2dump(Class,_),
	mutable(Bindings,[]),
	mutable(Tmp,true,true),
	every((
	       (   binding(Class,Var,UFS),
		   (   fs!normalize(UFS,FS,Bindings)
		   xor mutable(Tmp,fail),
		       fail
		   )
	       ;   
		   xsuper(Class,Super,NS,class_bindings),
		   '$answers'(class_bindings(Super,Super_Bindings)),
%%		   class_bindings(Super,Super_Bindings),
		   mutable_read(Super_Bindings,L),
		   domain(var(_Var):_FS,L),
%%		   format('Try Herit binding ~w:~w from ~w!~w:~w => ~w:~w\n',[Class,Bindings,NS,Super,Super_Bindings,_Var,_FS]),
		   deep_module_shift(_Var,NS,Var),
		   fs!shift(_FS,NS,FS),
%%		   format('Herit binding ~w:~w from ~w!~w:~w => ~w:~w\n',[Class,Bindings,NS,Super,Super_Bindings,Var,FS]),
		   true
	       ),
	       mutable_read(Tmp,true),
%%	       format('Check bind ~w: ~w & ~w\n',[Class,var(Var),FS]),
	       (   fs!check_or_bind(var(Var),FS,Bindings) 
	       xor mutable(Tmp,fail))
	      )),
	mutable_read(Tmp,Flag),
	check_failed(Class, Flag == true ),
	true
	.

class_bindings(Class,Bindings) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!bindings(DClass,L)),
	mutable(Bindings,L)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Guards
%% use same notations that class equations
%%
%%  Guards should be analyzed first to add constraints

:-std_prolog new_var/2.

new_var(Type,var(V)) :-
	update_counter(eqn,N),
	name_builder('~w@~w',[Type,N],V),
%%	V = (Type '@' N),
%%	format('New var ~w\n',[V]),
	true
	.

:-std_prolog class_nodeguards/1.

class_nodeguards(Class) :-
	every(( guard(Class,Node,Rel,Guard),
		class_nodeguards_aux(Class,Guard,XGuard),
		record_without_doublon( normalized_guard(Class,Node,Rel,XGuard) )
	      ))
	.

:-rec_prolog class_nodeguards_aux/3.

class_nodeguards_aux(_,[],[]).

class_nodeguards_aux(Class,[Path1=Path2|Guard],[guard(Type,T1,T2)|XGuard]) :-
	(   Path1 = value(V1),
	    T1=V1,
	    Type1=none
	xor analyze_equation(Class,Path1,T1,Type1)
	),
	(   Path2 = value(V2),
	    T2=V2,
	    Type2 =none
	xor analyze_equation(Class,Path2,T2,Type2)
	),
	eqtype_select(Type1,Type2,Type),
	class_nodeguards_aux(Class,Guard,XGuard)
	.


class_nodeguards_aux(Class,[Path1==Path2|Guard],[xguard(Type,T1,T2)|XGuard]) :-
	(   Path1 = value(V1),
	    T1=V1,
	    Type1=none
	xor analyze_equation(Class,Path1,T1,Type1)
	),
	(   Path2 = value(V2),
	    T2=V2,
	    Type2 =none
	xor analyze_equation(Class,Path2,T2,Type2)
	),
	eqtype_select(Type1,Type2,Type),
	class_nodeguards_aux(Class,Guard,XGuard)
	.

class_nodeguards_aux(Class,[and(Guard1)|Guard2],[and(XGuard1)|XGuard2]) :-
	class_nodeguards_aux(Class,Guard1,XGuard1),
	class_nodeguards_aux(Class,Guard2,XGuard2)
	.

class_nodeguards_aux(Class,[OrGuard::[_|_]|Guard],[XOrGuard|XGuard]) :-
	class_nodeguards_aux(Class,OrGuard,XOrGuard),
	class_nodeguards_aux(Class,Guard,XGuard)
	.

:-light_tabular eqtype_select/3.

eqtype_select(T1,T2,T) :-
	( T1 = none -> T = T2
	;   T = T1 )
	.

:-std_prolog nodeguard_closure_fixpoint/2.

nodeguard_closure_fixpoint(Class,Level) :-
%	format('fixpoint class=~w level=~w\n',[Class,Level]),
	class_bindings(Class,Bindings),
	mutable_read(Bindings,OldB),
	% (number(Level) xor
	% format('*** pb var class=~w level=~w\n',[Class,Level])
	% ),
	every((repnode(Class,N),
	       domain(Rel,guardrel[]),
%	       format('try nodeguard closure class=~w n=~w rel=~w level=~w\n',[Class,N,Rel,Level]),
	       nodeguard_closure(Class,N,Rel,Guard,Level),
%	       format('got nodeguard closure class=~w n=~w rel=~w level=~w => guard=~w\n',[Class,N,Rel,Level,Guard]),
	       true
	      )),
	mutable_read(Bindings,NewB),
	(
	 (OldB = NewB xor Level > 10)->
%	 true ->
	 %% reached fix point (or too deep)
%	 format('reached fixpoint class=~w level=~w\n',[Class,Level]),
	 record_without_doublon( nodeguard_fixpoint(Class,Level) )
	;
	 %% new iteration
	 NewLevel is Level+1,
	 nodeguard_closure_fixpoint(Class,NewLevel)
	)
	.

:-light_tabular nodeguard_closure/4.
:-mode(nodeguard_closure/4,+(+,+,+,-)).

nodeguard_closure(Class,N,Rel,Guard) :-
	\+ class2dump(Class,_),
	recorded( nodeguard_fixpoint(Class,Level) ),
	% ( recorded( nodeguard_fixpoint(Class,Level) )
	% xor format('no recorded fixpoint ~w\n',[Class]), fail
	% ),
	% ( number(Level) xor
	% format('*** pb var class=~w level=~w node=~w rel=~w\n',[Class,Level,N,Rel])
	% ),
%	format('retrieve nodeguard class=~w node=~w rel=~w (level is ~w)\n',[Class,M,Rel,Level]),
	'$answers'( nodeguard_closure(Class,N,Rel,Guard,Level) ),
	every(( erase_relation( nodeguard_closure(Class,N,Rel,Guard,_) ) )),
%         nodeguard_closure(Class,N,Rel,Guard,0),
%	format('=> ~w\n',[Guard]),
	true
	.
	
:-light_tabular nodeguard_closure/5.
:-mode(nodeguard_closure/5,+(+,+,+,-,+)).

nodeguard_closure(Class,N,Rel,XGuard,Level) :-
	\+ class2dump(Class,_),
	class_bindings(Class,Bindings),
%	( number(Level) xor
%	  format('*** pb var class=~w level=~w node=~w rel=~w\n',[Class,Level,N,Rel])
%	),
	repnode(Class,N),
	%% we find initial guard
	every(('$answers'(pfather(Class,P,N,_:_P1)), _P1 \== [],
	       domain(PRel,guardrel[]),
	       nodeguard_closure(Class,P,PRel,_,Level))),
	( Level = 0 ->
	  %% do not use '$answers' for equals_closure
	  %% because nodeguard is called before safe_repnode
	  xequals(Class,N,N1),
	  (   recorded( normalized_guard(Class,N1,Rel,Guard) )
	  ;   xsuper(Class,Super,NS,nodeguard_closure),
	      deep_module_unshift(N1,NS,_N1),
	      '$answers'(nodeguard_closure(Super,_N1,Rel,_Guard)),
	      guard!shift(_Guard,NS,Guard)
	  )
	;
	  %% we use reduced guard from previous level
%	  format('ask nodeguard ~w ~w ~w l=~w\n',[Class,N,Rel,Level]),
	  PrevLevel is Level-1,
	  '$answers'(nodeguard_closure(Class,N,Rel,Guard,PrevLevel)),
%	  format('found nodeguard ~w ~w ~w l=~w => ~w\n',[Class,N,Rel,Level,Guard]),
	  true
%	  nodeguard_closure(Class,N,Rel,Guard,PrevLevel)
	),
%	mutable_read(Bindings,LL),
%	format('guard closure ~w ~w ~w l=~w=> ~w\tbindings=~w\n',[Class,N,Rel,Level,Guard,LL]),
	( Guard == true ->
%	  format('reused true guard ~w ~w ~w ~w l=~w\n',[Class,N,Rel,Guard,Level]),
	  XGuard1 = true
	; guard!reduce(Guard,XGuard1,Bindings) ->
	  true
	;
	  XGuard1 = true,
%	  format('true guard ~w ~w ~w ~w l=~w\n',[Class,N,Rel,Guard,Level])
	  true
	),
%	format('=> reduced ~w ~w ~w: ~w\n',[Class,Rel,N,XGuard1]),
	( Rel = (+) -> Rel2 = (-) ; Rel2 = guardrel[+,++]),
	(   
	    (	XGuard1 = [and(_XGuard2)],
		%% \+ (domain(_X,XGuard2), \+ _X = guard(_,_,_))
		%% XGuard2 = [guard(_,_,_)]
		(   _XGuard2 = guard(_,_,_),
		    XGuard2 = [_XGuard2]
		xor _XGuard2 = XGuard2
		)
	    xor XGuard1 = [guard(_,_,_)],
		XGuard2 = XGuard1
	    xor XGuard1 = guard(_,_,_),
		XGuard2 = [XGuard1]
	    ),
	    (XGuard1 = [] ->
	     record_without_doublon( empty_nodeguard(Class,N,Rel) )
	    ; Rel = guardrel[+,++] ->
	     true
	    ; record_without_doublon( Tmp::tmp_nodeguard(Class,N,Rel,Level) ),
	     TmpFlag=yes
	    ),
%%	    format('Potential reduce ~w Rel2=~w\n',[Class,Rel2]),
%%	    Rel = guardrel[+,-],
	    (	nodeguard_closure(Class,N,Rel2,[],Level)
	    xor recorded( empty_nodeguard(Class,N,Rel2) )),
%%	    format('Extra Potential reduce ~w\n',[Class]),
	    neutral_class(Class),
%%	    format('Hyper Potential reduce ~w\n',[Class]),
	    \+ ancestor_node_optional(Class,N,Bindings,Level) ->
%% 	    format('Guard try apply ~w ~w\n',[Class,N]),
	    ( guard!try_apply(XGuard2,_XGuard,Bindings) ->
%	      format('Guard Apply ~w ~w ~w ~w l=~w=> ~w\n',[Class,Rel,N,XGuard1,Level,_XGuard]),
	      (_XGuard = [] -> XGuard = true ; XGuard = _XGuard)
	    ;	
	      XGuard = []
	    )
	;   
	    XGuard = XGuard1
	), 
	check_failed(Class,
		     \+ ( XGuard = [],
			    %% to avoid mutual expectencies of empty nodeguards
			    %% related to the fact the nodeguard_closure is only light_tabular !
			    record_without_doublon( empty_nodeguard(Class,N,Rel) ),
			    (	nodeguard_closure(Class,N,Rel2,[],Level)
			    xor recorded( empty_nodeguard(Class,N,Rel2)) ),
%%			    format('POTENTIALLY FAILED NODEGUARD ~w ~w rel=~w guard=~w => ~w\n',[Class,N,Rel,Guard,XGuard1]),
			    neutral_class(Class),
			    \+ node_optional(Class,N,Bindings,Level),
			  %%  name2class(C,XC),
			  %%  format('FAILED NODEGUARD ~w xc=~w ~w rel=~w guard=~w level=~w=> ~w\n',[Class,XC,N,Rel,Guard,Level,XGuard1]),
			  true
			)),
%	(XGuard = true -> format('GUARD ~w ~w rel=~w l=~w->~w\n',[Class,N,Rel,Level,XGuard]) ; true),
	(var(TmpFlag) xor erase(Tmp)),
	true
	.

nodeguard_closure(Class,Node,Kind,Formula) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!nodeguard(DClass,Node,Kind,Formula))
	.


:-std_prolog guard!try_apply/3.

guard!try_apply(L,LL,Bindings) :-
	mutable_read(Bindings,Save),
	( guard!apply(L,LL,Bindings) ->
	    true
	;   
	    mutable(Bindings,Save),
	    fail
	)
	.

:-std_prolog guard!apply/3.

guard!apply(Guards,XGuards,Bindings) :-
	(   Guards = [], XGuards = []
	xor ( Guards = [guard(_,FS1,FS2)|Guards1] ;
		Guards = [[guard(_,FS1,FS2)]|Guards1] ),
	    fs!normalize(FS1,XFS1,Bindings),
	    fs!normalize(FS2,XFS2,Bindings),
	    fs!unify(XFS1,XFS2,_,Bindings),
	    guard!apply(Guards1,XGuards,Bindings)
	xor Guards = [Or1|Guards1],
	    XGuards = [Or1|XGuards1],
	    guard!apply(Guards1,XGuards1,Bindings)
	)
	.

:-std_prolog ancestor_node_optional/4.

ancestor_node_optional(Class,N,Bindings,Level) :-
	( '$answers'(pfather(Class,A,N,_:_P)),
	  _P \== [],
	  (               
	      '$answers'(nodefeature_closure(Class,A,A_FS)),
	      fs!value(A_FS,type,type:alternative,Bindings),
	      %% A is an alternative node, such that there is exist at least one other son N2 other than N still alive
	      %% (ie N2 has no failed positive guards)
	      ('$answers'(pfather(Class,A,N2,_:_P2)),
	       _P2 \== [],
	       N2 \== N,
	       \+ ( 
		    ( recorded(empty_nodeguard(Class,N2,+))
		    xor '$answers'(nodeguard_closure(Class,N2,guardrel[+,++],[],_))
%		    xor nodeguard_closure(Class,N2,++,[],Level)
%		    xor nodeguard_closure(Class,N2,+,[],Level)
		    )
		  )
	      )
	  xor node_optional(Class,A,Bindings,Level)
	  ) xor fail
	)
	.

:-light_tabular node_optional/4.
%%:-std_prolog node_optional/4.

node_optional(Class,N,Bindings,Level) :-
%	format('Try optional ~w ~w l=~w\n',[Class,N,Level]),
	(
	    (	'$answers'(nodefeature_closure(Class,N,FS)),
		fs!value(FS,optional,optional:yes,Bindings)
	    )
	xor ( recorded( tmp_nodeguard(Class,N,-,Level) ) )
	xor ( '$answers'(nodeguard_closure(Class,N,-,G,Level)), G \== [] )
%%	xor ( recorded( true_guard(Class,N,-) ) )
%%	xor node_optional_aux(Class,N,Bindings)
	xor ancestor_node_optional(Class,N,Bindings,Level)
	/*
	xor ( '$answers'(pfather(Class,A,N,_:_P)), _P \== [],
		(   '$answers'(nodefeature_closure(Class,A,A_FS)),
		    fs!value(A_FS,type,type:alternative,Bindings)
		xor node_optional(Class,A,Bindings,Level)
		)
	    )
        */
	),
%	format('=>optional ~w ~w l=~w\n',[Class,N,Level]),
	true
	.

:-rec_prolog guard!reduce/3.

guard!reduce([],[],_).
guard!reduce(Guard::[G::guard(Type,FS1,FS2)|Guard2],XGuard,Bindings) :-
%	format('start or_reduce guard=~w\n',[Guard]),
	(   fs!check_unify(FS1,FS2,Bindings,BindP) ->
	    ( BindP = nobind ->
%	      format('*** guard true: ~w = ~w\n',[FS1,FS2]),
	      fail		%% no need to register true guards
	    ;	
		XGuard = [G|XGuard2],
		guard!reduce(Guard2,XGuard2,Bindings)
	    )
	;   
	    guard!reduce(Guard2,XGuard,Bindings)
	),
%	format('==> or_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true
	.
guard!reduce([G::xguard(Type,FS1,FS2)|Guard2],XGuard,Bindings) :-
	( fs!check_unify(FS1,FS2,Bindings,BindP) ->
	    ( BindP = nobind ->
%%	      format('*** guard true: ~w = ~w\n',[FS1,FS2]),
		fail		%% no need to register true guards
	    ;	
		XGuard = [G|XGuard2],
		guard!reduce(Guard2,XGuard2,Bindings)
	    )
	;   
	    guard!reduce(Guard2,XGuard,Bindings)
	)
	.

guard!reduce(Guard::[G::and(Guard1)|Guard2],XGuard,Bindings) :-
%%	format('Try reduce and guard: ~w\n',[Guard1]),
%	format('start or_reduce guard=~w\n',[Guard]),
	( guard!reduce_and(Guard1,XGuard1,Bindings) ->
%%	    format('\t--> ~w\n',[XGuard1]),
	    ( XGuard1 = []  ->
	      %% we have a true condition => no need to register the guard
	      fail
	    ;	XGuard1 = [G1] ->
	      ( G1 = [_|_] ->
		append(G1,XGuard2,XGuard)
	      ;
		XGuard = [G1|XGuard2]
	      )
	    ;	
		XGuard = [and(XGuard1)|XGuard2]
	    ),
	    guard!reduce(Guard2,XGuard2,Bindings)
	;
	  %%	    format('\t--> FAIL\n',[]),
	    guard!reduce(Guard2,XGuard,Bindings)
	),
%	format('==> or_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true
	.

guard!reduce(Guard::[OrGuard::[_|_]|Guard2],XGuard,Bindings) :-
%	format('start or_reduce guard=~w\n',[Guard]),
	append(OrGuard,Guard2,FlatGuard),
	guard!reduce(FlatGuard,XGuard,Bindings),
	% ( guard!reduce(OrGuard,XOrGuard,Bindings) ->
	%   XGuard = [XOrGuard|XGuard2],
	%   guard!reduce(Guard2,XGuard2,Bindings)
	% ;
	%   fail
	% ),
%	format('==> or_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true
	.

guard!reduce(Guard::[[]|Guard2],XGuard,Bindings) :-
%	format('start or_reduce guard=~w\n',[Guard]),
	guard!reduce(Guard2,XGuard,Bindings),
	true
	.

:-rec_prolog append/3.

append([],Z,Z).
append([A|X],Y,[A|Z]) :- append(X,Y,Z).

:-rec_prolog guard!reduce_and/3.

guard!reduce_and([],[],_).

guard!reduce_and(Guard::[G::guard(Type,FS1,FS2)|Guard2],XGuard,Bindings) :-
%%	mutable_read(Bindings,LL),
%%	format('GUARD1 AND REDUCE ~w bindings=~w\n',[Guard,LL]),
%	format('start and_reduce guard=~w\n',[Guard]),
	( fs!check_unify(FS1,FS2,Bindings,BindP) ->
	    ( BindP = nobind ->
%%	      format('*** guard and true: ~w = ~w\n',[FS1,FS2]),
	      guard!reduce_and(Guard2,XGuard,Bindings)
	    ;	
	      XGuard = [G|XGuard2],
	      guard!reduce_and(Guard2,XGuard2,Bindings)
	    )
	;   
	    fail
	),
%	format('==> and_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true
	.

guard!reduce_and(Guard::[G::xguard(Type,FS1,FS2)|Guard2],XGuard,Bindings) :-
%	mutable_read(Bindings,LL),
%%	format('GUARD2 AND REDUCE ~w bindings=~w\n',[Guard,LL]),
%	format('start and_reduce guard=~w\n',[Guard]),
	( fs!check_unify(FS1,FS2,Bindings,BindP,NewL) ->
	    ( BindP = nobind ->
%%	      format('*** guard and true: ~w = ~w\n',[FS1,FS2]),
		guard!reduce_and(Guard2,XGuard,Bindings)
	    ;	
	      XGuard = [G|XGuard2],
	      mutable_read(Bindings,OldL),
	      mutable(Bindings,NewL),
	      ( guard!reduce_and(Guard2,XGuard2,Bindings) ->
		mutable(Bindings,OldL)
	      ;
		mutable(Bindings,OldL),
		fail
	      )
	    )
	;   
	    fail
	),
%	format('==> and_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true
	.

guard!reduce_and(Guard::[OrGuard::[_|_]|Guard2],XGuard,Bindings) :-
%%	format('HERE AND REDUCE ~w\n',[Guard]),
%	format('start and_reduce guard=~w\n',[Guard]),
	( guard!reduce(OrGuard,XOrGuard,Bindings) ->
	  ( XOrGuard = [] ->
	    %% is it the right action ?
	    %% XGuard = XGuard2
	    fail
	  ;
	    XGuard = [XOrGuard|XGuard2]
	  ),
	  guard!reduce_and(Guard2,XGuard2,Bindings),
%%	  format('%% and reduce ~w => ~w\n',[[OrGuard|Guard2],XGuard]),
	  true
	;
	  %% OrGuard is true
	  guard!reduce_and(Guard2,XGuard,Bindings)
	),
%	format('==> and_reduce guard=~w => xguard=~w\n',[Guard,XGuard]),
	true.

% guard!reduce_and(Guard::[OrGuard::[]|Guard2],XGuard,Bindings) :-
% 	format('start and_reduce guard=~w\n',[Guard]),
% 	fail
% 	.

guard!reduce_and(Guard::[and(Guard1)|Guard2],XGuard,Bindings) :-
%	format('start and_reduce guard=~w\n',[Guard]),
	append(Guard1,Guard2,FlatGuard),
	guard!reduce_and(FlatGuard,XGuard,Bindings)
	.

%:-light_tabular guard!shift/3.
%:-mode(guard!shift/3,+(+,+,-)).

:-std_prolog guard!shift/3.

guard!shift(_G,NS,G) :-
	( NS = [] ->
	    _G=G
	;
	    guard!shift_aux(_G,NS,G)
	).

:-rec_prolog guard!shift_aux/3.

guard!shift_aux([],_,[]).
guard!shift_aux([guard(Type,_FS1,_FS2)|Guard],NS,[guard(Type,FS1,FS2)|XGuard]) :-
	fs!shift(_FS1,NS,FS1),
	fs!shift(_FS2,NS,FS2),
	guard!shift_aux(Guard,NS,XGuard)
	.
guard!shift_aux([xguard(Type,_FS1,_FS2)|Guard],NS,[xguard(Type,FS1,FS2)|XGuard]) :-
	fs!shift(_FS1,NS,FS1),
	fs!shift(_FS2,NS,FS2),
	guard!shift_aux(Guard,NS,XGuard)
	.
guard!shift_aux([and(Guard1)|Guard2],NS,[and(XGuard1)|XGuard2]) :-
	guard!shift_aux(Guard1,NS,XGuard1),
	guard!shift_aux(Guard2,NS,XGuard2)
	.
guard!shift_aux([Guard1::[_|_]|Guard2],NS,[XGuard1|XGuard2]) :-
	guard!shift_aux(Guard1,NS,XGuard1),
	guard!shift_aux(Guard2,NS,XGuard2)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Class reducers

:-std_prolog class_reducers/1.

class_reducers(Class) :-
	every((reducer(Class,Path),
	       analyze_equation(Class,Path,Var,_),
	       record_without_doublon( normalized_reducer(Class,Var) )))
	.

:-light_tabular reducer_closure/2.
:-mode(reducer_closure/2,+(+,-)).

reducer_closure(Class,FS) :-
	\+ class2dump(Class,_),
	(   recorded( normalized_reducer(Class,FS) )
	;   xsuper(Class,Super,NS,reducer_closure),
	    '$answers'(reducer_closure(Super,_FS)),
	    fs!shift(_FS,NS,FS)
	),
%%	format('reducer closure ~w => ~w\n',[Class,FS]),
	true
	.

reduce_closure(Class,FS) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!reducer(DClass,FS))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Class equations
%%  Equations use compact path notations <root> (: <feature> )*
%%     where <root> := node(<name>)
%%                   | desc
%%                   | var(<name>)
%%
%%  Class equations should be analyzed first to add constraints

:-std_prolog class_equations/1.

class_equations(Class) :-
	every((equation(Class,Path1,Path2),
	       \+ Path1 = Path2, %% avoid looping equations
	       (   Path1 = value(V1)
	       xor Path1 = domain(_), V1=nobinding
	       xor Path1 = property(_), V1=nobinding
	       xor analyze_equation(Class,Path1,V1,_)),
	       (   Path2 = value(V2)
	       xor Path2 = domain(_), V2=nobinding
	       xor Path2 = property(_), V2=nobinding
	       xor analyze_equation(Class,Path2,V2,_)),
	       ( V1 = var(XV1) ->
		   record_without_doublon( binding(Class,XV1,V2) )
	       ; V2 = var(XV2) ->
		   record_without_doublon( binding(Class,XV2,V1) )
	       ; (V1 == nobinding xor V2 == nobinding ) ->
		   true
	       ;   
		   warning('please avoid equation between values: ~w = ~w\n',[Path1,Path2])
	       )
	      ))
	.

:-light_tabular analyze_equation/4.
:-mode(analyze_equation/4,+(+,+,-,-)).

analyze_equation(Class,node(N) : Path,Var,Type) :-
	analyze_path(Path,Var,FS,Type),
	record_without_doublon( OO::nodefeature(Class,N,FS) )
	.

analyze_equation(Class,desc : Path,Var,Type ) :-
	analyze_path(Path,Var,FS,Type),
	record_without_doublon( OO::description(Class,FS) )
	.

analyze_equation(Class,(var(V)) : Path,Var,Type) :-
	analyze_path(Path,Var,FS,Type),
	record_without_doublon( OO::binding(Class,V,FS) )
	.

analyze_equation(Class,node(N),Var,none) :-
	new_var('',Var),
	record_without_doublon( OO::nodefeature(Class,N,Var) )
	.

analyze_equation(Class,father(N),Var,none) :-
	new_var('',Var),
	record_without_doublon( OO::anonymous(Class,father(N),nodefeature(Var)))
	.

analyze_equation(Class,father(N) : Path,Var,Type) :-
	analyze_path(Path,Var,FS,Type),
	record_without_doublon( OO::anonymous(Class,father(N),nodefeature(FS)))
	.

analyze_equation(Class,desc,Var,none ) :-
	new_var(desc,Var),
	record_without_doublon( OO::description(Class,Var) )
	.

analyze_equation(Class,(var(V)),Var,none) :-
	new_var('',Var),
	record_without_doublon( OO::binding(Class,V,Var) )
	.

:-std_prolog analyze_path/4.

analyze_path(Path,Var,FS,Type) :-
	( Path = (F:Path2) ->
	    analyze_path(Path2,Var,FS2,Type),
	    FS = [F:FS2]
	;
	    FS = [Path:Var],
	    Type = Path,
	    new_var(Type,Var)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Anonymous

:-light_tabular anonymous_closure/3.

anonymous_closure(Class,NodeSpec,Info) :-
	\+ class2dump(Class,_),
	anonymous(Class,NodeSpec,Info)
	.

anonymous_closure(Class,NodeSpec,Info) :-
	\+ class2dump(Class,_),
	xsuper(Class,Super,NS,anonymous_closure),
	'$answers'(anonymous_closure(Super,_NodeSpec,_Info)),
	\+ '$answers'(name_anonymous(Super,_NodeSpec,_)),
%%	format('ANONYMOUS CLOSURE ~w ~w ~w\n',[Super,NS,_NodeSpec]),
	(   _NodeSpec = father(_N),_Info=nodefeature(_FS),
	    NodeSpec = father(N) ,Info=nodefeature(FS) ),
	deep_module_shift(_N,NS,N),
	fs!shift(_FS,NS,FS)
	.

anonymous_closure(Class,NodeSpec,Info) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!anonymous(DClass,NodeSpec,Info))
	.


:-light_tabular name_anonymous/3.

name_anonymous(Class,NodeSpec,N) :-
	every(( '$answers'(anonymous_closure(Class,NodeSpec,Info)),
		Info = nodefeature(FS),
		record_without_doublon( nodefeature(Class,N,FS) )
	      ))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-finite_set(rel,[eq,left,right,father,son,ancestor,descendant,wleft,wright,dom,any]).
:-light_tabular node_rel/4.
:-mode(node_rel/4,+(+,+,+,-)).

node_rel(Class,A,B,R::rel[]) :-
	( A==B -> R = eq
	; '$answers'( precedes_closure(Class,A,B,strict) ) -> R = left
	; '$answers'( precedes_closure(Class,B,A,strict) ) -> R = right
	; '$answers'( dominates_closure(Class,A,B,1) ) -> R = father
	; '$answers'( dominates_closure(Class,B,A,1) ) -> R = son
	; '$answers'( dominates_closure(Class,A,B,+) ) -> R = ancestor
	; '$answers'( dominates_closure(Class,B,A,+) ) -> R = descendant
	;  '$answers'( dominates_closure(Class,A,C,_) ),
	  '$answers'( dominates_closure(Class,B,C,_) ) -> R = dom
	; '$answers'( precedes_closure(Class,A,B,dom) ),
	  '$answers'( precedes_closure(Class,B,A,dom) ) -> R = dom
	; '$answers'( precedes_closure(Class,A,B,dom) ) -> R = wleft
	; '$answers'( precedes_closure(Class,B,A,dom) ) -> R = wright
	; R = any
	),
%%	format('Node rel ~w ~w => ~w\n',[A,B,R]),
	true
	.
