/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008, 2009, 2011, 2012, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tools.pl -- Auxiliary tools
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-import{ file=>'format.pl',
	   preds => [format/2,warning/2,format/3,format_hook/4] }.

:-require 'forest.pl'.

:-std_prolog record_without_doublon/1.

record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

:-std_prolog
	update_counter/2,
	value_counter/2
	.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_inc(M,V)
        ;   V=0,
            mutable(M,1),
            record( counter(Name,M) )
        )
        .

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 0
        )
        .

:-std_prolog verbose/2.

verbose(Fmt,Args) :-
	\+ option('-verbose') xor format(Fmt,Args)
	.

:-std_prolog verbose_dump/2.

verbose_dump(Fmt,Args) :-
	( recorded( dump_stream(Stream) ) ->
	  format(Stream,Fmt,Args)
	;
	  true
	)
	.

:-std_prolog mgcomp!warning/2.

mgcomp!warning(Fmt,Args) :-
	( option('-warning') ->
	    format(2,'** WARNING ** ',[]),
	    format(2,Fmt,Args)
	;
	    true
	)
	.

:-std_prolog usage/0.
:-extensional usage_info/3.

:-light_tabular option/1.

option(X) :-
	(X = [_|_] ->
	 domain(Opt,X)
	;
	 X=Opt
	),
	option(Opt,_).

:-light_tabular option/2.

option(Opt,Args) :-
	argv(L),
	( usage_info(Opt,ArgsPat,Help) ->
	  ( length(ArgsPat,N),
	    length(Args,N) ->
	    option_with_args_aux(Opt,Args,L)
	  ;
	    warning('Not a valid option ~w\n',[Opt]),
	    usage,
	    exit(1)
	  )
	;
	  warning('Not a valid option ~w\n',[Opt]),
	  usage,
	  exit(1)
	)
	.

:-std_prolog option_with_args_aux/3.

option_with_args_aux(Opt,Args,[A|L]) :-
	(Opt=A,
	 check_args(Opt,Args,L)
	xor option_with_args_aux(Opt,Args,L)
	).

:-std_prolog check_args/3.

check_args(Opt,Args,L) :-
	(Args = [] -> true
	; L = [] ->
	 warning('Not a valid option call ~w\n',[Opt]),
	 usage,
	 exit(1)
	; Args=[A|_Args],L=[A|_L] ->
	 (usage_info(A,_,_) ->
	  warning('Not a valid value ~w for option ~w\n',[A,Opt]),
	  usage,
	  exit(1)
	 ;
	  check_args(Opt,_Args,_L)
	 )
	;
	 fail
	)
	.
	 
:-light_tabular deep_module_shift/3.
:-mode(deep_module_shift/3,+(+,+,-)).

deep_module_shift(F,NS,ShiftF) :-
	( NS = [] ->
	    F = ShiftF
	;   
	    deep_module_shift_aux(F,NS,ShiftF)
	).

:-std_prolog deep_module_shift_aux/3.

deep_module_shift_aux(F,Module,ShiftF) :-
	atom_module(F,Module1,F1),
        ( F == [] ->
	    ShiftF = Module
	;   Module1 == [] ->
            atom_module(ShiftF,Module,F)
        ;   Module == Module1 -> %% dont deep shift if same module
            ShiftF = F
        ;   
            deep_module_shift_aux(Module1,Module,ShiftModule1),
            atom_module(ShiftF,ShiftModule1,F1)
        )
        .

:-light_tabular deep_module_unshift/3.
:-mode(deep_module_unshift/3,+(+,+,-)).

deep_module_unshift(F,NS,UnshiftF) :-
	( NS = [] ->
	    UnshiftF = F
	;   
	    deep_module_unshift_aux(F,NS,UnshiftF)
	),
	(   deep_module_shift_aux(UnshiftF,NS,F)
	xor warning('pb module unshifting ~w ~w => ~w\n',[F,NS,UnshiftF])
	)
	.

:-std_prolog deep_module_unshift_aux/3.

deep_module_unshift_aux(F,Module,UnshiftF) :-
	atom_module(F,Module1,F1),
	( Module1 = [] ->
	    fail
	;   Module1 = Module ->
	    UnshiftF = F1
	;   
	    deep_module_unshift_aux(Module1,Module,UnshiftModule1),
	    atom_module(UnshiftF,UnshiftModule1,F1)
	)
	.

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .


:-std_prolog delete_address/1.

delete_address(X) :-
	'$interface'( 'object_delete'(X:ptr),[return(bool)]).

:-std_prolog erase_relation/1.

erase_relation( Rel ) :-
	%% Erase both Call and Return items
	item_term(I::'*RITEM*'(Call,_),Rel),
	every(( recorded(C::'*CITEM*'(Call,Call),Add),
%		format('erase ~w\n',[C]),
		delete_address(Add)
	      )),
	every(( recorded(I,Add),
%		format('erase ~w\n',[I]),
%%		format(2,'  Erase rel ~w\n',[Rel]),
		delete_address(Add),
%		delete_upwards(Add),
		true
	      )
	     ),
	every(( recorded( I :> _, Add ),
		delete_address(Add),
		true
	      )),
% 	Rel =.. [Pred,Class|Args],
% 	Rel2 =.. [Pred,_Class|Args],
% 	item_term(I2::'*RITEM*'(_,_),Rel2),
% 	every(( recorded(Cont2::(I2 :> _),Add),
% 		format('delete cont ~w\n',[Cont2]),
% 		delete_address(Add),
% 		true
% 	      )),
	true
	.

:-std_prolog delete_upwards/1.

delete_upwards(Add) :-
	\+ Add == 0,
	every(( forest!true_forest(Add,Forest),
		( Forest = and(Add_X,Y),
		  ( delete_upwards(Add_X)
		  ;
		    fail,
		    ( Y = _:Add_Y xor Y=Add_Y ),
		    \+ Add_Y == 0,
		    recorded(O,Add_Y),
		    tab_item_term(O,T),
		    domain(T,[deep_module_shift(_,_,_),deep_module_unshift(_,_,_)]),
		    delete_upwards(Add_Y)
		  )
		)
	      )),
	delete_address(Add)
	.

:-light_tabular tab_item_term/2.
:-mode(tab_item_term/2,+(-,-)).

tab_item_term(I,T) :- item_term(I,T).

:-std_prolog forest_follow_indirect/2.

forest_follow_indirect(Forest,XForest) :-
	( Forest = indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = _ : indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = XForest
	),
%	format('deref forest ~w => ~w\n',[Forest,XForest]),
	true
	.

:-xcompiler
forest!true_forest(Add,Forest) :-
%	format('process add=~w\n',[Add]),
	forest!forest(Add,_Forest),
%	format('=>got ~w\n',[_Forest]),
	forest_follow_indirect(_Forest,Forest)
	.


format_hook(0'Z,Stream,[Bindings|R],R) :-
	mutable_read(Bindings,L),
	format(Stream,'Bindings:\n',[]),
	every(( domain(X:V,L),
		format(Stream,'\t~w -> ~w\n',[X,V])
	      ))
	.

