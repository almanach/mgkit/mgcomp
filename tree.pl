/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2006, 2007, 2008, 2009, 2011, 2012, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tree.pl -- Tree enumeration
 *
 * ----------------------------------------------------------------
 * Description
 *
 *  For neutral classes, enumerate trees from closure relations
 *

 Principle of the algorithm:

 It goes from root to leaves, tabulating all possible trees for each node.

 tree(+Class,+N,+DomList,-Tree)
    
      DomList is a list of nodes dominated by some ancestor of N
      They are to be included in generated trees Tree.

      First, all nodes dominated by N but not by an ancestor of N are
      collected in LL

      LL and DomList are merged and ordered as [C_1 .. C_n]
      where each C_i=[N_{i,0} .. N_{i,n_i}]
      and satisfying the following conditions:

           - N_{i,j} j>1 is not a son of N

	   - There is no precedence relation between N_{i,0} and N_{i,j} j>1
	   (N_{i,j} are going to be descendants of N_{i,0})

	   - C_k should not precedes C_i if k>i 
	   i.e N_{k,l} < N_{i,j} => k <= i

	   - Classe nodes are internally ordered
	   i.e. N_{i.l} < N_{i,j} => l <= j
      
    Subtrees are generated in order for each C_i,
    returned by tree(Class,N_{i,0},[C_{i,1},...,C_{i,n_i}],SubTree_i)

 Note:

   When only father relations are used in a Meta Grammar, DomL is
always empty. It means that the present algorithm has no overcost in
this special case (w.r.t. a more specific algorithm only designed for
this case).
 
 * ----------------------------------------------------------------
 */

 :-import{ file=>'format.pl',
	   preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-require
	'closure.pl',
	'toplevel.pl'
	.


:-finite_set(srel,[right,left,free,in]).

:-extensional seq_rel_aux/4.

seq_rel_aux(N,O, (O,[N]), right).
seq_rel_aux(N,O, ([N],O), left).
seq_rel_aux(N,O, ([N] ## O), free).
seq_rel_aux(N,O, (O ## [N]), free).

mgcomp!toplevel(Class,tree(Root,Tree)) :-
	'$answers'(neutral_class(Class)),
	'$answers'(root(Class,Root)),
	tree(Class,Root,[],[],Tree)
	.

mgcomp!toplevel(Class,tree(Tree)) :-
	'$answers'(neutral(Class)),
	'$answers'(root(Class,Root)),
	tree(Class,Root,[],[],Tree)
	.


:-std_prolog sorted_interleave/3.

sorted_interleave(X,Y,Z) :-
	%% Z = (X ## Y).
	( X @< Y -> Z = (X ## Y) ; Z = (Y ## X)).


%:-std_prolog (tree)/3.

:-light_tabular (tree)/3.
:-mode((tree)/3,+(+,+,-)).

%% Generating all possible trees from a description
tree(Class,N,Tree) :-
	\+ class2dump(Class,_),
	mutable(M,[],true),
	every(( '$answers'( pfather(Class,N,_N,weak:[]) ),
		node_rel(Class,N,_N,rel[dom,any]),
		mutable_read(M,_L),
		mutable(M,[_N|_L])
	      )),
	mutable_read(M,DomL),
%%	format('Try tree class=~w N=~w DomL=~w\n',[Class,N,DomL]),
	tree(Class,N,DomL,[],Tree),
%%	tree(Class,N,[],[],Tree),
%%	format('Got tree class=~w tree=~w\n',[Class,Tree]),
	true
	.


%% Reusing dumped class
tree(Class,N,Tree) :-
	class2dump(Class,DClass),
	dump!fact(DClass,dump!tree(DClass,N,Tree)),
	verbose('Reused dumped tree ~w ~w ~w\n',[Class,N,Tree]),
	verbose_dump('%% Reused dumped tree ~w ~w ~w\n',[Class,N,Tree]),
	true
	.

:-light_tabular (tree)/5.
:-mode((tree)/5,+(+,+,+,+,-)).

%% Generate all trees rooted at N and including in right order
%% all nodes in DomL (that may be dominated by N)
%% Loop is a list of weak ancestors of N that may potentially occur
%% as sons of N (Lx pather of N and N pfather of Lx)
tree(Class,N,DomL,Loop,Tree) :-
	%% Get all nodes whose higher dominant is N
%%	format('Try internal tree ~w N=~w Dom=~w Loop=~w\n',[Class,N,DomL,Loop]),
	mutable(M,DomL,true),
	every((
	       '$answers'(pfather(Class,N,NN,_:N)),
	       ('$answers'(node_closure(Class,NN)) ->
		    true
	       ;
	       name2class(Class,XClass),
	       format(2,'*** virtual node ~w in class ~w, maybe an error ?\n',[NN,XClass]),
	       fail
	       ),
	       \+ domain(NN,Loop),
	       \+ domain(NN,DomL),
	       %% NN is not an guarded node with a failed positive guard
	       %% and no negative condition to enforce
	       %% => NN can be discarded
	       \+ (   '$answers'(nodeguard_closure(Class,NN,+,[])),
		      (	  %% no negative guard
			  \+ ('$answers'(nodeguard_closure(Class,NN,-,_G)), _G \== true)
		      ;	  %% or a fail negative guard
			  %% => case of failure for current tree
			  %% because positive and negative guards both fail
			  %% no failure if some ancestor of NN is optional
			  '$answers'(nodeguard_closure(Class,NN,-,[]))
		      )
		  ),
	       mutable_read(M,L),
	       mutable(M,[NN|L])
	      )),
	mutable_read(M,L),
%%	format('Node order ~w ~w ~w ~w\n',[Class,N,L,DomL]),
	node_order(Class,N,L,_LL),
%%	format('Node order -> ~w\n',[_LL]),
	apply_firstlast_constraint(Class,_LL,LL),
%%	format('Check brother ~w ~w \n',[Class,LL]),
	check_brothers(Class,LL),
%%	format('Node order after last constraints -> ~w\n',[LL]),
	('$answers'(pfather(Class,N,NN,weak:_)),
	 '$answers'(pfather(Class,NN,N,weak:_)) ->
	 Loop2 = [N|Loop]
	;
	 Loop2=Loop
	),
	node_traverse(Class,LL,Loop2,SubTrees),
	Tree = [N|SubTrees]
	.

:-std_prolog check_brothers/2.

check_brothers(Class,LL) :-
	collect_brothers(LL,Brothers,[]),
	check_brothers_aux(Class,Brothers).

:-rec_prolog collect_brothers/3.

collect_brothers(true,L,L).
collect_brothers([N|_],[N|L],L).
collect_brothers((C1,C2),XL,L) :-
	collect_brothers(C1,XL1,L),
	collect_brothers(C2,XL,XL1)
	.
collect_brothers((C1 ## C2),XL,L) :-
	collect_brothers(C1,XL1,L),
	collect_brothers(C2,XL,XL1)
	.

:-std_prolog check_brothers_aux/2.

check_brothers_aux(Class,L) :-
	%% No element of L has a brother not in L
	\+ ( 	domain(N,L),
	       (   '$answers'( brother_closure(Class,N,M) )
%%	       ;   '$answers'( brother_closure(Class,M,N) )
	       ),
	       \+ domain(M,L)
	   )
	.

:-rec_prolog check_last/4.

check_last(Class,Last::[N|_],true,Last) :-
	is_last(Class,N).
check_last(Class,(C1,C2),XC,Last) :-
	check_last(Class,C2,XC2,Last),
	( XC2 = true ->
	    XC = C1
	;   
	    XC = (C1,XC2)
	)
	.

:-rec_prolog check_first/4.

check_first(Class,First::[N|_],true,First) :-
	is_first(Class,N).
check_first(Class,(C1,C2),XC,First) :-
	check_first(Class,C1,XC1,First),
	( XC1 = true ->
	    XC = C2
	;   
	    XC = (XC1,C2)
	)
	.

:-rec_prolog apply_firstlast_constraint/3.

apply_firstlast_constraint(Class,true,true).
apply_firstlast_constraint(Class,[],[]).
apply_firstlast_constraint(Class, C::[_|_], C ).
apply_firstlast_constraint( Class,(C1,C2), (XC1,XC2) ) :-
	apply_firstlast_constraint(Class,C1,XC1),
	\+ ( check_last(Class,XC1,_,_) ),
	apply_firstlast_constraint(Class,C2,XC2),
	\+ ( check_first(Class,XC2,_,_) )
	.
apply_firstlast_constraint(Class,(C1##C2),XC) :-
	apply_firstlast_constraint(Class,C1,XC1),
	apply_firstlast_constraint(Class,C2,XC2),
	(   check_last(Class,XC1,_XC1,Last1) xor Last1=none ),
	(   check_last(Class,XC2,_XC2,Last2) xor Last2=none ),

	(   check_first(Class,XC1,__XC1,First1) xor First1=none ),
	(   check_first(Class,XC2,__XC2,First2) xor First2=none ),

%%	format('firstlast c1=~w xc1=~w => f1=~w l1=~w _xc1=~w\n\t c2=~w xc2=~w => f2=~w l2=~w _xc2=~w\n',[C1,XC1,First1,Last1,_XC1,C2,XC2,First2,Last2,_XC2]),
	
	( Last1 \== none, Last2 \==none -> fail
	;   First1 \== none, First2 \==none -> fail
	; Last1 \== none, First1 \== none, _XC1=true -> fail
	;   Last2 \== none, First2 \== none, _XC2=true -> fail
	;   Last1 \== none ->
	    ( _XC1 = true ->
		XC = (XC2,Last1)
	    ;	First1 \== none ->
		check_first(Class,__XC1,__XC1_Minus_Last1,Last1),
		sorted_interleave(XC2,__XC1_Minus_Last1,Body),
		XC = ((First1,Body),Last1)
	    ;	First2 \== none ->
		sorted_interleave(__XC2,_XC1,Body),
		XC = ((First2,Body),Last1)
	    ;
		sorted_interleave(XC2,_XC1,Body),
		XC = (Body,Last1)
	    )
	;   Last2  \== none ->
	    ( _XC2 = true ->
		XC = (XC1,Last2)
	    ;	First2 \== none ->
		check_first(Class,__XC2,__XC2_Minus_Last2,Last2),
		sorted_interleave(XC1,__XC2_Minus_Last2,Body),
		XC = ((First2,Body),Last2)
	    ;	First1 \== none ->
		sorted_interleave(__XC1,_XC2,Body),
		XC = ((First1,Body),Last2)
	    ;	
		XC = ((XC1 ## _XC2),Last2)
	    )
	;   First1 \== none ->
	    ( __XC1 = true ->
		XC = (First1,XC2)
	    ;
		sorted_interleave(__XC1,XC2,Body),
		XC = (First1,Body)
	    )
	;   First2 \== none ->
	    ( __XC2 = true ->
		XC = (First2,XC1)
	    ;
		sorted_interleave(__XC2,XC1,Body),
		XC = (First2,Body)
	    )
	;
	    sorted_interleave(XC1,XC2,XC)
	)
	.
			
:-rec_prolog node_order/4.

node_order(_,_,[],true).
	
node_order(Class,N,[N1|L1],XL) :-
	node_order(Class,N,L1,XL1),
	node_insert(Class,N,N1,XL1,XL)
	.

%% Node Order should return a structure of the form:
%%      <order> := <class>
%%               | <order> , <order>     % sequence
%%               | <order> ## <order>    % interleave
%%      <class> := [<node>+]
%%
%% The first element of each class is the root of a subtree
%% The following constraints should hold:
%%          [N1,..N2..]            => not(N2 < N1 or N1 < N2)
%%          Order1 , Order2 and [N1,...] in Order1 and [N2,..N3..] in Order2
%%                           => N1 < N2 and not(N3 < N1)
%%          Order1 ## Order2 and [N1,..N3..] in Order1 and [N2,..N4..] in Order2
%%                           => not(N1 < N2 or N2 < N1) and not(N3 < N4)


:-std_prolog node_insert_rel/6.

node_insert_rel(Class,P,N,O,XO,R) :-
	node_insert(Class,P,N,O,XO),
	( seq_rel_aux(N,O,XO,R) xor R=in).

:-light_tabular node_insert/5.
:-mode(node_insert/5,+(+,+,+,+,-)).

%% insertion wrt void ordering
node_insert(Class,N,N1,true,[N1]). 

%% inserted wrt a class
%% may return  1-   [N1] , C
%%             2-   C, [N1]
%%             3-   [N1] ## C
%%             4-   [N1|C]
%%             5-   [N2,N1|R2]
node_insert(Class,N,N1,C::[N2|R2],XO) :-
%%	format('insert ~w ~w:\n',[N1,C]),
	node_insert_aux(0,Class,N,N1,C,XO),
%%	format('\t => ~w ~w->~w\n',[N1,C,XO]),
	true
	.

%% insertion wrt an ordered sequence
node_insert(Class,P,N,O::(O1,O2),XO) :-
%%	format('insert ~w ~w:\n',[N,O]),
	node_insert_rel(Class,P,N,O1,XO1,R1),
	node_insert_rel(Class,P,N,O2,XO2,R2),
%%	format('here xo1=~w xo2=~w r1=~w r2=~w\n',[XO1,XO2,R1,R2]),
	( R1= srel[right], R2=srel[right] ->
	  %% R2=right => R1=Right
	  XO = (O,[N])
	;   R1=srel[right] ->
	  XO = (O1,XO2)
	;   R1=srel[left], R2=srel[left] ->
	  %% R1=left => R2=left
	  XO = ([N],O)
	; R2 = srel[left] ->
	  XO= (XO1,O2)
	; R1 = srel[free],R2 = srel[free] ->
	    sorted_interleave([N],O,XO)
	; R1 = srel[free],R2=srel[in] ->
%%	  format('%% Do approximate R1 with XO1=~w XO2=~w\n',[XO1,XO2]),
	  ( %% Approximate R1 by right
	      XO = (O1,XO2)
	  ;   %% Approximate R1 by left, unless covered by free,free
	      \+ node_insert_rel(Class,P,N,O2,_,free),
	      XO = ([N],O)
	  )
	; R1 = srel[in], R2=srel[free] ->
%%	  format('%% Do approximate R2 with XO1=~w XO2=~w\n',[XO1,XO2]),
	  ( %% Approximate R2 by left
	    XO = (XO1,O2)
	  ;  %% Approximate R2 by right unless covered by free,free
	    \+ node_insert_rel(Class,P,N,O1,_,free),
	    XO = (O,[N])
	  )
	; %% R1 = srel[in] R2 = srel[in]
	  fail
	),
%%	format('\t => ~w ~w->XO=~w\tR1=~w XO1=~w R2=~w XO2=~w\n',[N,O,XO,R1,XO1,R2,XO2]),
	true
	.

%% insertion wrt an interleaved sequence
node_insert(Class,P,N,O::(O1 ## O2),XO) :-
%%	format('insert ~w ~w:\n',[N,O]),
	node_insert_rel(Class,P,N,O1,XO1,R1),
	node_insert_rel(Class,P,N,O2,XO2,R2),
	(   R1=srel[free], R2=srel[free] ->
	    %% N may interleave with both O1 and O2
	    sorted_interleave([N],O,XO)
	;   R1=srel[free] ->
	    sorted_interleave(O1,XO2,XO)
	;   R2=srel[free] ->
	    sorted_interleave(XO1,O2,XO)
	;   R1=srel[left],R2=srel[left] ->
	    %% N precedes both O1 and O2
	    XO = ([N],O)
	;   R1=srel[right],R2=srel[right] ->
	    %% N is preceded by both O1 and O2
	    XO = (O,[N])
	
	;   R1 = srel[right] ->
	    XO = (O1,XO2)
	;   R2 = srel[right] ->
	    XO = (O2,XO1)

	;   R1 = srel[left] ->
	    XO = (XO2,O1)
	;   R2 = srel[left] ->
	    XO = (XO1,O2)
	    
	;
	    fail
	),
%%	format('\t => ~w ~w->~w\tXO1=~w XO2=~w\n',[N,O,XO,XO1,XO2]),
	true
	.

%% node_insert_aux insert a node wrt a single node class

:-rec_prolog node_insert_aux/6.

node_insert_aux(free,Class,P,N,[],O) :-
	(
	    sorted_interleave([N],[],O)
	;   \+ '$answers'( pfather(Class,P,N,strong:P) ),
	    O = [N]
	)
	.

node_insert_aux(sup,Class,P,N,[],O) :-
	(   O = ([],[N])
	;   \+ '$answers'( pfather(Class,P,N,strong:P) ),
	    O = [N]
	)
	.

node_insert_aux(I,Class,P,N,C::[N1|C1],O) :-
%%	format('node insert aux I=~w P=~w N=~w C=~w\n',[I,P,N,C]),
	(  
	    ( node_rel(Class,N,N1,left)) ->
	    %% the following check is not necessary if we are sure about closure !
	    ( I == 0 ->
		\+ ( domain(N2,C), node_rel(Class,N,N2,rel[~ [any,left,wleft]])),
		%%		\+ is_last(Class,N),
		%% N precedes the root of C
		%% add a new class preceding C
		%% N can't become the root of C because it would precede one of its children (N1)
		O = ([N],C)
	    ;	%% N precedes some element of C but not its root
		%% N can only belong to C if it is a weak son of P
		\+ '$answers'( pfather(Class,P,N,strong:P) ),
		O = [N|C]
	    )
	; node_rel(Class,N,N1,wleft) ->
	  (   I=0 ->
	      ( \+ ( domain(N2,C), node_rel(Class,N,N2,rel[~ [left,wleft,any]])),
		%% no element of C precedes N => N may be before C
		O = ([N],C)
	      ; \+ '$answers'( pfather(Class,P,N,strong:P) ),
		'$answers'( pfather(Class,N1,N,weak:_) ),
		%% but N can also be a descendant of N1
		O = [N1,N|C1]
	      ; \+ '$answers'( pfather(Class,P,N1,strong:P) ),
		'$answers'( pfather(Class,N,N1,weak:_) ),
		\+ ( domain(N2,C), node_rel(Class,N,N2,rel[left,right,son,descendant])),
		O = [N|C]
	      )
	  ;   \+ '$answers'( pfather(Class,P,N,strong:P) ),
	      O = [N|C]
	  )
	;   ( node_rel(Class,N,N1,right), _Kind = strict
	    xor node_rel(Class,N,N1,wright), _Kind = dom
	    ) ->
	  ( I=0,
	    _Kind = dom,
	    %% stronger priority on using _Kind=strict
	    '$answers'( pfather(Class,N,N1,weak:_)),
	    \+ '$answers'( pfather(Class,P,N1,strong:P)),
	    \+ ( domain(_N,C), node_rel(Class,N,_N,rel[left,right,son,descendant])),
	    O = [N|C]
	  ; node_insert_aux(sup,Class,P,N,C1,O1),
	    ( O1 = (C1 , [N]) ->
	      O = (C,[N])
	    ;	(_Kind = dom ; I \== 0),
	      O = [N1|O1]
	    )
	  )
	;   ( I == 0 ->  J = free ; J = I),
%%	    format('INSERT AUX class=~w N=~w C=~w J=~w\n',[Class,N,C1,J]),
	    node_insert_aux(J,Class,P,N,C1,O1),
%%	    format('=> INSERT AUX  O1=~w\n',[O1]),
	    ( O1 = (C1 , [N]) ->
		O = (C,[N])
	    ;	(O1 = ([N] ## C1) ; O1 = (C1 ## [N])) ->
		(   I == 0,
		    \+ '$answers'( pfather(Class,P,N1,strong:P) ),
		    \+ ( domain(_N,C),
			 node_rel(Class,N,_N,rel[~ [any,dom,ancestor,father]])),
		    O = [N|C]
		;
		    \+ ( domain(_N,C), node_rel(Class,N,_N,rel[~ [any,free]])),
		    sorted_interleave([N],C,O)
%%		    O = ([N] ## C)
		)
	    ;
	      ( I==0 ->
		('$answers'( pfather(Class,N1,N,_:_) ),
		 \+ '$answers'( pfather(Class,P,N,strong:P) ),
		  O = [N1,N|C1]
		; \+ '$answers'( pfather(Class,P,N1,strong:P) ),
		 \+ ( domain(_N,C), node_rel(Class,N,_N,rel[left,right,son,descendant])),
		 O= [N|C]
		)
	      ;
		\+ '$answers'( pfather(Class,P,N,strong:P) ),
		O = [N1,N|C1]
	      )
	    )
	),
%%	format('===> I=~w P=~w N=~w C=~w O=~w\n',[I,P,N,C,O]),
	true
	.

:-light_tabular is_last/2.

is_last(Class,N) :-
	'$answers'(nodefeature_closure(Class,N,FS)),
	class_bindings(Class,Bindings),
%%	format('TRY LAST NODE ~w ~w\n',[Class,N]),
	(   fs!value(FS,rank,rank:last,Bindings)
	xor fs!value(FS,rank,rank:single,Bindings)
	),
%%	format('FOUND LAST NODE ~w ~w\n',[Class,N]),
	true
	.

:-light_tabular is_first/2.

is_first(Class,N) :-
	'$answers'(nodefeature_closure(Class,N,FS)),
	class_bindings(Class,Bindings),
%%	format('TRY FIRST NODE ~w ~w\n',[Class,N]),
	(   fs!value(FS,rank,rank:first,Bindings)
	xor 	fs!value(FS,rank,rank:single,Bindings)
	),
%%	format('FOUND FIRST NODE ~w ~w\n',[Class,N]),
	true
	.

:-rec_prolog node!sort_list/2.
:-rec_prolog node!sort_list_aux/3.

node!sort_list([],[]).
node!sort_list([A|L],LL) :-
	node!sort_list(L,L1),
	node!sort_list_aux(A,L1,LL)
	.
node!sort_list_aux(A,[],[A]).
node!sort_list_aux(A,[B|L],LL) :-
        ( A == B ->
	  LL = [B|L]
	;   A @< B ->
	  LL=[A,B|L]
        ;   
	  LL=[B|L1],
	  node!sort_list_aux(A,L,L1)
        )
        .

:-rec_prolog node_traverse/4.

node_traverse(_,true,Loop,[]).
node_traverse(Class,[N|Dom],Loop,[Tree]) :-
	node!sort_list(Dom,SortedDom),
	node!sort_list(Loop,SortedLoop),
	tree(Class,N,Dom,Loop,Tree)
	.
node_traverse(Class,(C1,C2),Loop,T) :-
	( C1 = (A,B) ->
	    node_traverse(Class,(A,(B,C2)),Loop,T)
	;   
	    node_traverse(Class,C1,Loop,[T1]),
	    node_traverse(Class,C2,Loop,T2),
	    T = [T1|T2]
	)
	.

node_traverse(Class,(C1 ## C2),Loop,T) :-
	( C1 = (A ## B) ->
	    node_traverse(Class,(A ## (B ## C2)),Loop,T)
	;   try_preserve_tig(Class,(C1 ## C2)) ->
	    %% To get TIG tree, we remove interleaving covering foot nodes
	    %% by building two trees
	    %% this scheme does not work for complex foot-covering interleaving cases
	    node_traverse(Class,C1,Loop,[T1]),
	    node_traverse(Class,C2,Loop,[T2]),
	    (	T = [T1,T2] ; T = [T2,T1] )
	;   
	    node_traverse(Class,C1,Loop,T1),
	    node_traverse(Class,C2,Loop,T2),
	    (	T2 = [T3], T3 = (_ ## _) xor T2 = T3 ),
	    T = [(T1 ## T3)]
	)
	.

:-std_prolog try_preserve_tig/2.

try_preserve_tig(Class,N::([N1] ## [N2])) :-
	option('-trytig'),
	(   is_foot(Class,N1) xor is_foot(Class,N2))
	.

:-std_prolog is_foot/2.

is_foot(Class,N) :-
	'$answers'(nodefeature_closure(Class,N,FS)),
	class_bindings(Class,Bindings),
	fs!value(FS,type,type:foot,Bindings)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test wrt an expected set of answer trees

:-std_prolog test_trees/1.

test_trees(Class) :-
	name2class(Class,XClass),
	mutable(M,true),
	every(( recorded(answer(XClass,T)),
		standardize_answer(T,TT),
%%		format('Standardize answer ~w => ~w\n',[T,TT]),
		record_without_doublon( sanswer(XClass,TT)))),
	every(( '$answers'(tree(Class,Root,T)),
		standardize_answer(T,TT),
%%		format('Standardize tree ~w => ~w\n',[T,TT]),
		record_without_doublon( stree(Class,Root,TT)))),
	%%
	every(( recorded(sanswer(XClass,T)),
		T = [Root|_],
		\+ recorded( stree(Class,Root,T) ),
		warning('class ~w: missing expected tree ~w\n',[XClass,T]),
		mutable(M,error)
	      ;	  recorded( stree(Class,Root,T) ),
		\+ recorded(sanswer(XClass,T)),
		warning('class ~w:  unexpected tree ~w\n',[XClass,T]),
		mutable(M,error)
	      )),
	( mutable_read(M,true) ->
	  format('ok -- class ~w\n',[XClass])
	;
	  format('fail -- class ~w\n',[XClass])
	)
	.

:-rec_prolog standardize_answer/2.
:-rec_prolog standardize_answer_aux/2.

:-std_prolog standardize_answer_sort/3.
:-std_prolog standardize_answer_sort_aux/3.

standardize_answer((T1 ## T2),XT) :-
	standardize_answer(T2,XT2),
	standardize_answer_sort(T1,XT2,XT).

standardize_answer_sort(T1,T2,XT) :-
	( T1 = (T11 ## T12) ->
	    standardize_answer_sort(T12,T2,XT12),
	    standardize_answer_sort(T11,XT12,XT)
	;   
	    standardize_answer(T1,XT1),
	    standardize_answer_sort_aux(XT1,T2,XT)
	).
standardize_answer_sort_aux(T1,T2,XT) :-
	(   T2 = (T21 ## T22) ->
	    ( T1 @< T21 ->
		XT = (T1 ## T2)
	    ;	standardize_answer_sort_aux(T1,T22,XT22),
		XT = (T21 ## XT22)
	    )
	;   T1 @< T2 ->
	    XT = (T1 ## T2)
	;   XT = (T2 ## T1)
	).
standardize_answer([],[]).
standardize_answer([A|Sons],[A|XSons]) :-
	standardize_answer_aux(Sons,XSons).
standardize_answer_aux([],[]).
standardize_answer_aux([T1|L],[XT1|XL]) :-
	standardize_answer(T1,XT1),
	standardize_answer_aux(L,XL)
	.
