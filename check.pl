/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  check.pl -- Class integrity checking
 *
 * ----------------------------------------------------------------
 * Description
 *
 * ----------------------------------------------------------------
 */

 :-import{ file=>'format.pl',
	  preds => [format/2,warning/2,format/3,format_hook/4] }.

:-include 'header.plh'.

:-require
	'tools.pl',
	'fs.pl',
	'closure.pl'
	.

:-finite_set(nodetype,[subst,anchor,lex,foot,std]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check terminal classes
:-std_prolog check_terminals/0.

check_terminals :-
	every(( terminal(C),
		(   provides_closure(C,R,NS),
		    needs_closure(C,R,NS),
		    atom_module(XR,NS,R),
		    mgcomp!warning('class ~w: +- ~w\n',[C,XR]))))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check neutral classes
:-std_prolog check_class/1.

check_class(Class) :-
	option('-warning'),
	mutable(M,false),
	class_bindings(Class,Bindings),
	%% No more than one root node
	class_violation(('$answers'(root(Class,N1)),
			 '$answers'(root(Class,N2)),
			 N1 \== N2),
			M,Class,
			'more than one root ~w and ~w',[N1,N2]),
	every((repnode(Class,N),
	       %% Check that subst, foot, lex or anchor nodes are leaves
	       class_violation(('$answers'(nodefeature_closure(Class,N,FS)),
				fs!value(FS,
					 type,
					 type:Type::nodetype[subst,anchor,lex,foot],
					 Bindings),
				'$answers'(dominates_closure(Class,N,_,_))),
			       M,Class,
			       'expected leaf node for ~w (type=~w)',[N,Type]),
	       %% No node should precede itself
	       class_violation('$answers'(precedes_closure(Class,N,N,strict)),
			       M,Class,
			       'preceding loop for ~w',[N]),
	       ancestor(Class,N,Ancestor),
	       %% No node should precedes one of its ancestor (includind itself)
	       class_violation(('$answers'(precedes_closure(Class,N,Ancestor,strict)) ;
				'$answers'(precedes_closure(Class,Ancestor,N,strict)) ), 
			       M,Class,
			       'preceding/ancestor clash between ~w and ~w',
			       [Ancestor,N]),
	       %% No node should be its own proper ancestor
	       class_violation('$answers'(dominates_closure(Class,N,Ancestor,_)),
			       M,Class,
			       'ancestor loop for ~w\n',[N])
	       ))
	.

:-xcompiler((class_violation(Cond,M,Class,Fmt,Args) :-
	    ( Cond ->
		class_warning(M,Class,Fmt,Args)
	    ;
		true
	    ))).

:-std_prolog class_warning/4.

class_warning(M,Class,Fmt,Args) :-
	(   mutable_read(M,true)
	xor
	name2class(Class,XClass),
	format(2,'** WARNING ** class ~w\n',[XClass])),
	mutable(M,true),
	format(2,'\t',[]),
	format(2,Fmt,Args),
	nl(2)
	.

