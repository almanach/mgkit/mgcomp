/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2015, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  display.pl -- Display MG information
 *
 * ----------------------------------------------------------------
 * Description
 *
 *     Display MG information using various formats
 *
 *
 * ----------------------------------------------------------------
 */

:-op(  700, xfx, [?=]). % for default value
:-xcompiler((X ?= Y :- (X = Y xor true))).

 :-import{ file=>'format.pl',
	   preds => [format/2,format/3,format_hook/4,error/2] }.

:-include 'header.plh'.

:-require
	'tools.pl',
	'fs.pl',
	'closure.pl',
	'crossing.pl',
	'toplevel.pl'
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display raw information

mgcomp!toplevel(Class,display) :-
	class_full_display(Class),
	nl
	.

:-std_prolog class_full_display/1.

class_full_display(Class) :-
	name2class(Class,XClass),
	format('Class ~w\n',[XClass]),
	every((   
		  derived_res(Class,Res,NS),
		  atom_module(XRes,NS,Res),
		  format('\t~w\n',[XRes]))),
	every((class_bindings(Class,Bindings),
	       mutable_read(Bindings,L),
	       domain(B,L),
	       format('\tBinding: ~w\n',[B])
	      )),
	every((
	       description_closure(Class,Desc),
	       format('\tDescription: ~w\n',[Desc])
	      )),
	every((
	       repnode(Class,Node),
	       ( root(Class,Node)  ->
		   format('\tNode ~w [Root] { ',[Node])
	       ;   '$answers'(node_closure(Class,Node)) ->
		   format('\tNode ~w { ',[Node])
	       ;   option('-virtual') ->
		   format('\tNode ~w [Virtual] { ',[Node])
	       ;   
		   fail
	       ),
	       every(( (Node=N2 ; '$answers'(equals_closure(Class,Node,N2))),
		       (   option('-virtual') xor '$answers'(node_closure(Class,N2)) ),
		       format('~w ',[N2]))),
	       format('}\n',[]),
	       every(( '$answers'(pfather(Class,Node,N2,K:_)),
		       (   option('-virtual') xor '$answers'(node_closure(Class,N2))),
		       format('\t\t~w ~w father ~w\n',[Node,K,N2]))),
	       every(( '$answers'(pfather(Class,Node,N1,_)),
		       '$answers'(pfather(Class,Node,N2,_)),
		       (   option('-virtual')
		       xor  '$answers'(node_closure(Class,N1)),
			   '$answers'(node_closure(Class,N2))),	    
		       node_rel(Class,N1,N2,_KP), _KP=rel[left,wleft],
		       (_KP=left -> KP=strict ; KP=dom),
		       format('\t\t~w < ~w [~w]\n',[N1,N2,KP]))),
	       every(( '$answers'(brother_closure(Class,Node,N2)),
		       (   option('-virtual') xor '$answers'(node_closure(Class,N2)) ),
		       format('\t\t~w brother ~w\n',[Node,N2]))),
	       every(( '$answers'(nodefeature_closure(Class,Node,FS)),
		       format('\tFS: ~w\n',[FS]) ))
	      ))	
 .


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dump

:-light_tabular base_class_dump/2.

base_class_dump(Stream,Class) :-
	every((super(Class,Super),
	       base_class_dump(Stream,Super))),
	format(Stream,'\n\n%% Base class ~w\n\n',[Class]),
	format(Stream,'class(~q).\n',[Class]),
	every((domain(Kind/N,[ super/2,
			       needs/2,
			       provides/2,
			       description/2,
			       node/2,
			       nodefeature/3,
			       guard/4,
			       father/3,
			       dominates/3,
			       precedes/3,
			       equals/3,
			       equation/3,
			       anonymous/3,
			       reducer/2
			      ]),
	       functor(Fact,Kind,N),
	       Fact =.. [_,Class|_],
	       recorded(Fact),
	       format(Stream,'~q.\n',[Fact])
	      ))
	.
	

:-std_prolog neutral_class_dump/2.

neutral_class_dump(Stream,Class) :-
	name2class(Class,XClass),
	update_counter(dump_class,Id),
	record(dump!map(Class,Id)),
	format(Stream,'\n\n%% Dump Neutral Class ~w\n\n',[Id]),
	( class2dump(Class,DClass) ->
	  format(Stream,'%% reusing former dumped class ~w with mapping ~w\n',[DClass,Class])
	;
	  true
	),
	format(Stream,'dump!neutral_class(~w,~q).\n',[Id,XClass]),
	every(( domain(XTerm,XClass),
		atom_module(XTerm,NS,Term),
		format(Stream,'dump!dependency(~w,~q,~q).\n',[Id,Term,NS])
	      )),
%%	every(( xsuper(Class,Super,NS),
%%		name2class(Super,[XSuper]),
%%		format(Stream,'dump!xsuper(~w,~q,~q).\n',[Id,XSuper,NS])
%%	      )),
%%	every(( derived_res(Class,= Res,NS),
%%		format(Stream,'dump!resource(~w,~q,~q).\n',[Id,Res,NS]))),
	every(( '$answers'(class_bindings(Class,Bindings)),
		mutable_read(Bindings,L),
		format(Stream,'dump!bindings(~k,~q).\n',[Id,L])
	      )),
	every(( '$answers'(description_closure(Class,Desc)),
		format(Stream,'dump!description(~w,~q).\n',[Id,Desc]) )),
	every(( repnode(Class,Node),
		format(Stream,'dump!repnode(~w,~q).\n',[Id,Node]) )),
	every(( '$answers'(root(Class,Node)),
		format(Stream,'dump!root(~w,~q).\n',[Id,Node]))),
% 	every(( '$answers'(equals_closure(Class,Node,N2)), N2 \== Node,
% 		format(Stream,'dump!equals(~w,~q,~q).\n',[Id,Node,N2]) )),
	every(( '$answers'(equals_closure(Class,Node,N2)),
		format(Stream,'dump!equals(~w,~q,~q).\n',[Id,Node,N2]))),
	every(( '$answers'(pfather(Class,Node,N2,K:_)),
		format(Stream,'dump!pfather(~w,~q,~q,~w).\n',[Id,Node,N2,K]))),
	every(( '$answers'(precedes_closure(Class,Node,N2,K)),
		format(Stream,'dump!precedes(~w,~q,~q,~w).\n',[Id,Node,N2,K]))),
	every(( '$answers'(dominates_closure(Class,Node,N2,K)),
		format(Stream,'dump!dominates(~w,~q,~q,~w).\n',[Id,Node,N2,K]))),
	every(( '$answers'(brother_closure(Class,Node,N2)),
		format(Stream,'dump!brother(~w,~q,~q).\n',[Id,Node,N2]))),
	every(( '$answers'(nodefeature_closure(Class,Node,FS)),
		format(Stream,'dump!nodefeature(~w,~q,~q).\n',[Id,Node,FS]) )),
	every(( '$answers'(nodeguard_closure(Class,Node,Kind,F)),
		format(Stream,'dump!nodeguard(~w,~q,~w,~q).\n',[Id,Node,Kind,F]))),
	every(( '$answers'(anonymous_closure(Class,Node,Spec)),
		format(Stream,'dump!anonymous(~w,~q,~q).\n',[Id,Node,Spec]))),
	every(( '$answers'(reducer_closure(Class,FS)),
		format(Stream,'dump!reducer(~w,~q).\n',[Id,FS]))),
	true
 .

:-std_prolog tree_dump/4.

tree_dump(Stream,Class,Root,Tree) :-
	recorded(dump!map(Class,Id)),
	format(Stream,'dump!tree(~w,~q,~q).\n',[Id,Root,Tree])
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display Trees

mgcomp!toplevel(Class,display(Tree,tree)) :-
	format('~T\n',[Tree])
	.

format_hook(0'T,Stream,[[Father|Nodes]|R],R) :-
	(   Nodes = [] ->
	    format(Stream,'~w',[Father])
	;
	    format(Stream,'~w(~L)',[Father,['~T',','],Nodes])
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display simple trees (cat+type information)

mgcomp!toplevel(Class,display(Tree,simple)) :-
	simple!node(Class,Tree),
	nl
	.

:-std_prolog
	simple!node/2.

simple!node(Class,Tree::[Node|Child]) :-
	'$answers'(nodefeature_closure(Class,Node,FS)),
%%	nodefeature_closure(Class,Node,FS),
	class_bindings(Class,Bindings),
	fs!value(FS,cat:'??',cat:Cat,Bindings),
	fs!value(FS,type:std,type:Type,Bindings),
	simple!typemap(Type,Type2),
	format('~w~w',[Cat,Type2]),
	( Child=[First|Others] ->
	    format('(',[]),
	    simple!node(Class,First),
	    every((domain(Other,Others),
		   format(', ',[]),
		   simple!node(Class,Other))),
	    format(')',[])
	;   
	    true
	)
	.

:-extensional simple!typemap/2.

simple!typemap(lex,'#').
simple!typemap(subst,'!').
simple!typemap(anchor,'&').
simple!typemap(foot,'*').
simple!typemap(std,'').
simple!typemap(coanchor,'&-').
simple!typemap(skip,'').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display XML information using TAG1 DTD 

mgcomp!toplevel(Class,display(Tree,xml)) :-
	xml!tree(Class,Tree),
	nl
	.

:-std_prolog
	xml!tree/2,
	xml!family/3,
	xml!tree/3,
	xml!narg/3,
	xml!guards/3,
	xml!guard/3,
	xml!fstruct/3,
	xml!fs/3,
	xml!val/3,
	xml!xnode/3,
	xml!interleave/3,
	xml!sequence/3,
	xml!nodeopt/3,
	xml!node/3,
	xml!elt/4,
	xml!start_elt/4,
	xml!end_elt/3,
	xml!opening,
	xml!closing
	.

xml!opening :-
	( option('-o',[OutputFile]) ->
	  current_output(Old_Stream),
	  absolute_file_name(OutputFile,AF),
	  open(AF,write,Stream),
	  set_output(Stream),
	  record( old_output(Old_Stream) )
	;
	  true
	),
	format('<?xml version="1.0" encoding="ISO-8859-1"?>',[]),
	mutable(Ctx,0),
	Handler=class([],[],[]),
	xml!start_elt(Handler,tag,[],Ctx)
	.

xml!closing :-
	mutable(Ctx,1),
	Handler=class([],[],[]),
	xml!end_elt(Handler,tag,Ctx),
	format('\n',[]),
	( recorded( old_output(Old_Stream) ) ->
	  set_output(Old_Stream)
	;
	  true
	)
	.

xml!tree(Class,Tree) :-
	class_bindings(Class,Bindings),
	update_counter(tree,Id),
	Handler=class([],Class,Bindings),
	name2class(Class,XClass),
	Name = [Id|XClass],
	mutable(Ctx,1),
	xml!family(Handler,family(Id,Name,Tree),Ctx),
	reset_binding_display(Bindings)
	.

event_super_handler(class(S,Class,Bindings),default(S)).

%% Theoretically no need for the following
%% but seems to be neeed (bug somewhere in libdyalogxml ?)
xml!elt(H::class(Stream,_,_),Name,Attr,Ctx) :-
        Attr ?= [],
        '$interface'('XML_my_start_element_print'(Stream:output,Name:term,Attr:term,true:bool),[])
        .

xml!start_elt(class(Stream,_,_),
	      Name,
	      Attr,
	      Ctx
	     ) :-
	Attr ?= [],
	Empty ?= false,
	'$interface'('XML_my_start_element_print'(Stream:output,
						  Name:term,
						  Attr:term,
						  Empty:bool),[]),
	mutable_read(Ctx,N),
	( Empty == true ->
	  N=M
	;   
	  M is N+1,
	  mutable(Ctx,M),
	  private!set_indent(M)
	)
	.

xml!end_elt(class(Stream,_,_),Name,Ctx) :-
	mutable_read(Ctx,N),
	M is N-1,
	mutable(Ctx,M),
	private!set_indent(M),
	'$interface'('XML_my_close_element_print'(Stream:output,Name:term),[]).

xml!family(H::class(_,Class,_),family(Id,Name,Tree),Ctx) :-
	( option('-family') ->
	  xml!wrapper(H,
		      Ctx,
		      family,[name:Name],
		      xml!tree(H, tree(Id,Name,Tree), Ctx)
		     )
	;
	  xml!tree(H, tree(Id,Name,Tree), Ctx )
	)
	.

xml!tree(H::class(_,Class,Bindings),tree(Id,Name,Tree),Ctx) :-
	name2class(Class,XClass),
	xml!wrapper(H,
		    Ctx,
		    tree,[name:Name],
		    ( try(( '$answers'(description_closure(Class,Desc)),
			    try(( option('-ht'),
				  fs!value(Desc,ht:none,ht:HT,Bindings),
				  HT \== none ->
				  %% emit hypertag when found under feature ht
				  %% of class description
				  xml!fstruct(H,fstruct(ht,HT),Ctx))),
			    xml!fstruct(H,fstruct(description,Desc),Ctx)
			  )),
		      reset_binding_display(Bindings),
		      every(( '$answers'(reducer_closure(Class,FS)),
			      xml!fstruct(H,fstruct(reducer,FS),Ctx))),
		      xml!xnode(H, xnode(Tree,[]),Ctx)
		    ))
	.

xml!fstruct(H,fstruct(Name,FS),Ctx) :-
	xml!wrapper(H,
		    Ctx,
		    Name,[],
%		    xml!fs(H,FS,Ctx)
		    xml!val(H,FS,Ctx)
		   )
	.


xml!xnode(H,xnode(XNode,Father),Ctx) :-
	( XNode = ([] ## N1) ->
	  xml!interleave(H,interleave(N1,Father),Ctx)
	;
	  XNode = (N1 ## []) ->
	  xml!interleave(H,interleave(N1,Father),Ctx)
	;
	 XNode = (_ ## _) ->
	  xml!wrapper(H,
		      Ctx,
		      interleave,[],
		      xml!interleave(H,interleave(XNode,Father),Ctx))
	;   
	  xml!nodeopt(H,nodeopt(XNode,Father),Ctx)
	)
	.

xml!interleave(H,interleave(XNode,Father),Ctx) :-
	( XNode = (XN1 ## XN2) ->
	  xml!sequence(H,sequence(XN1,Father),Ctx),
	  xml!interleave(H,interleave(XN2,Father),Ctx)
	;
	  xml!sequence(H,sequence(XNode,Father),Ctx)
	)
	.

xml!sequence(H,sequence(L,Father),Ctx) :-
	( L = [Node] ->
	  xml!xnode(H,xnode(Node,Father),Ctx)
	; L = [] ->
	  format(2,'*** empty sequence\n',[]),
	  xml!elt(H,node,[type:lex,adj:no,lex:''],Ctx)
	;   xml!wrapper(H,
			Ctx,
			sequence, [],
			every((domain(Node,L),
			       xml!xnode(H,xnode(Node,Father),Ctx))))
	)
	.

xml!nodeopt(H::class(_,Class,Bindings),nodeopt(N::[Node|Child],Father),Ctx) :-
	'$answers'(nodefeature_closure(Class,Node,FS)),
	( fs!value(FS,optional:no,optional:yes,Bindings) ->
	  xml!wrapper(H,
		      Ctx,
		      optional,[n:0],
		      xml!node(H,node(N,Father),Ctx)) 
	;   fs!value(FS,star:no,star: Star,Bindings), Star \== no ->
	  xml!wrapper(H,
		      Ctx,
		      optional,[n: Star],
		      xml!node(H,node(N,Father),Ctx))
	;  xml!node(H,node(N,Father),Ctx)
	)
	.

:-extensional default_adj/2.

default_adj(std,yes).
default_adj(anchor,yes).
default_adj(coanchor,yes).
default_adj(subst,no).
default_adj(foot,no).
default_adj(lex,no).
default_adj(sequence,no).
default_adj(alternative,no).
default_adj(skip,no).
	    
xml!node(H::class(_,Class,Bindings),node(N,Father),Ctx) :-
	N = [Node|Child],
	'$answers'(nodefeature_closure(Class,Node,FS)),
	fs!value(FS,type:std,type:Type,Bindings),
	default_adj(Type,Default_Adj),
	fs!value(FS,adj:Default_Adj,adj:Adj,Bindings),
	( Adj == no ->
	  fs!flat_values(FS,[id:Node,cat,type:std,lex,token,secondary,secondary_label,secondary_ref],Attrs,Bindings)
	;
	  fs!flat_values(FS,[id:Node,cat,type:std,lex,token,secondary,secondary_label,secondary_ref,adjleft,adjright,adjwrap],Attrs,Bindings)
	),
	fs!value(FS,top:[],top:Top,Bindings),
	fs!value(FS,bot:[],bot:Bot,Bindings),
	( (Father=[] xor '$answers'( dominates_closure(Class,Father,Node,1) )) ->
	    Attrs2=Attrs
	;   
	    Attrs2=[dom: +|Attrs]
	),
	Attrs3=[adj:Adj|Attrs2],
	(   Child = [],Top=[],Bot=[],
	    \+ domain(Type,[sequence,alternative]),
	    \+ ('$answers'(nodeguard_closure(Class,Node,_,_G)), _G \== true) ->
	    xml!elt(H,node,Attrs3,Ctx)
	;   Child = [],Top=[],Bot=[],
	    \+ domain(Type,[sequence,alternative]),
	    '$answers'(nodeguard_closure(Class,Node,-,[])),
	    \+ '$answers'(nodeguard_closure(Class,Node,+,[])),
	    \+ ('$answers'(nodeguard_closure(Class,Node,+,_G)), _G \== true) ->
	    %% the negative guard is failed and the positive guard is not failed => the node has to be present
	    %% all positive guards are trivially true => we can remove the guards
	    xml!elt(H,node,Attrs3,Ctx)
	;   '$answers'(nodeguard_closure(Class,Node,+,[])) ->
	    %% the node can't exist because of a failed guard
	    %% we replace it by a lex node with empty lex
	    ( '$answers'(nodeguard_closure(Class,Node,-,_G)),
	      _G \== true ->
	      xml!wrapper(H,
			  Ctx,
			  node,[type:lex,lex:''],
			  every(( domain(Rel,guardrel[]),
				  xml!guards(H,guards(Node,Rel),Ctx) ))
			 )
	    ;	
	      true
	    )
	;   domain(Type,[sequence,alternative]) ->
	    (	Child = [] ->
		name2class(Class,XClass),
		format(2,'*** empty ~w in class ~w => replacing by an empty lexical node\n',[Type,XClass]),
		xml!elt(H,node,[type:lex,adj:no,lex:''],Ctx)
	    ; Child = [N1],
	      \+ ('$answers'( nodeguard_closure(Class,Node,guardrel[+,++],_G) ), _G \== true),
	      '$answers'( nodeguard_closure(Class,Node,-,[]) ) ->
		xml!node(H,node(N1,Father),Ctx)
	    ;	xml!wrapper(H,
			    Ctx,
			    Type,Attrs2,
			    ( every(( domain(Rel,guardrel[]),
				      xml!guards(H,guards(Node,Rel),Ctx))),
			      every(( domain(N1,Child),
				      xml!xnode(H,xnode(N1,Node),Ctx) ))
			    )
			   )
	    )
	; '$answers'(nodeguard_closure(Class,Node,-,[])),
	    \+ '$answers'(nodeguard_closure(Class,Node,+,[])),
	    \+ ('$answers'(nodeguard_closure(Class,Node,+,_G)), _G \== true) ->
	    xml!wrapper(H,
			Ctx,
			node,Attrs3,
			(   xml!narg(H,narg(Top,top),Ctx),
			    xml!narg(H,narg(Bot,bot),Ctx),
			    every(( domain(N1,Child),
				    xml!xnode(H,xnode(N1,Node),Ctx )))
			)
		       )
	;   
	    xml!wrapper(H,
			Ctx,
			node,Attrs3,
			(   xml!narg(H,narg(Top,top),Ctx),
			    xml!narg(H,narg(Bot,bot),Ctx),
			    every(( domain(Rel,guardrel[]),
				    xml!guards(H,guards(Node,Rel),Ctx) )),
			    every(( domain(N1,Child),
				    xml!xnode(H,xnode(N1,Node),Ctx )))
			)
		       )
	)
 	.


xml!guards(H::class(_,Class,_),guards(Node,Rel),Ctx) :-
	( '$answers'(nodeguard_closure(Class,Node,Rel,[])) ->
	  xml!wrapper(H,
		      Ctx,
		      guards,[rel:Rel],
		      xml!elt(H,or,[],Ctx)
		     )
	; '$answers'(nodeguard_closure(Class,Node,Rel,_Guard)), _Guard \== true
	->
	  xml!wrapper(H,
		      Ctx,
		      guards,[rel: Rel],
		      every(( '$answers'(nodeguard_closure(Class,Node,Rel,Guard)),
			      Guard \== true,
			      xml!guard(H,Guard,Ctx))))
	;   
	  true
	)
	.

xml!guard(H,Guard,Ctx) :-
	( Guard = [guard(Type,FS1,FS2)] ; Guard = guard(Type,FS1,FS2) ->
	  ( Type = none -> Attr = [] ; Attr = [type:Type] ),
	  xml!wrapper(H,Ctx,
		      guard,Attr,
		      (   xml!val(H,FS1,Ctx),
			  xml!val(H,FS2,Ctx)
		      )
		     )
	; Guard = [xguard(Type,FS1,FS2)] ; Guard = xguard(Type,FS1,FS2) ->
	  ( Type = none -> Attr = [] ; Attr = [type:Type] ),
	  xml!wrapper(H,Ctx,
		      xguard,Attr,
		      (   xml!val(H,FS1,Ctx),
			  xml!val(H,FS2,Ctx)
		      )
		     )
	; Guard = [and(Guards)] ; Guard = and(Guards) ->
	  xml!wrapper(H,Ctx,
		      and,[],
		      every(( domain(GuardX,Guards),
			      xml!guard(H,GuardX,Ctx)
			    )))
	;   
	  xml!wrapper(H,Ctx,
		      or,[],
		      every(( domain(GuardX,Guard),
			      xml!guard(H,GuardX,Ctx))))
	)
	.

xml!narg(H,narg(FS,Type),Ctx) :-
	(   FS=[]
	xor xml!wrapper(H,
			Ctx,
			narg,[type:Type],
			xml!val(H,FS,Ctx))
	)
	.

%%:-light_tabular binding_display/2.
%%:-mode(binding_display/2,+(+,-)).

:-std_prolog reset_binding_display/1.

reset_binding_display(Bindings) :-
	erase( binding_display(Bindings,_) )
	.

:-std_prolog binding_display/2.

binding_display(Bindings,Display) :-
	(   recorded( binding_display(Bindings,Display) )
	xor mutable(Display,[]),
	    record_without_doublon( binding_display(Bindings,Display) )
	)
	.

:-std_prolog displayed_var/2.

displayed_var(V,Bindings) :-
	binding_display(Bindings,Display),
	mutable_read(Display,L),
	domain(V,L)
	.

:-std_prolog set_displayed_var/2.

set_displayed_var(V,Bindings) :-
	binding_display(Bindings,Display),
	mutable_read(Display,L),
	mutable(Display,[V|L])
	.

xml!fs(H,TFS,Ctx) :-
	( TFS = Type @ FS ->
	  Attrs = [type: Type]
	; TFS = FS,
	  Attrs = []
	),
	xml!wrapper(H,
		    Ctx,
		    fs,Attrs,
		    every(( domain(Name:V,FS),
			    ( var(V) ->
			      xml!elt(H,f,[name:Name],Ctx)
			    ;
			      xml!wrapper(H,Ctx,
					  f,[name:Name],
					  xml!val(H,V,Ctx))
			    )
			  ))
		   )
	.

xml!val(H::class(_,Class,Bindings),V,Ctx) :-
%%	format('Trying val ~w\n',[V]),
	( V = var([]) ->
	  xml!elt(H,any,[],Ctx)
	;  V = var(Var) ->
	  fs!deref_vars(V,VV,Bindings),
	  VV=var(VName),
	  fs!deref(VV,Value,Bindings),
	  ( VV=Value ->
	    xml!elt(H,var,[name:VName],Ctx)
	  ;	VV = var([]) ->
	    xml!elt(H,any,[],Ctx)
	  ; displayed_var(VName,Bindings) ->
	    xml!elt(H,var,[name:VName],Ctx)
	  ; fs!atom(Value) -> %% no need to keep var binding for ground atomic values
	    xml!val(H,Value,Ctx)
	  ; set_displayed_var(VName,Bindings),
	    xml!wrapper(H,Ctx,
			var,[name:VName],
			xml!val(H,Value,Ctx)
		       )
	  )
	; V=(+) ->
%%	  format('Trying2 val ~w\n',[V]),
	  xml!elt(H,plus,[],Ctx)
	;  V=(-) ->
	  xml!elt(H,minus,[],Ctx)
	;  fs!atom(V) ->
	  xml!elt(H,symbol,[value:V],Ctx)
	;  V = disj(L) ->
	  xml!wrapper(H,
		      Ctx,
		      vAlt,[],
		      every((domain(VV,L), xml!val(H,VV,Ctx)))
		     )
	; V = notdisj([VV]) ->
	  xml!wrapper(H,
		      Ctx,
		      not,[],
		      xml!val(H,VV,Ctx))
	; V = notdisj(L) ->
	  xml!wrapper(H,
		      Ctx,
		      not,[],
		      xml!wrapper(H,Ctx,
				  vAlt,[],
				  every((domain(VV,L),
					 xml!val(H,VV,Ctx)))))
	; xml!fs(H,V,Ctx)
	)
	.

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :-
	mutable(M,0,true),
	every((   domain(A,AA),
		  mutable_inc(M,V),
		  (   V == 0 xor write(Stream,Sep) ),
		  format(Stream,Format,[A]) ))
	.

:-xcompiler((try(G) :- (G xor true))).

:-xcompiler((xml!wrapper(Handler,Ctx,Name,Attrs,G) :-
	     xml!start_elt(Handler,Name,Attrs,Ctx),
             G,
             xml!end_elt(Handler,Name,Ctx)
            )).


:-xcompiler((
             private!newline_start(Start) :- '$interface'( 'Newline_Start_1'(Start:term),[])
            ))
.

:-light_tabular private!indenter/2.

private!indenter(0,'').
private!indenter(N,Indent) :-
	N > 0,
	M is N-1,
	private!indenter(M,Indent2),
	atom_concat(Indent2,'  ',Indent)
	.

:-std_prolog private!set_indent/1.

private!set_indent(N) :-
	private!indenter(N,Indent),
	private!newline_start(Indent)
	.
