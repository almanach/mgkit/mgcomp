<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="text"
        indent="no"
        encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="class">
  <xsl:variable name="class">'<xsl:value-of select="@name"/>'</xsl:variable>
class(<xsl:value-of select="$class"/>).
<xsl:apply-templates>
  <xsl:with-param name="class" select="$class"/>
</xsl:apply-templates>
</xsl:template>

<xsl:template match="supers/s">
  <xsl:param name="class"/>
super(<xsl:value-of select="$class"/>,'<xsl:value-of select="@name"/>').
</xsl:template>

<xsl:template match="pf">
  <xsl:param name="class"/>
<xsl:value-of select="name(..)"/>(<xsl:value-of select="$class"/>,'<xsl:value-of select="@name"/>').
</xsl:template>


<xsl:template match="description/fs">
  <xsl:param name="class"/>
description(<xsl:value-of select="$class"/>,
            [<xsl:apply-templates/>]
).
</xsl:template>

<xsl:template match="qn">
  <xsl:param name="class"/>
  <xsl:variable name="node">'<xsl:value-of select="@name"/>'</xsl:variable>
node(<xsl:value-of select="$class"/>,<xsl:value-of select="$node"/>).
  <xsl:apply-templates>
    <xsl:with-param name="class" select="$class"/>
    <xsl:with-param name="node" select="$node"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="qn/fs">
  <xsl:param name="class"/>
  <xsl:param name="node"/>
nodefeature(<xsl:value-of select="$class"/>,
            <xsl:value-of select="$node"/>,
            [<xsl:apply-templates/>]
).
</xsl:template>

<xsl:template match="father|precedes|equals|dominates">
  <xsl:param name="class"/>
  <xsl:value-of select="name(.)"/>(<xsl:value-of select="$class"/>,'<xsl:value-of select="preceding-sibling::arg1[1]/@name"/>','<xsl:value-of select="following-sibling::arg2[1]/@name"/>').
</xsl:template>

<xsl:template match="atom[@val]">
  <xsl:choose>
    <xsl:when test="@val=''"><xsl:text>_</xsl:text>
    </xsl:when>
    <xsl:when test="number(@val)"><xsl:value-of select="@val"/>
    </xsl:when>
    <xsl:otherwise>'<xsl:value-of select="@val"/>'</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="val/fs">[<xsl:apply-templates/>]
</xsl:template>

<xsl:template match="f">'<xsl:value-of select="@name"/>' : <xsl:apply-templates/>
<xsl:if test="not(position()=last())">,</xsl:if>
</xsl:template>

</xsl:stylesheet>
