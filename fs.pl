/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008, 2009, 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  fs.pl -- Feature Structure light implementation
 *
 * ----------------------------------------------------------------
 * Description
 *
 *  Light implementation of open feature structures
 *
 *    Term := <atom>                   %% atomic value (not eq to [])
 *         |  [ (<atom> : <term>)* ]   %% Feature: value pairs
 *         |  var(<atom>)              %% named variable
 *         |  var([])                  %% anonymous variable
 *         |  <term> ^ <term>          %% term unification
 *         |  disj([<atom> *])         %% Atomic disjunction
 *         |  notdisj([<atom> *])      %% Atomic not disjunction (unif with element not in disj)
 *
 *    Term unification is usually used for binding: var(x) ^ <term>
 *
 *
 * ----------------------------------------------------------------
 */

 :-import{ file=>'format.pl', preds => [format/2] }.

%% FS are normalized by sorting lists by feature names

:-std_prolog fs!safe_normalize/3.

fs!safe_normalize(FS,XFS,Bindings) :-
	( var(FS) -> XFS=FS
	;   fs!normalize(FS,XFS,Bindings)
	)
	.

:-rec_prolog fs!normalize/3.

fs!normalize(U::var(_),U,_).
fs!normalize(U,U,_) :- atomic(U), U \== [].
fs!normalize([],[],_).
fs!normalize([T1:V1|FS2],XFS,Bindings) :-
	fs!normalize(FS2,XFS2,Bindings),
	fs!normalize(V1,XV1,Bindings),
	fs!normalize(T1:XV1,XFS2,XFS,Bindings)
	.
fs!normalize(FS1^FS2,FS3,Bindings) :-
	fs!normalize(FS1,XFS1,Bindings),
	fs!normalize(FS2,XFS2,Bindings),
	fs!unify(XFS1,XFS2,FS3,Bindings)
	.
fs!normalize(disj(L),disj(LL),Bindings) :-
	fs!sort_list(L,LL)
	.

fs!normalize(notdisj(L),notdisj(LL),Bindings) :-
	fs!sort_list(L,LL)
	.

fs!normalize(Type @ FS, Type @ XFS, Bindings ) :-
%%	format('Normalize ~w\n',[Type @ FS]),
	fs!normalize(FS,XFS,Bindings),
%%	format('--> normalize ~w\n',[Type @ XFS]),
	true
	.

:-light_tabular fs!sort_list/2.
:-mode(fs!sort_list/2,+(+,-)).

fs!sort_list(L,XL) :- fs!xsort_list(L,XL).

:-rec_prolog fs!xsort_list/2.

fs!xsort_list([],[]).
fs!xsort_list([A|L],LL) :-
        fs!xsort_list(L,L1),
        fs!sort_list_aux(A,L1,LL).

:-rec_prolog fs!sort_list_aux/3.

fs!sort_list_aux(A,[],[A]).
fs!sort_list_aux(A,[B|L],LL) :-
        ( A == B ->
	    LL = [B|L]
	;   A @< B ->
            LL=[A,B|L]
        ;   
            LL=[B|L1],
            fs!sort_list_aux(A,L,L1)
        )
        .

:-rec_prolog fs!normalize/4.

fs!normalize(X,[],[X],_).
fs!normalize(T1:V1,FS::[T2:V2|FS2],XFS,Bindings) :-
	( T1 @< T2 ->
	    XFS=[T1:V1|FS]
	;   
	    XFS=[T2:V2|XFS2],
	    fs!normalize(T1:V1,FS2,XFS2,Bindings)
	)
	.

:-std_prolog fs!atom/1.

fs!atom(X) :- X \== [], atomic(X).

%% We assume that unification is performed between normalized FS
%% it means that ^ operators has been removed
%% and in particular, var(x) ^ T replaced by var(x)

:-rec_prolog fs!unify/4.

fs!unify(var([]),A,A,_).
fs!unify(A,var([]),A,_) :- \+ A = var([]).
fs!unify(A,A,A,_) :- A \== [], atomic(A).
fs!unify([],FS,FS,_).
fs!unify(FS::[_|_],[],FS,_).
fs!unify(FS:: (_ @ _),[],FS,_).
fs!unify(FS1::[T1:V1|XFS1],FS2::[T2:V2|XFS2],FS,Bindings) :-
%%	format('** Unify ~w ~w\n',[FS1,FS2]),
	( FS1=FS2-> FS=FS1
	;   T1 == T2 ->
	    ( V1=V2 ->
		V3=V1
	    ;	
		fs!unify(V1,V2,V3,Bindings)
	    ),
	    FS=[T1:V3|XFS],
	    fs!unify(XFS1,XFS2,XFS,Bindings)
	;   T1 @< T2 ->
	    FS=[T1:V1|XFS],
	    fs!unify(XFS1,FS2,XFS,Bindings)
	;   
	    FS=[T2:V2|XFS],
	    fs!unify(XFS2,FS1,XFS,Bindings)
	),
%%	format('--> ~w ~w = ~w\n',[FS1,FS2,FS]),
	true
	.

fs!unify(T1 @ FS1, TFS2, T1 @ FS3,Bindings) :-
	\+ TFS2 = var(_),
	\+ FS2 = [],
%%	format('** Unify ~w ~w\n',[T1 @ FS1,TFS2]),
	( TFS2 = T2 @ FS2 ->
	  T1 = T2,
	  fs!unify(FS1,FS2,FS3,Bindings)
	;
	  fs!unify(FS1,TFS2,FS3,Bindings)
	),
%%	format('--> ~w ~w = ~w\n',[T1 @ FS1,TFS2,T1 @ FS3]),
	true
	.

fs!unify(FS1, T2 @ FS2, T2 @ FS3,Bindings) :-
	\+ FS1 = var(_),
	\+ FS1 = [],
%%	format('** Unify ~w ~w\n',[FS1, T2 @ TFS2]),
	\+ FS1 = _ @ _,
	fs!unify(FS1,FS2,FS3,Bindings),
%%	format('--> ~w ~w = ~w\n',[FS1,T2 @ FS2,T2 @ FS3]),
	true
	.
	  
fs!unify(notdisj(L),A,A,Bindings) :-
	\+ A=var(_),
	\+ A=notdisj(_),
	\+ A=disj(_),
	\+ domain(A,L)
	.
fs!unify(A,notdisj(L),A,Bindings) :-
	\+ A=var(_),
	\+ A=notdisj(_),
	\+ A=disj(_),
	\+ domain(A,L)
	.
fs!unify(notdisj(L1),notdisj(L2),notdisj(L3),Bindings) :-
%%	format('NOTxNOT UNIFY ~w & ~w\n',[L1,L2]),
	fs!union_list(L1,L2,L3),
%%	format('\t->~w\n',[L3]),
	true
	.
fs!unify(notdisj(L1),disj(L2),X,Bindings) :-
%%	format('NOTxDISJ UNIFY ~w & ~w\n',[L1,L2]),
	fs!diff_list(L2,L1,L::[_|_]),
%%	format('\t->~w\n',[L]),
	(   L=[X] xor X=disj(L))
	.
fs!unify(disj(L1),notdisj(L2),X,Bindings) :-
%%	format('DISJxNOT UNIFY ~w & ~w\n',[L1,L2]),
	fs!diff_list(L1,L2,L::[_|_]),
%%	format('\t->~w\n',[L]),
	(   L=[X] xor X=disj(L))
	.

%%:-rec_prolog fs!union_list/3.
:-light_tabular fs!union_list/3.
:-mode(fs!union_list/3,+(+,+,-)).

fs!union_list([],L,L).
fs!union_list(L::[_|_],[],L).
fs!union_list([A|LA],[B|LB],L) :-
	(   A == B ->
	    fs!union_list(LA,LB,LX),
	    L=[A|LX]
	;   A @< B ->
	    fs!union_list(LA,[B|LB],LX),
	    L=[A|LX]
	;
	    fs!union_list(LB,[A|LA],LX),
	    L=[B|LX]
	)
	.


%%:-rec_prolog fs!diff_list/3.
:-light_tabular fs!diff_list/3.
:-mode(fs!diff_list/3,+(+,+,-)).

%% fs!diff_list(L1,L2,L3)
%% L3 = L1\L2

fs!diff_list([],_,[]).
fs!diff_list([A|LA],LB,L) :-
	fs!diff_list(LA,LB,LL),
	( domain(A,LB) ->
	    L=LL
	;   
	    L=[A|LL]
	)
	.

fs!unify(disj(L),A,A,Bindings) :- domain(A,L).
fs!unify(A,disj(L),A,Bindings) :- domain(A,L).
fs!unify(disj(L1),disj(L2),X,Bindings) :-
%%	format('DISJ UNIFY ~w & ~w\n',[L1,L2]),
%%	fs!sort_list(L1,XL1),
%%	fs!sort_list(L2,XL2),
%%	format('NORMALIZED DISJ UNIFY ~w & ~w\n',[XL1,XL2]),
	fs!inter_list(L1,L2,L::[_|_]),
%%	format('\t->~w\n',[L]),
	(   L=[X] xor X=disj(L))
	.

%%:-rec_prolog fs!inter_list/3.
:-light_tabular fs!inter_list/3.
:-mode(fs!inter_list/3,+(+,+,-)).

fs!inter_list([],_,[]).
fs!inter_list([_|_],[],[]).
fs!inter_list([A|LA],[B|LB],L) :-
%%	format('INTER LIST ~w ~w\n',[[A|LA],[B|LB]]),
	(   A == B ->
	    fs!inter_list(LA,LB,LX),
	    L=[A|LX]
	;   A @< B ->
	    fs!inter_list(LA,[B|LB],L)
	;
	    fs!inter_list(LB,[A|LA],L)
	)
	.

%% No need for FS1 ^ FS2 cases because of normalization

fs!unify(FS1::var(X),FS2,FS3,Bindings) :-
	X \== [],
	( \+ FS2 = var(_) ->
	    fs!check_or_bind(FS1,FS2,Bindings),
	    ( fs!atom(FS2) -> FS3=FS2 ; FS3 = FS1)
	;   FS1 == FS2 -> FS3 = FS1
	;   FS1 @< FS2 ->
	    fs!check_or_bind(FS1,FS2,Bindings),
	    FS3=FS1
	;   
	    fs!check_or_bind(FS2,FS1,Bindings),
	    FS3=FS2
	)
	.

fs!unify(FS1,FS2::var(X),FS3,Bindings) :-
	X \== [],
	\+ FS1 = var(_), 
	fs!check_or_bind(FS2,FS1,Bindings),
	( fs!atom(FS1) -> FS3=FS1 ; FS3=FS2 )
	.

:-std_prolog fs!deref/3.

fs!deref(FS1,FS2,Bindings) :-
	( FS1 = var(X), X \== [] ->
	    mutable_read(Bindings,L),
	    (	domain(FS1:FS3,L) ->
		fs!deref(FS3,FS2,Bindings),
		( FS3 \== FS2, (fs!atom(FS2) ; FS2 = var(_)) -> 
		    %% rebind for more direct dereferencing
		    %% but only when FS2 is an atom or a variable
		    fs!delete_binding(FS1,L,LL),
		    mutable(Bindings,[FS1:FS2|LL])
		;   
		    true
		)
	    ;	
		FS2 = FS1
	    )
	;   
	    FS2=FS1
	)
	.

:-std_prolog fs!deref_vars/3.

fs!deref_vars(FS1,FS2,Bindings) :-
	( FS1 = var(X), X \== [] ->
	    mutable_read(Bindings,L),
	    ( domain(FS1:FS3::var(_),L) ->
		fs!deref_vars(FS3,FS2,Bindings),
		( FS2 == FS3 -> true
		;   fs!delete_binding(FS1,L,LL), %% rebind for more direct dereferencing
		    mutable(Bindings,[FS1:FS2|LL])
		)
	    ;	
		FS2 = FS1
	    )
	;   
	    FS2=FS1
	)
	.


:-std_prolog fs!check_or_bind/3.

%% A binding list is of the form (Var:FS)*
%% It would be more efficient to order the list by Vars

fs!check_or_bind(V::var(X),FS1,Bindings) :-
	mutable_read(Bindings,L),
%%	format('Check/Bind ~w -> ~w with ~Z\n',[V,FS1,Bindings]),
	( domain(V:FS2,L) ->
	    ( FS2 == FS1 ->
		true
	    ;	fs!atom(FS2) ->
		fs!unify(FS2,FS1,FS3,Bindings)
	    ;	FS2 = var(_) ->
		fs!check_or_bind(FS2,FS1,Bindings)
	    ;	
		fs!delete_binding(V,L,LL),
		mutable(Bindings,LL),
		fs!unify(FS2,FS1,FS3,Bindings),
%%		format('Unify ret ~w & ~w => ~w\n',[FS2,FS1,FS3]),
		mutable_read(Bindings,L2),
		( domain(V:FSX,L2) ->
		    %% Cycle !
		    (	FSX == FS3
		    xor FSX == FS2
		    xor (FSX == FS1, FS1 = var(_))
		    xor fs!check_or_bind(V,FS3,Bindings)
%%		    fs!unify(V,FS3,_,Bindings)
		    )
		;   
		    mutable(Bindings,[V:FS3|L2])
		)
	    )
        ;   FS1 = var(_), domain(FS1:_,L) ->
%%          format('Flip/Flap ~w ~w\n',[V,FS1]),
            fs!check_or_bind(FS1,V,Bindings)
        ;   FS1 = var(_), FS1 @< V ->
            mutable(Bindings,[FS1:V|L])
	;   
	    mutable(Bindings,[V:FS1|L])
	),
%%	mutable_read(Bindings,L3),
%%	format('==> ~w\n',[L3]),
	true
	.

:-std_prolog fs!delete_binding/3.

%% We assume there exists a binding for V in L
fs!delete_binding(V,[V1:FS1|L],LL) :-
	( V1 == V ->
	    LL = L
	;   
	    fs!delete_binding(V,L,L1),
	    LL = [V1:FS1|L1]
	)
	.

%% Subsumption

%% To be updated to handle variables variables !

:-rec_prolog fs!subsume/3.

fs!subsume([],_,_).
fs!subsume(FS1::[T1:V1|XFS1],FS2::[T2:V2|XFS2],Bindings) :-
	(   FS1 == FS2
	xor T1 == T2,
	    (	var(V1) xor V1==V2 xor fs!subsume(V1,V2,Bindings)),
	    fs!subsume(XFS1,XFS2,Bindings)
	xor T1 @> T2,
	    fs!subsume(FS1,XFS2,Bindings)
	)
	.
	    
:-std_prolog fs!value/4.

fs!value(FS,X,T:VV,Bindings) :-
	fs!yderef(FS,FS2,Bindings),
	( X=T:U ->
	    (	domain(T:V,FS2) xor V=U)
	;   X=T,
	    domain(T:V,FS2)
	),
	fs!yderef(V,VV,Bindings),
%%	format('yderef ~w -> ~w\n',[V,VV]),
	true
	.

:-rec_prolog fs!values/4.

fs!values(FS,[],[],_).
fs!values(FS1,[X|LT],FS2,Bindings) :-
	fs!values(FS1,LT,FS3,Bindings),
	( fs!value(FS1,X,Y,Bindings) ->
	  FS2 = [Y|FS3]
	;   
	  FS2=FS3
	)
	.

:-rec_prolog fs!flat_values/4.

fs!flat_values(FS,[],[],_).
fs!flat_values(FS1,[X|LT],FS2,Bindings) :-
	fs!flat_values(FS1,LT,FS3,Bindings),
	( fs!flat_value(FS1,X,Y,Bindings) ->
	  FS2 = [Y|FS3]
	;   
	  FS2=FS3
	)
	.

:-std_prolog fs!flat_value/4.

fs!flat_value(FS,X,T:VVV,Bindings) :-
	fs!yderef(FS,FS2,Bindings),
	( X=T:U ->
	    (	domain(T:V,FS2) xor V=U)
	;   X=T,
	    domain(T:V,FS2)
	),
	fs!xderef(V,_VV,Bindings),
	(_VV = _Var : VV1 xor _VV = VV1, _Var = []),
	(VV1 = disj(L) ->
	 fs!flat_value_aux(L,LL,Bindings),
	 VV=disj(LL)
	;
	 VV=VV1
	),
	%% use special notation '^' VarName '^'
	%% to indicate that a flat value is bound to a non-anonymous variable
	%% This is not very elegant !
	( VV = var(VV_Name) ->
	    VVV = disj(['^',VV_Name,'^'])
	;   ( _Var = [];_Var = var([])) ->
	    VVV = VV
	;   VV = disj(LL) ->
	    _Var=var(_VarName),
	    VVV = disj(['^',_VarName,'^'|LL])
	;   VVV = VV
	),
%%	format('flat value ~w -> ~w\n',[V,VV]),
	true
	.

:-std_prolog fs!flat_value_aux/3.

fs!flat_value_aux(L,LL,Bindings) :-
	( L = [] ->
	  LL=[]
	; L=[A|R] ->
	  fs!flat_value_aux(R,RR,Bindings),
	  fs!yderef(A,AA,Bindings),
	  LL=[AA|RR]
	)
	.

:-std_prolog fs!shift/3.

%:-light_tabular fs!shift/3.
%:-mode(fs!shift,+(+,+,-)).

fs!shift(FS,NS,XFS) :-
	( NS==[] -> XFS=FS ; fs!shift_aux(FS,NS,XFS))
	.

%% Defined in tools.pl
:-light_tabular deep_module_shift/3.
:-mode(deep_module_shift/3,+(+,+,-)).
:-std_prolog deep_module_shift_aux/3.

:-rec_prolog fs!shift_aux/3.

fs!shift_aux(var(X),NS,var(Y)) :-
	( X == [] ->
	    Y=[]
	;   X = gensym(XX) ->
	    deep_module_shift(XX,NS,YY),
	    Y = gensym(YY)
	;
	    deep_module_shift(X,NS,Y)
	).
fs!shift_aux(U,_,U) :- atomic(U), U \== [].
fs!shift_aux([],_,[]).
fs!shift_aux([T1:V1|FS2],NS,[T1:XV1|XFS2]) :-
	fs!shift_aux(V1,NS,XV1),
	fs!shift_aux(FS2,NS,XFS2)
	.
fs!shift_aux(FS1^FS2,NS,XFS1^XFS2) :-
	fs!shift_aux(FS1,NS,XFS1),
	fs!shift_aux(FS2,NS,XFS2)
	.
fs!shift_aux(U::disj(_),_,U).
fs!shift_aux(U::notdisj(_),_,U).

:-std_prolog fs!check_unify/4.

fs!check_unify(FS1,FS2,Bindings,BindP) :-
	fs!check_unify(FS1,FS2,Bindings,BindP,_).

:-std_prolog fs!check_unify/5.

fs!check_unify(FS1,FS2,Bindings,BindP,NewL) :-
	mutable_read(Bindings,L),
	(   fs!normalize(FS1,XFS1,Bindings),
	    fs!normalize(FS2,XFS2,Bindings),
	    fs!unify(XFS1,XFS2,_,Bindings) ->
	    %%	    format('check unify ~w ~w\n',[FS1,FS2]),
	    mutable_read(Bindings,NewL),
	    ( L=NewL ->
	      %%		format('nobind\n',[]),
	      BindP = nobind
	    ;
	      %%		format('bind\n',[]),
	      BindP = bind
	    ),
	    mutable(Bindings,L)
	;   
	    mutable(Bindings,L),
	    fail
	)
	.

:-rec_prolog fs!closure/3.

%% Collect all variables reachable from some feature structure

fs!closure(FS::[_:_|_],Bindings,C) :-
	domain(_:FS1,FS),
	fs!closure(FS1,Bindings,C)
	.
fs!closure(FS1^FS2,Bindings,C) :-
	domain(FS,[FS1,FS2]),
	fs!closure(FS,Bindings,C)
	.
/*
fs!closure(disj(L),Bindings,C) :-
	domain(FS,L),
	fs!closure(FS,Bindings,C).
fs!closure(notdisj(L),Bindings,C) :-
	domain(FS,L),
	fs!closure(FS,Bindings,C).
*/
fs!closure(Var::var(X),Bindings,C) :-
	X \== [],
	mutable_read(C,L),
	(   domain(Var,L)
	%% already seen: nothing to do
	%% otherwise add it and dereference
	%% but beware that a variable bound to a compound term should be reachable
	%% ie. X -> Y -> Z -> T
	%% => X,Z + reachable(T) are reachable if T is a compound term
	%% => X + reachable(T) otherwise (T a constante or a free variable)
	xor LL=[Var|L],
	    mutable(C,LL),
	    fs!xderef(Var,FS,Bindings),
	    ( Var == FS ->
		%% Var is a free variable
		true
	    ;	FS = Var1 : FS1 ->
		%% Var is possibly bound to Var1 itself bound to some compound term
		( Var1 == Var ->
		    fs!closure(FS1,Bindings,C)
		;   
		    %% Var1 should also be reachable
		    fs!closure(Var1,Bindings,C)
		)
	    ;
		%% Bound to a free variable or constant
		fs!closure(FS,Bindings,C)
	    )
	)
	.

:-std_prolog fs!yderef/3.

fs!yderef(FS1,FS2,Bindings) :-
	fs!xderef(FS1,FS3,Bindings),
	(FS3 = FS2 : _ xor FS3 = FS2)
	.

:-std_prolog fs!xderef/3.

fs!xderef(FS1,FS2,Bindings) :-
	( FS1 = var(X), X \== [] ->
	    mutable_read(Bindings,L),
	    (	domain(FS1:FS3,L) ->
		( FS3 = var(_) ->
		    fs!xderef(FS3,FS2,Bindings),
		    ( FS2 = FSX : _ ->
			(   FSX == FS3
			xor fs!delete_binding(FS1,L,LL),
			    mutable(Bindings,[FS1:FSX|LL])
			)
		    ;	FS3 \== FS2, (fs!atom(FS2) ; FS2 = var(_)) -> 
			%% rebind for more direct dereferencing
			%% but only when FS3 is an atom or a variable
			fs!delete_binding(FS1,L,LL),
			mutable(Bindings,[FS1:FS2|LL])
		    ;	
			true
		    )
		;   fs!atom(FS3)->
		    FS2 = FS3
		;   
		    FS2 = FS1:FS3
		)
	    ;	
		FS2 = FS1
	    )
	;   
	    FS2=FS1
	)
	.


:-std_prolog fs!reduce_bindings/2.
	
fs!reduce_bindings(Vars,Bindings) :-
	mutable_read(Bindings,L),
	fs!reduce_bindings_aux(L,LL,Vars),
	mutable(Bindings,LL)
	.

:-rec_prolog fs!reduce_bindings_aux/3.

fs!reduce_bindings_aux([],[],_).
fs!reduce_bindings_aux([V:FS|L1],XL,Vars) :-
	fs!reduce_bindings_aux(L1,XL1,Vars),
	( \+ V = var(gensym(_)) ->
	    XL = [V:FS|XL1]
	;   domain(V,Vars) ->
	    XL = [V:FS|XL1]
	;   
	    XL = XL1
	)
	.

	    

			   
